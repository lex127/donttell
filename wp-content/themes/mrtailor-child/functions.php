<?php
/**
 * Created by PhpStorm.
 * User: Alexey Sinyaev
 * Date: 27.11.2017
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

	unset( $tabs['description'] );      	// Remove the description tab
	unset( $tabs['reviews'] ); 			// Remove the reviews tab
	unset( $tabs['additional_information'] );  	// Remove the additional information tab
	return $tabs;
}
/**
 * @desc Remove quantity in all product type
 */
function wc_remove_all_quantity_fields( $return, $product ) {
	return true;
}
add_filter( 'woocommerce_is_sold_individually', 'wc_remove_all_quantity_fields', 10, 2 );

add_action( 'wp_enqueue_scripts', 'wpshout_dequeue_and_then_enqueue', 100 );

function wpshout_dequeue_and_then_enqueue() {
	// Dequeue (remove) parent theme script
	wp_dequeue_script( 'mr_tailor-counters' );

	// Enqueue replacement child theme script
	wp_enqueue_script(
		'child_mr_tailor-counters',
		get_stylesheet_directory_uri() . '/js/wc-counters.js', array('mr_tailor-scripts'), '1.0', TRUE );
}