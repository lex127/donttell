<?php
/**
 * Woo Economic constants and text strings
 *
 * @author kristianrasmussen.com
 */

define("WOO_ECONOMIC_VERSION", "1.0");
define("WOO_ECONOMIC_PREFIX", "woo_economic_");
define("WOO_ECONOMIC_VERSOIN_OPT", WOO_ECONOMIC_PREFIX."version");
define("WOO_ECONOMIC_OPTIONS", WOO_ECONOMIC_PREFIX."options");
define("WOO_ECONOMIC_ECONOMIC_OPTIONS", WOO_ECONOMIC_PREFIX."economic_options");
define("WOO_ECONOMIC_INTEGRATION_TITLE", 'E-conomic integration');
define("WOO_ECONOMIC_SYNC_TITLE", 'E-conomic synchronisation');
define("WOO_ECONOMIC_GENERAL_SETTINGS", 'General settings');
define("WOO_ECONOMIC_ECONOMIC_SETTINGS", 'E-conomic settings');

define("WOO_ECONOMIC_PLUGIN_NAME", "Woo-economic");

define("WOO_ECONOMIC_OPTION_PRODUCT_GROUP_NAME", WOO_ECONOMIC_PREFIX."product_group");
define("WOO_ECONOMIC_OPTION_PRODUCT_GROUP", "Product group");

define("WOO_ECONOMIC_OPTION_PRODUCT_OFFSET_NAME", WOO_ECONOMIC_PREFIX."product_offset");
define("WOO_ECONOMIC_OPTION_PRODUCT_OFFSET", "Product offset");

define("WOO_ECONOMIC_OPTION_CUSTOMER_OFFSET_NAME", WOO_ECONOMIC_PREFIX."customer_offset");
define("WOO_ECONOMIC_OPTION_CUSTOMER_OFFSET", "Customer offset");

define("WOO_ECONOMIC_OPTION_CUSTOMER_GROUP_NAME", WOO_ECONOMIC_PREFIX."customer_group");
define("WOO_ECONOMIC_OPTION_CUSTOMER_GROUP", "Customer group");

define("WOO_ECONOMIC_OPTION_ECONOMIC_DEBTOR_NAME", WOO_ECONOMIC_PREFIX."economic_debtor");
define("WOO_ECONOMIC_OPTION_ECONOMIC_INVOICE_NAME", WOO_ECONOMIC_PREFIX."economic_invoice_number");

define("WOO_ECONOMIC_OPTION_SHIPPING_PRODUCT_NUMBER_NAME", WOO_ECONOMIC_PREFIX."shipping_product_number");
define("WOO_ECONOMIC_OPTION_SHIPPING_PRODUCT_NUMBER", "Shipping product number");

define("WOO_ECONOMIC_OPTION_AUTO_DEB_PAYMENT_NAME", WOO_ECONOMIC_PREFIX."auto_deb_payment");
define("WOO_ECONOMIC_OPTION_AUTO_DEB_PAYMENT", "Create debtor payment");

define("WOO_ECONOMIC_OPTION_CASHBOOK_NAME", WOO_ECONOMIC_PREFIX."cashbook");
define("WOO_ECONOMIC_OPTION_CASHBOOK", "Cashbook for payments");

/********************  E-conomic fields ***********************/

define("WOO_ECONOMIC_OPTION_ECONOMIC_AGREEMENT_NAME", WOO_ECONOMIC_PREFIX."economic_agreement");
define("WOO_ECONOMIC_OPTION_ECONOMIC_AGREEMENT", "E-conomic agreement");

define("WOO_ECONOMIC_OPTION_ECONOMIC_USER_NAME", WOO_ECONOMIC_PREFIX."economic_user");
define("WOO_ECONOMIC_OPTION_ECONOMIC_USER", "E-conomic user");

define("WOO_ECONOMIC_OPTION_ECONOMIC_PASSWD_NAME", WOO_ECONOMIC_PREFIX."ecnomic_passwd");
define("WOO_ECONOMIC_OPTION_ECONOMIC_PASSWD", "E-conomic password");

/******************** Synchronization forms ******************/

define("WOO_ECONOMIC_FORM_SYNC", "woo_economic_sync");

/******************** Synced from e-conomic first ***********/

define("WOO_ECONOMIC_SYNCED_FROM_ECONOMIC_NAME", WOO_ECONOMIC_PREFIX."synced_from_eco")

?>
