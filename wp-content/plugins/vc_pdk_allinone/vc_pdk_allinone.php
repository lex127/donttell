<?php

/*
  Plugin Name: vConnect Post Danmark All in One
  Plugin URI: http://woothemes.com/woocommerce
  Description: PostNord Shipping integration for WooCommerce
  Version: 1.2.12
  Author: vConnect
  Author URI: http://vconnect.dk
 */

/**
 * Check if WooCommerce is active
 */
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    define( 'AINO_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

    $vc_aino_dir = plugin_dir_path( __FILE__ );

    foreach (glob($vc_aino_dir."classes/core/services/*.php") as $filename) {
        require_once $filename;
    }

    require_once 'classes/specific/vc-aino-shipping-methods.php';

    new VC_Aino_License();
    new Vc_Aino_Shipping_Methods();
    new Vc_Aino_Template();
    new Vc_Aino_Scripts();
    new Vc_Aino_Pickup_Points();
    new Vc_Aino_Order();

}