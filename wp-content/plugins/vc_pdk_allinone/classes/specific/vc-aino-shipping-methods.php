<?php

class Vc_Aino_Shipping_Methods {

    function __construct(){
        add_action('woocommerce_shipping_init', array($this, 'your_shipping_method_init'));
        add_filter('woocommerce_shipping_methods', array($this, 'add_your_shipping_method'));
    }

    function your_shipping_method_init() {
        // Require all shipping classes located in the classes/methods folder
        foreach (glob(AINO_PLUGIN_PATH . '/classes/specific/methods/*.php') as $filename){
            require_once $filename;
        }
    }

    /*
     * Add the shipping methods to WooCommerce
     */
    function add_your_shipping_method($methods) {
        $methods['vconnect_postnord_dk_privatehome']        = 'WC_VC_DK_Privatehome';
        $methods['vconnect_postnord_dk_pickup']             = 'WC_VC_DK_Pickup';
        $methods['vconnect_postnord_dk_commercial']         = 'WC_VC_DK_Commercial';
        $methods['vconnect_postnord_dk_se_pickup']          = 'WC_VC_SE_Pickup';
        $methods['vconnect_postnord_dk_no_pickup']          = 'WC_VC_NO_Pickup';
        $methods['vconnect_postnord_dk_fi_pickup']          = 'WC_VC_FI_Pickup';
        $methods['vconnect_postnord_dk_dpdeu']              = 'WC_VC_EU_Private';
        $methods['vconnect_postnord_dk_dpdinternational']   = 'WC_VC_World_Private';

        return $methods;
    }

    function get_available_shipping_methods(){
        $packages = WC()->shipping->get_packages();
        $available_methods = array();

        foreach ( $packages as $key => $package ) {
            $key_arr = explode(':', $key);
//            $available_methods = array_merge($available_methods, $package['rates']);
            $available_methods[$key_arr[0]] = $package['rates'];
        }

        return $available_methods;
    }

    // Specific
    function map_methods() {
        if(get_option('vc_aino_use_universe')=='yes'){
            $methods = array(
                'vconnect_postnord_dk_privatehome'      => 'your-address',
                'vconnect_postnord_dk_pickup'           => 'post-office',
                'vconnect_postnord_dk_commercial'       => 'your-business',
                'vconnect_postnord_dk_se_pickup'        => 'post-office',
                'vconnect_postnord_dk_no_pickup'        => 'post-office',
                'vconnect_postnord_dk_fi_pickup'        => 'post-office',
                'vconnect_postnord_dk_dpdeu'            => 'aino-international',
                'vconnect_postnord_dk_dpdinternational' => 'aino-international'
            );
        } else {
            $methods = array(
                'vconnect_postnord_dk_pickup'           => 'post-office',
                'vconnect_postnord_dk_se_pickup'        => 'post-office',
                'vconnect_postnord_dk_no_pickup'        => 'post-office',
                'vconnect_postnord_dk_fi_pickup'        => 'post-office',
            );
        }

        return $methods;
    }
}
