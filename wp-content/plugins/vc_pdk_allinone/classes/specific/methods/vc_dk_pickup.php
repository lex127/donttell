<?php
if (!class_exists('WC_VC_DK_Pickup')) {

    require_once AINO_PLUGIN_PATH . 'classes/core/abstract/vc_aino_shipping_method.php';

    class WC_VC_DK_Pickup extends Vc_Aino_Shipping_Method {

        // Id for your shipping method. Should be uunique.
        protected $vc_aino_id = 'vconnect_postnord_dk_pickup';
        // Title shown in admin
        protected $vc_aino_method_title = 'Denmark pickup';
        // Description shown in admin
        protected $vc_aino_method_description = 'Description of your shipping method';
        // This can be added as an setting but for this example its forced.
        protected $vc_aino_title = 'Denmark pickup';

    }

}