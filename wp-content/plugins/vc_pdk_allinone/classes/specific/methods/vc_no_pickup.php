<?php
if (!class_exists('WC_VC_NO_Pickup')) {

    require_once AINO_PLUGIN_PATH . 'classes/core/abstract/vc_aino_shipping_method.php';

    class WC_VC_NO_Pickup extends Vc_Aino_Shipping_Method {

        // Id for your shipping method. Should be unique.
        protected $vc_aino_id = 'vconnect_postnord_dk_no_pickup';
        // Title shown in admin
        protected $vc_aino_method_title = 'Norway pickuppoint';
        // Description shown in admin
        protected $vc_aino_method_description = 'Description of your shipping method';
        // This can be added as an setting but for this example its forced.
        protected $vc_aino_title = 'Norway pickuppoint';


    }

}