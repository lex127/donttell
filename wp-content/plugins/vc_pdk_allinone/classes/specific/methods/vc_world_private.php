<?php
if (!class_exists('WC_VC_World_Private')) {

    require_once AINO_PLUGIN_PATH . 'classes/core/abstract/vc_aino_shipping_method.php';

    class WC_VC_World_Private extends Vc_Aino_Shipping_Method {

        // Id for your shipping method. Should be unique.
        protected $vc_aino_id = 'vconnect_postnord_dk_dpdinternational';
        // Title shown in admin
        protected $vc_aino_method_title = 'International Delivery';
        // Description shown in admin
        protected $vc_aino_method_description = 'Description of your shipping method';
        // This can be added as an setting but for this example its forced.
        protected $vc_aino_title = 'International Delivery';

    }

}