                        <div class="tab-list aino-your-business cf column" data-target="your-business">
                            <div class="aino-inner-tabs cf">
                                <ul class="aino-nav active">
                                    <li>
                                        <p class="aino-tab-sub-title">
                                            Leveringstid
                                        </p>
                                        <a role='tab' href="#arrival" class="clearfix">
                                            <span>
                                                <span class="mounth-day">1-3 dage</span>08:00 - 16:00
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <p class="aino-tab-sub-title">
                                            Levering
                                        </p>

                                        <a role='tab' href="#personal-delivery">
                                            <span>Din adresse</span>
                                        </a>

                                        <p>
                                            Små pakker lægges i postkassen.
                                        </p>

                                        <br/>

                                        <div class="aino-textarea-holder">
                                            <textarea rows="6" cols="10" name="write_place" placeholder="Skriv placering"></textarea>
                                        </div>
                                    </li>
                                </ul>
                                <div class="aino-tabs-body">
                                    <div class="tab-list aino-arrival column-2" data-target="arrival">
                                        <div>Vælg leveringsdag <span class="close"></span></div>
                                        <span class="cf">
                                            <ul class="aino-nav">
                                                <li class="active">
                                                    <label class="aino-input aino-checkbox">
                                                        <input type="radio" name="arrival-2"
                                                               value="1-3 days 08:00 - 16:00" checked="checked">
                                                        <span class="aino-indicator"></span>
                                                        <span class="label-text">1-3 dage 08:00 - 16:00</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </span>

                                        <div class="aino-calendar-holder">
                                            <div class="toggle-calendar">show calendar</div>
                                            <div class="aino-calendar hidden"></div>
                                        </div>
                                        <div class="delivery-time">
                                            <div class="delivery-time-title">
                                                <i>Select delivery time</i> <span class="close-inner"></span>
                                            </div>
                                            <ul class="aino-nav aino-delivery-time-picker">

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="tab-list aino-personal-delivery column-2"
                                         data-target="personal-delivery">
                                        <div>Vælg levering <span class="close"></span></div>
                                        <ul class="aino-nav">
                                            <li class="active">
                                                <label class="aino-input aino-checkbox">
                                                    <input type="radio" name="personal-delivery"
                                                           value="Personal Delivery" checked="checked">
                                                    <span class="aino-indicator"></span>
                                                    <span class="label-text">Din adresse</span>
                                                </label>
                                            </li>
                                            <li class="aino-sub-nav">
                                                <a class="aino-collapse">Flexlevering</a>

                                                <div class="aino-collapse-content">
                                                    <ul class="aino-nav">
                                                        <li>
                                                            <label class="aino-input aino-checkbox">
                                                                <input type="radio" name="personal-delivery"
                                                                       value="In front of the Door">
                                                                <span class="aino-indicator"></span>
                                                                <span class="label-text">Foran døren</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="aino-input aino-checkbox">
                                                                <input type="radio" name="personal-delivery"
                                                                       value="Carport">
                                                                <span class="aino-indicator"></span>
                                                                <span class="label-text">Carport/garage</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="aino-input aino-checkbox">
                                                                <input type="radio" name="personal-delivery"
                                                                       value="Infront of backdoor">
                                                                <span class="aino-indicator"></span>
                                                                <span class="label-text">Ved bagdøren</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="aino-input aino-checkbox">
                                                                <input type="radio" name="personal-delivery"
                                                                       value="I have modttagarflex">
                                                                <span class="aino-indicator"></span>
                                                                <span class="label-text">Jeg har Modtagerflex</span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label class="aino-input aino-checkbox">
                                                                <input type="radio" name="personal-delivery"
                                                                       value="Other place">
                                                                <span class="aino-indicator"></span>
                                                                <span class="label-text">Andet sted</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>