                        <li class="mobile-container">
                            <a class="aino-collapse cf" data-method="#your-business">
                                <?php echo $method_info->label; ?>
                                <span class="aino-delivery-price"><?php echo $method_info->cost; ?> kr</span>
                            </a>

                            <div class="aino-collapse-content">
                                <div class="aino-weather">
                                    <ul class="aino-nav cf">

                                    </ul>
                                </div>

                                <ul class="aino-nav">
                                    <li>
                                        <p class="aino-tab-sub-title">
                                            Leveringstid
                                        </p>

                                        <a class="aino-collapse cf">
                                            1-3 dage 08:00 - 16:00
                                        </a>

                                        <div class="aino-collapse-content">
                                            <ul class="aino-nav">
                                                <li>
                                                    <label class="aino-input aino-checkbox">
                                                        <input type="radio" name="arrival" value="1-3 days 08:00 - 16:00">
                                                        <span class="aino-indicator"></span>
                                                        <span class="label-text">1-3 dage 08:00 - 16:00</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <p class="aino-tab-sub-title">
                                            Levering
                                        </p>

                                        <div class="aino-textarea-holder">

                                            <p>
                                                Små pakker lægges i postkassen.
                                            </p>

                                            <br/>
                                            <textarea rows="6" cols="10" name="write_place" placeholder="Skriv placering"></textarea>
                                        </div>

                                        <a class="aino-collapse cf">
                                            Foran døren
                                        </a>

                                        <div class="aino-collapse-content">
                                            <ul class="aino-nav">
                                                <li class="active">
                                                    <label class="aino-input aino-checkbox">
                                                        <input type="radio" name="personal-delivery" checked="checked" value="Personal Delivery">
                                                        <span class="aino-indicator"></span>
                                                        <span class="label-text">Din adresse</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="aino-input aino-checkbox">
                                                        <input type="radio" name="personal-delivery"
                                                               value="In front of the Door">
                                                        <span class="aino-indicator"></span>
                                                        <span class="label-text">Foran døren</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="aino-input aino-checkbox">
                                                        <input type="radio" name="personal-delivery"
                                                               value="Carport">
                                                        <span class="aino-indicator"></span>
                                                        <span class="label-text">Carport/garage</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="aino-input aino-checkbox">
                                                        <input type="radio" name="personal-delivery"
                                                               value="Infront of backdoor">
                                                        <span class="aino-indicator"></span>
                                                        <span class="label-text">Ved bagdøren</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="aino-input aino-checkbox">
                                                        <input type="radio" name="personal-delivery"
                                                               value="I have modttagarflex">
                                                        <span class="aino-indicator"></span>
                                                        <span class="label-text">Jeg har Modtagerflex</span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label class="aino-input aino-checkbox">
                                                        <input type="radio" name="personal-delivery"
                                                               value="Other place">
                                                        <span class="aino-indicator"></span>
                                                        <span class="label-text">Andet sted</span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>