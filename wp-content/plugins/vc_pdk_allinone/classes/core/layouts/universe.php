<div data-target="aino-popup" class="aino-popup aino-universe">
    <div class="aino-container desktop">
        <div class="aino-core">
            <div class="aino-header cf">
                <h1 class="postnord-logo">
                    <span>Postnord Logo</span>
                </h1>

                <div class="aino-weather cf">
                    <ul class="aino-nav">

                    </ul>
                </div>
            </div>
            <div class="aino-tabs cf">
                <ul class="aino-nav aino-deliver-to">
                    <?php foreach ($contents['tabs'] as $tab) { ?>
                        <?php echo $tab; ?>
                    <?php } ?>
                </ul>
                <div class="aino-tabs-body">
                <?php foreach ($contents['desktop'] as $desktop_item) { ?>
                    <?php echo $desktop_item; ?>
                <?php } ?>
                </div>
            </div>
            <div class="aino-footer">
                <div>
                    <button class="aino-button gray" role="close" onclick="InitAllInOneModule().close">
                        Fortryd
                    </button>
                    <button class="aino-button blue" role="submit"
                            onclick="InitAllInOneModule().submit">
                        ok
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="aino-container mobile">
    <div class="aino-core">
        <div class="aino-header">
            <h1 class="postnord-logo">
                <span>Postnord</span>
            </h1>
        </div>
        <div class="aino-body">
            <ul class="aino-nav">
                <?php foreach ($contents['mobile'] as $mobile_item) { ?>
                    <?php echo $mobile_item; ?>
                <?php } ?>
            </ul>
        </div>
        <div class="aino-footer">
            <div>
                <button class="aino-button gray" role="close" onclick="InitAllInOneModule().close">
                    Fortryd
                </button>
                <button class="aino-button blue" role="submit"
                        onclick="InitAllInOneModule().submit">
                    ok
                </button>
            </div>
        </div>
    </div>
</div>
