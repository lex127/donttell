<?php
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Flat Rate Shipping Method.
 *
 * @class 		WC_Shipping_Flat_Rate
 * @version		2.6.0
 * @package		WooCommerce/Classes/Shipping
 * @author 		WooThemes
 */
class Vc_Aino_Shipping_Method extends WC_Shipping_Method {

    // Id for your shipping method. Should be unique.
    protected $vc_aino_id = '';
    // Title shown in admin
    protected $vc_aino_method_title = '';
    // Description shown in admin
    protected $vc_aino_method_description = '';
    // This can be added as an setting but for this example its forced.
    protected $vc_aino_title = '';
    // Define the Universe popup this shipping method uses
    public $vc_aino_popup = 'post-office';
    // Define the Universe popup this shipping method uses
    public $transit_time = '';

    private $vc_rates;

    /** @var string cost passed to [fee] shortcode */
    protected $fee_cost = '';

    /**
     * Constructor.
     */
    public function __construct($instance_id = 0) {
        $this->id = $this->vc_aino_id;
        $this->instance_id = absint($instance_id);
        $this->method_title = $this->vc_aino_method_title;
        $this->method_description = $this->vc_aino_method_description;
        $this->supports = array(
            'shipping-zones',
            'instance-settings',
            'instance-settings-modal',
        );
        $this->init();

        add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
    }

    /**
     * init user set variables.
     */
    public function init() {
        $this->instance_form_fields = include( 'includes/settings-aino-shipping-method.php' );
        $this->title = $this->get_option('title');
        $this->transit_time = $this->get_option('transit_time');
		$this->tax_status           = $this->get_option( 'tax_status' );
//		$this->cost                 = $this->get_option( 'cost' );
//		$this->type                 = $this->get_option( 'type', 'class' );

        $this->vc_rates = get_option($this->id . '_rates[' . $this->get_instance_id() . ']');
    }

    /**
     * Evaluate a cost from a sum/string.
     * @param  string $sum
     * @param  array  $args
     * @return string
     */
    protected function evaluate_cost($sum, $args = array()) {
        include_once( WC()->plugin_path() . '/includes/libraries/class-wc-eval-math.php' );

        // Allow 3rd parties to process shipping cost arguments
        $args = apply_filters('woocommerce_evaluate_shipping_cost_args', $args, $sum, $this);
        $locale = localeconv();
        $decimals = array(wc_get_price_decimal_separator(), $locale['decimal_point'], $locale['mon_decimal_point']);
        $this->fee_cost = $args['cost'];

        // Expand shortcodes
        add_shortcode('fee', array($this, 'fee'));

        $sum = do_shortcode(str_replace(
                        array(
            '[qty]',
            '[cost]'
                        ), array(
            $args['qty'],
            $args['cost']
                        ), $sum
                ));

        remove_shortcode('fee', array($this, 'fee'));

        // Remove whitespace from string
        $sum = preg_replace('/\s+/', '', $sum);

        // Remove locale from string
        $sum = str_replace($decimals, '.', $sum);

        // Trim invalid start/end characters
        $sum = rtrim(ltrim($sum, "\t\n\r\0\x0B+*/"), "\t\n\r\0\x0B+-*/");

        // Do the math
        return $sum ? WC_Eval_Math::evaluate($sum) : 0;
    }

    /**
     * Work out fee (shortcode).
     * @param  array $atts
     * @return string
     */
    public function fee($atts) {
        $atts = shortcode_atts(array(
            'percent' => '',
            'min_fee' => '',
            'max_fee' => '',
                ), $atts);

        $calculated_fee = 0;

        if ($atts['percent']) {
            $calculated_fee = $this->fee_cost * ( floatval($atts['percent']) / 100 );
        }

        if ($atts['min_fee'] && $calculated_fee < $atts['min_fee']) {
            $calculated_fee = $atts['min_fee'];
        }

        if ($atts['max_fee'] && $calculated_fee > $atts['max_fee']) {
            $calculated_fee = $atts['max_fee'];
        }

        return $calculated_fee;
    }

    /**
     * calculate_shipping function.
     *
     * @param array $package (default: array())
     */
    public function calculate_shipping($package = array()) {
        $rate = array(
            'id' => $this->get_rate_id(),
            'label' => $this->title,
            'cost' => $this->get_cost($package),
            'package' => $package,
        );

        // Calculate the costs
        $has_costs = false; // True when a cost is set. False if all costs are blank strings.
        $cost = $this->get_cost($package);

        if ($cost !== '') {

            $has_costs = true;
            $rate['cost'] = $this->evaluate_cost($cost, array(
                'qty' => $this->get_package_item_qty($package),
                'cost' => $package['contents_cost'],
                    ));
        }

        // Add shipping class costs
        $found_shipping_classes = $this->find_shipping_classes($package);
        $highest_class_cost = 0;

        foreach ($found_shipping_classes as $shipping_class => $products) {
            // Also handles BW compatibility when slugs were used instead of ids
            $shipping_class_term = get_term_by('slug', $shipping_class, 'product_shipping_class');
            $class_cost_string = $shipping_class_term && $shipping_class_term->term_id ? $this->get_option('class_cost_' . $shipping_class_term->term_id, $this->get_option('class_cost_' . $shipping_class, '')) : $this->get_option('no_class_cost', '');

            if ($class_cost_string === '') {
                continue;
            }

            $has_costs = true;
            $class_cost = $this->evaluate_cost($class_cost_string, array(
                'qty' => array_sum(wp_list_pluck($products, 'quantity')),
                'cost' => array_sum(wp_list_pluck($products, 'line_total'))
                    ));

            if ($this->type === 'class') {
                $rate['cost'] += $class_cost;
            } else {
                $highest_class_cost = $class_cost > $highest_class_cost ? $class_cost : $highest_class_cost;
            }
        }

        if(!empty($this->type)){
            if ($this->type === 'order' && $highest_class_cost) {
                $rate['cost'] += $highest_class_cost;
            }
        }

        // Add the rate
        if ($has_costs) {
            $this->add_rate($rate);
        }

        do_action('woocommerce_' . $this->id . '_shipping_add_rate', $this, $rate);
    }

    /**
     * Get items in package.
     * @param  array $package
     * @return int
     */
    public function get_package_item_qty($package) {
        $total_quantity = 0;
        foreach ($package['contents'] as $item_id => $values) {
            if ($values['quantity'] > 0 && $values['data']->needs_shipping()) {
                $total_quantity += $values['quantity'];
            }
        }
        return $total_quantity;
    }

    /**
     * Finds and returns shipping classes and the products with said class.
     * @param mixed $package
     * @return array
     */
    public function find_shipping_classes($package) {
        $found_shipping_classes = array();

        foreach ($package['contents'] as $item_id => $values) {
            if ($values['data']->needs_shipping()) {
                $found_class = $values['data']->get_shipping_class();

                if (!isset($found_shipping_classes[$found_class])) {
                    $found_shipping_classes[$found_class] = array();
                }

                $found_shipping_classes[$found_class][$item_id] = $values;
            }
        }

        return $found_shipping_classes;
    }

    public function generate_WC_VC_Table_rates_table_html() {
        global $woocommerce;
        ob_start();
        ?>
        <tr valign="top">
            <th scope="row" class="titledesc"><?php _e('Costs', 'vc-allinone'); ?>:</th>
            <td class="forminp" id="<?php echo $this->id; ?>_rates">
                <table class="shippingrows widefat" cellspacing="0">
                    <thead>
                        <tr>
                            <th class="check-column"><input type="checkbox"></th>
                            <th><?php _e('Minimum Weight', 'vc-allinone'); ?></th>
                            <th><?php _e('Maximum Weight', 'vc-allinone'); ?></th>
                            <th><?php _e('Total from', 'vc-allinone'); ?></th>
                            <th><?php _e('Total to', 'vc-allinone'); ?></th>
                            <th><?php _e('Shipping Price', 'vc-allinone'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan="6">
                                <a href="#" class="add button">
        <?php _e('+ Add row', 'vc-allinone'); ?>
                                </a>
                                <a href="#" class="remove button">
                                    <?php _e('Delete selected rows', 'vc-allinone'); ?>
                                </a>
                            </th>
                        </tr>
                    </tfoot>
                    <tbody class="<?php echo $this->id; ?>_rates_tbody">
        <?php
        $i = -1;
        if ($this->vc_rates) {
            foreach ($this->vc_rates as $rate) {
                $i++;

                echo '<tr class="vc_rate">
                                        <th class="check-column"><input type="checkbox" name="select" /></th>
                                        <td><input type="number" class="vc-rate-weight-min" step="any" min="0" value="' . esc_attr($rate['weight_min']) . '" name="woocommerce_' . esc_attr($this->id . '_rates][' . $i . '][weight_min]') . '" placeholder="' . __('0.00', 'vc-allinone') . '" size="4" /></td>
                                        <td><input type="number" class="vc-rate-weight-max" step="any" min="0" value="' . esc_attr($rate['weight_max']) . '" name="woocommerce_' . esc_attr($this->id . '_rates][' . $i . '][weight_max]') . '" placeholder="' . __('0.00', 'vc-allinone') . '" size="4" /></td>
                                        <td><input type="number" class="vc-rate-total-from" step="any" min="0" value="' . esc_attr($rate['total_from']) . '" name="woocommerce_' . esc_attr($this->id . '_rates][' . $i . '][total_from]') . '" placeholder="' . __('0.00', 'vc-allinone') . '" size="4" /></td>
                                        <td><input type="number" class="vc-rate-total-to" step="any" min="0" value="' . esc_attr($rate['total_to']) . '" name="woocommerce_' . esc_attr($this->id . '_rates][' . $i . '][total_to]') . '" placeholder="' . __('0.00', 'vc-allinone') . '" size="4" /></td>
                                        <td><input type="text" value="' . esc_attr($rate['cost']) . '" name="woocommerce_' . esc_attr($this->id . '_rates][' . $i . '][cost]') . '" placeholder="' . __('0.00', 'vc-allinone') . '" size="4" /></td>
                                      </tr>';
            }
        }
        ?>
                    </tbody>
                </table>
                <script type="text/javascript">
                    jQuery(function () {

                    jQuery('#<?php echo $this->id; ?>_rates').on('click', 'a.add', function () {

                    var size = jQuery('#<?php echo $this->id; ?>_rates tbody .vc_rate').size();
                    jQuery('<tr class="vc_rate">\
                                        <th class="check-column"><input type="checkbox" name="select" /></th>\
                                        <td><input type="number" step="any" min="0" name="woocommerce_<?php echo $this->id; ?>_rates][' + size + '][weight_min]" placeholder="0.00" size="4" /></td>\
                                        <td><input type="number" step="any" min="0" name="woocommerce_<?php echo $this->id; ?>_rates][' + size + '][weight_max]" placeholder="0.00" size="4" /></td>\
                                        <td><input type="number" step="any" min="0" name="woocommerce_<?php echo $this->id; ?>_rates][' + size + '][total_from]" placeholder="0.00" size="4" /></td>\
                                        <td><input type="number" step="any" min="0" name="woocommerce_<?php echo $this->id; ?>_rates][' + size + '][total_to]" placeholder="0.00" size="4" /></td>\
                                        <td><input type="text" name="woocommerce_<?php echo $this->id; ?>_rates][' + size + '][cost]" placeholder="0.00" size="4" /></td>\
                            </tr>').appendTo('#<?php echo $this->id; ?>_rates table tbody');
                    return false;
                    });
                    // Remove row
                    jQuery('#<?php echo $this->id; ?>_rates').on('click', 'a.remove', function () {
                    var answer = confirm("<?php _e('Delete the selected rates?', 'vc-allinone'); ?>")
                            if (answer) {
                    jQuery('#<?php echo $this->id; ?>_rates table tbody tr th.check-column input:checked').each(function (i, el) {
                    jQuery(el).closest('tr').remove();
                    });
                    }
                    return false;
                    });
                    });
                </script>
            </td>
        </tr>
        <?php
        return ob_get_clean();
    }

	/**
	 * Processes and saves options.
	 * If there is an error thrown, will continue to save and validate fields, but will leave the erroring field out.
	 * @since 2.6.0
	 * @return bool was anything saved?
	 */
	public function process_admin_options() {
		if ( $this->instance_id ) {
			$this->init_instance_settings();

			$post_data = $this->get_post_data();

			foreach ( $this->get_instance_form_fields() as $key => $field ) {
				if ( 'title' !== $this->get_field_type( $field ) ) {
					try {
						$this->instance_settings[ $key ] = $this->get_field_value( $key, $field, $post_data );
					} catch ( Exception $e ) {
						$this->add_error( $e->getMessage() );
					}
				}

			}

                        update_option($this->id . '_rates[' . $this->get_instance_id() . ']', $post_data['woocommerce_' . $this->id . '_rates']);

			return update_option( $this->get_instance_option_key(), apply_filters( 'woocommerce_shipping_' . $this->id . '_instance_settings_values', $this->instance_settings, $this ) );
		} else {
			return parent::process_admin_options();
		}
	}


    /**
     * get_cost function.
     *
     * @access public
     * @param mixed $package
     * @return void
     */
    public function get_cost($package = array()) {
        global $woocommerce;

        $rates = get_option($this->id . '_rates[' . $this->get_instance_id() . ']');
        if (isset($woocommerce->cart->cart_contents_total)) {
            $weight = $woocommerce->cart->get_cart_contents_weight();

            $total = 0;

            // Get cart items
            $cart_items = $package['contents'];

            // Check all cart items
            foreach ($cart_items as $cart_item) {
                $shipping_class = $cart_item['data']->get_shipping_class();

                if ($shipping_class != 'billige-varer' && $shipping_class != 'udsalg') {

                    $total += $cart_item['data']->price*$cart_item['quantity'];
                }
            }

            if (!empty($rates)
//                    && $this->is_available($package)
                    ) {
                foreach ($rates as $rate) {
                    if ($weight >= $rate['weight_min'] && $weight < $rate['weight_max'] && $total >= $rate['total_from'] && $total < $rate['total_to']) {
                        return $rate['cost'] != '' ? $rate['cost'] : 0;
                    }
                }
            }
        }

        return '';
    }

}
