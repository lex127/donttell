<?php

class WC_Settings_Tab_Demo {

    /**
     * Bootstraps the class and hooks required actions & filters.
     *
     */
    public static function init() {
        add_filter('woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50);
        add_action('woocommerce_settings_tabs_settings_tab_demo', __CLASS__ . '::settings_tab');
        add_action('woocommerce_update_options_settings_tab_demo', __CLASS__ . '::update_settings');
    }

    /**
     * Add a new settings tab to the WooCommerce settings tabs array.
     *
     * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
     */
    public static function add_settings_tab($settings_tabs) {
        $settings_tabs['settings_tab_demo'] = __('vConnect All In One Settings', 'woocommerce-settings-tab-demo');
        return $settings_tabs;
    }

    /**
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     *
     * @uses woocommerce_admin_fields()
     * @uses self::get_settings()
     */
    public static function settings_tab() {
        woocommerce_admin_fields(self::get_settings());
    }

    /**
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses self::get_settings()
     */
    public static function update_settings() {
        woocommerce_update_options(self::get_settings());

        $license = new VC_Aino_License();
        $license->process_license(get_option('vc_aino_license_email'), get_option('vc_aino_license_key'), get_option('vc_aino_consumer_id'));
    }

    /**
     * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
     *
     * @return array Array of settings for @see woocommerce_admin_fields() function.
     */
    public static function get_settings() {
        $settings = array(
            'section_title' => array(
                'name' => __('Section Title', 'woocommerce-settings-tab-demo'),
                'type' => 'title',
                'desc' => '',
                'id' => 'wc_settings_tab_demo_section_title'
            ),
            'license_email' => array(
                'title' => __('vConnect license Email', 'vc-allinone'),
                'type' => 'text',
                'id' => 'vc_aino_license_email',
                'description' => __('The email address you have registered the Post Danmark All in One extension with.', 'vc-allinone'),
                'default' => __('', 'vc-allinone'),
                'desc_tip' => true,
            ),
            'license_key' => array(
                'title' => __('vConnect license Key', 'vc-allinone'),
                'type' => 'text',
                'id' => 'vc_aino_license_key',
                'description' => __('The license key you have been provided on registering the Post Danmark All in One extension.', 'vc-allinone'),
                'default' => __('', 'vc-allinone'),
                'desc_tip' => true,
            ),
            'consumer_id' => array(
                'title' => __('Postnord API consumerId', 'vc-allinone'),
                'type' => 'text',
                'id' => 'vc_aino_consumer_id',
                'description' => __('Post Danmark general consumer id', 'vc-allinone'),
                'default' => '',
                'desc_tip' => true
            ),
            'maps_api_key' => array(
                'title' => __('Google Maps API Key', 'vc-allinone'),
                'type' => 'text',
                'id' => 'vc_aino_gmaps_key',
                'description' => __('Google Maps API Key', 'vc-allinone'),
                'default' => '',
                'desc_tip' => true
            ),
            'use_universe' => array(
                'title' => __('Use Postnord Universe', 'vc-allinone'),
                'type' => 'checkbox',
                'id' => 'vc_aino_use_universe',
                'description' => __('Post Danmark general consumer id', 'vc-allinone'),
                'default' => '',
                'desc_tip' => true
            ),
            'section_end' => array(
                'type' => 'sectionend',
                'id' => 'wc_settings_tab_demo_section_end'
            ),
        );
        return apply_filters('wc_settings_tab_demo_settings', $settings);
    }

}

WC_Settings_Tab_Demo::init();
