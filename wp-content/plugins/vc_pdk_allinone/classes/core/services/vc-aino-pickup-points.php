<?php

class Vc_Aino_Pickup_Points {

    private $api_url = "https://api2.postnord.com/rest/businesslocation/v1/servicepoint/findNearestByAddress.json?";

    function __construct() {
        add_action('wp_ajax_get_points', array($this, 'get_points'));
        add_action('wp_ajax_nopriv_get_points', array($this, 'get_points'));
    }

    function api_call($consumer_id, $postcode, $country_code) {

        $url = $this->api_url
                . "apikey=$consumer_id"
                . "&countryCode=$country_code"
                . "&postalCode=$postcode"
                . "&numberOfServicePoints=10"
                . "&locale=en";

        $json_decoded = $this->curl_call($url);

        if (!empty($json_decoded->servicePointInformationResponse->compositeFault)
                || empty($json_decoded->servicePointInformationResponse->servicePoints)) {

            $url = $this->api_url
                    . "apikey=" . get_option('vc_aino_consumer_id')
                    . "&countryCode=$country_code"
                    . "&postalCode=" . $this->default_postcodes($country_code)
                    . "&numberOfServicePoints=10"
                    . "&locale=en";

            $json_decoded = $this->curl_call($url);
        }

        return !empty($json_decoded->servicePointInformationResponse->servicePoints) ? $json_decoded->servicePointInformationResponse->servicePoints : array();
    }

    function build_response($postcode, $data, $country_code) {

        $points = $data;

        $servicePointId = array();
        $addresses = array();
        $name = array();
        $distance = array();
        $number = array();
        $generate = array();
        $opening = array();
        $close = array();
        $opening_sat = array();
        $close_sat = array();
        $lat = array();
        $lng = array();
        $key = 1;

        if (!empty($points)) {
            foreach ($points as $key => $point) {
                if (!empty($point->deliveryAddress->streetName)) {

                    $pointId = isset($point->servicePointId) ? trim($point->servicePointId) : '';
                    $point_name = isset($point->name) ? trim($point->name) : '';
                    $point_distance = isset($point->routeDistance) ? trim($point->routeDistance) : '';
                    $streetName = isset($point->deliveryAddress->streetName) ? trim($point->deliveryAddress->streetName) : '';
                    $streetNumber = isset($point->deliveryAddress->streetNumber) ? trim($point->deliveryAddress->streetNumber) : '';
                    $city = isset($point->deliveryAddress->city) ? trim($point->deliveryAddress->city) : '';
                    $countryCode = isset($point->deliveryAddress->countryCode) ? trim($point->deliveryAddress->countryCode) : '';
                    $postalCode = isset($point->deliveryAddress->postalCode) ? trim($point->deliveryAddress->postalCode) : '';
                    $pointOpening = isset($point->openingHours[0]->from1) ? substr_replace(trim($point->openingHours[0]->from1), ':', 2, 0) : '';
                    $pointClosing = isset($point->openingHours[0]->to1) ? substr_replace(trim($point->openingHours[0]->to1), ':', 2, 0) : '';
                    $pointOpeningSat = isset($point->openingHours[5]->from1) ? substr_replace(trim($point->openingHours[5]->from1), ':', 2, 0) : '';
                    $pointClosingSat = isset($point->openingHours[5]->to1) ? substr_replace(trim($point->openingHours[5]->to1), ':', 2, 0) : '';

                    $addresses[] = $streetName . ' ' . $streetNumber;
                    $cities[] = $city;
                    $countryCodes[] = $countryCode;
                    $postalCodes[] = $postalCode;
                    $name[] = $point_name;
                    $distance[] = $point_distance / 1000;
                    $opening[] = $pointOpening;
                    $close[] = $pointClosing;
                    $opening_sat[] = $pointOpeningSat;
                    $close_sat[] = $pointClosingSat;

                    $lat[] = $point->coordinate->northing;
                    $lng[] = $point->coordinate->easting;

                    $number[] = $postalCode . ' ' . $city;
                    $servicePointId[] = $pointId;

                    $generate[$pointId] = $streetName . ' ' . $streetNumber . ',' . $point_name . ',' . $postalCode . ' ' . $city . ',' . ($key) . ',' . $pointOpening . ',' . $pointClosing . ',' . $pointOpeningSat . ',' . $pointClosingSat . ',' . $point->coordinate->northing . ',' . $point->coordinate->easting;

                    $key++;
                }
            }
        }

        $post_addresses['addresses'] = !empty($addresses) ? $addresses : array();
        $post_addresses['city'] = !empty($cities) ? $cities : array();
        $post_addresses['countryCode'] = !empty($countryCodes) ? $countryCodes : array();
        $post_addresses['postcode'] = !empty($postalCodes) ? $postalCodes : array();
        $post_addresses['name'] = !empty($name) ? $name : array();
        $post_addresses['distance'] = !empty($distance) ? $distance : array();
        $post_addresses['number'] = !empty($number) ? $number : array();
        $post_addresses['generate'] = !empty($generate) ? $generate : array();
        $post_addresses['opening'] = !empty($opening) ? $opening : array();
        $post_addresses['close'] = !empty($close) ? $close : array();
        $post_addresses['opening_sat'] = !empty($opening_sat) ? $opening_sat : array();
        $post_addresses['close_sat'] = !empty($close_sat) ? $close_sat : array();
        $post_addresses['lat'] = !empty($lat) ? $lat : array();
        $post_addresses['lng'] = !empty($lng) ? $lng : array();
        $post_addresses['servicePointId'] = !empty($servicePointId) ? $servicePointId : array();

        return $post_addresses;
    }

    function vc_geocode($address, $postcode, $country_code) {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?"
                . "address=" . urlencode($address) . "," . $postcode
                . "," . $country_code;

        $response = $this->curl_call($url);

        if(!empty($response->results)){
            $origin = array(
                'type' => $response->results[0]->geometry->location_type,
                'postcode' => $postcode,
                'lat' => $response->results[0]->geometry->location->lat,
                'lng' => $response->results[0]->geometry->location->lng,
                'partial_match' => !empty($response->results[0]->partial_match)
                                        ? 1 : 0,
            );
        } else {
            $origin = array(
                'type' => null,
                'postcode' => null,
                'lat' => null,
                'lng' => null,
                'partial_match' => 0,
            );
        }

        return $origin;
    }

    function get_points() {
        global $woocommerce;

        $country_code = !empty($_POST['country_code']) ? $_POST['country_code']
                : $woocommerce->customer->shipping_country;

        if (isset($_POST['postcode'])) {
            $postnumber = $_POST['postcode'];
        } else {
            if ($woocommerce->customer->shipping_postcode) {
                $postnumber = $woocommerce->customer->shipping_postcode;
            } else {
                $postnumber = $this->defaultPostcodes($country_code);
            }
        }

        $service_points = $this->get_service_points($postnumber, $country_code);
        $service_points['origin'] = $this->vc_geocode($_POST['address'], $_POST['postcode'], $_POST['country_code']);
        echo json_encode($service_points);

        die();
    }

    function get_service_points($postcode, $country_code = 'DK') {

        $consumer_id = get_option('vc_aino_consumer_id');

        $data = $this->api_call($consumer_id, $postcode, $country_code);

        $post_addresses = $this->build_response($postcode, $data, $country_code);

        return $post_addresses;
    }

    function default_postcodes($country_code) {
        if ($country_code == 'NO') {
            $postnumber = '0180';
        } else if ($country_code == "SE") {
            $postnumber = '11152';
        } else if ($country_code == "FI") {
            $postnumber = '00002';
        } else {
            $postnumber = '1000';
        }

        return $postnumber;
    }

    function curl_call($url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $jsonFile = curl_exec($curl);
        curl_close($curl);

        return json_decode($jsonFile);
    }



}
