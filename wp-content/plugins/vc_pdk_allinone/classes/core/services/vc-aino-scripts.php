<?php

class Vc_Aino_Scripts {

    function __construct() {
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

        add_filter('woocommerce_update_order_review_fragments', array($this, 'output_popup_filter'), 10, 1);
        add_action('woocommerce_review_order_before_submit', array($this, 'output_popup_action'));

        add_action('woocommerce_review_order_after_order_total', array($this, 'hide_methods'));
        add_action('woocommerce_after_shipping_rate', array($this, 'hide_methods'));
    }

    function enqueue_admin_scripts($hook) {
        if ('woocommerce_page_wc-settings' != $hook) {
            return;
        }

        wp_register_style('vc_aino_admin_css', plugins_url() . '/vc_pdk_allinone/assets/admin/css/styles.css', false, '1.0.1');
        wp_enqueue_style('vc_aino_admin_css');

//        wp_enqueue_script('vc_aino_admin_script', plugins_url() . '/vc_pdk_allinone/assets/admin/js/settings.js');
    }

    function enqueue_scripts() {
        if (is_checkout() && get_option('vc_aino_license_status') == 1) {
            wp_enqueue_style('aino_universe_style', plugins_url() . '/vc_pdk_allinone/assets/frontend/universe/css/style_red.css', false, '1.0.3');
            wp_enqueue_style('aino_universe_general', plugins_url() . '/vc_pdk_allinone/assets/frontend/css/general.css');

            wp_enqueue_script('aino_js_cookie', plugins_url() . '/vc_pdk_allinone/assets/frontend/js/lib/js.cookie.js', array('jquery'), '1.0.1');
            wp_enqueue_script('aino_facescroll', plugins_url() . '/vc_pdk_allinone/assets/frontend/universe/scripts/facescroll.js', array('aino_js_cookie'), '1.0.1');
            wp_enqueue_script('aino_touch_punch', plugins_url() . '/vc_pdk_allinone/assets/frontend/universe/scripts/jquery.ui.touch-punch.js', array('aino_facescroll'), '1.0.1');
            wp_enqueue_script('aino_specific_functions', plugins_url() . '/vc_pdk_allinone/assets/frontend/js/specific/universe.js', array('aino_touch_punch'), '1.0.1');
            wp_enqueue_script('aino_popup_functions', plugins_url() . '/vc_pdk_allinone/assets/frontend/universe/scripts/functions.js', array('aino_specific_functions'), '1.0.2');

            $js_params = $this->get_js_params();
            wp_enqueue_script('aino_wp_functions', plugins_url() . '/vc_pdk_allinone/assets/frontend/js/core/aino-wp.js', array('aino_popup_functions'), '1.0.1');
            wp_localize_script('aino_wp_functions', 'aino_params', $js_params);
        }
    }

    function hide_methods() {
        $use_universe = get_option('vc_aino_use_universe');

        if (!empty($use_universe) && $use_universe == 'yes') {
            echo '<script>';

            echo "if(jQuery('[id*=\"vconnect_postnord\"]:checked').size()>0){";
            echo "  jQuery('[id*=\"vconnect_postnord\"]:not(:checked)').parent().hide();";
            echo '} else {';
            echo "  jQuery('[id*=\"vconnect_postnord\"]:not(:first)').parent().hide();";
            echo '}';

            echo '</script>';
        }
    }

    function output_popup_action() {
        $popup = new Vc_Aino_Template();
        echo $popup->get_layout();
    }

    function output_popup_filter($array) {
        if (isset($array['.woocommerce-checkout-review-order-table'])) {
            $popup = new Vc_Aino_Template();
            $array['.aino-popup'] = $popup->get_layout();
        }

        return $array;
    }

    function get_translations() {
        return array(
            'main_button_text' => __('Vælg levering', 'vc-allinone'),
            'cancel_button' => __('Cancel', 'vc-allinone'),
            'new_zip' => __('Enter new zip code', 'vc-allinone'),
            'fiveDaysWeatherForecast' => __('5 Day Weather Forecast', 'vc-allinone'),
        );
    }

    function get_js_params() {
        $shippings = new Vc_Aino_Shipping_Methods();

        return array(
            'admin_ajax' => admin_url('admin-ajax.php'),
            'plugins_url' => plugins_url(),
            'methods_map' => $shippings->map_methods(),
            'translations' => $this->get_translations(),
            'use_universe' => get_option('vc_aino_use_universe'),
            'gmaps_api_key' => get_option('vc_aino_gmaps_key')
        );
    }

}
