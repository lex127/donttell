<?php

class Vc_Aino_Order {

    function __construct() {
        add_action('woocommerce_order_details_after_customer_details', array($this, 'order_details_after_customer_details'), '');
        add_action('woocommerce_admin_order_data_after_shipping_address', array($this, 'admin_order_data_after_shipping_address'), '');
        add_action('woocommerce_checkout_update_order_meta', array($this, 'checkout_update_order_meta'), 10, 2);
        add_action('woocommerce_checkout_process', array($this, 'checkout_process'));
        add_action('woocommerce_checkout_process', array($this, 'validate_information'));
        add_filter('woocommerce_email_customer_details_fields', array($this, 'add_email_information'), 10, 3);
        add_filter('wpo_wcpdf_billing_phone', array($this, 'add_pdf_information'), 10, 3);
    }

    function order_details_after_customer_details($order) {
        $order_meta = get_post_meta($order->id);

        if (isset($order_meta['_service_point_id'])) {
            $content = '<p class="none_set"><h3>Post Danmark Office</h3><table>';
            $content .= isset($order_meta['_service_point_id_name']) ? '<tr><td><strong>' . __('Service Point Name', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_name'][0] . '</td></tr>' : '';
            $content .= isset($order_meta['_service_point_id_address']) ? '<tr><td><strong>' . __('Address', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_address'][0] . '</td></tr>' : '';
            $content .= isset($order_meta['_service_point_id_city']) ? '<tr><td><strong>' . __('City', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_city'][0] . '</td></tr>' : '';
            $content .= isset($order_meta['_service_point_id_postcode']) ? '<tr><td><strong>' . __('Postcode', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_postcode'][0] . '</td></tr>' : '';
            $content .= isset($order_meta['_service_point_id_country']) ? '<tr><td><strong>' . __('CountryTag', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_country'][0] . '</td></tr>' : '';
            $content .= '</table></p>';

            echo $content;
        }
    }

    function admin_order_data_after_shipping_address($order) {
        $order_meta = get_post_meta($order->id);

        $content = '<h4>PostNord Details</h4>';
        $content .= '<p>';
        if (isset($order_meta['_service_point_id'])) {
            $content .= isset($order_meta['_service_point_id']) ? '<strong>Service Point ID:</strong>' . $order_meta['_service_point_id'][0] . '<br /><br />' : '';
            $content .= isset($order_meta['_service_point_id_name']) ? '<strong>' . __('Service Point Name', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_name'][0] . '<br /><br />' : '';
            $content .= isset($order_meta['_service_point_id_address']) ? '<strong>' . __('Address', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_address'][0] . '<br /><br />' : '';
            $content .= isset($order_meta['_service_point_id_city']) ? '<strong>' . __('City', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_city'][0] . '<br /><br />' : '';
            $content .= isset($order_meta['_service_point_id_postcode']) ? '<strong>' . __('Postcode', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_postcode'][0] . '<br /><br />' : '';
            $content .= isset($order_meta['_service_point_id_country']) ? '<strong>' . __('CountryTag', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_service_point_id_country'][0] . '<br /><br />' : '';
            $content .= isset($order_meta['_vc_flex_additional']) ? '<strong>' . __('CountryTag', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_vc_flex_additional'][0] . '<br />' : '';
        }

        // Handle Home and Commercial Options
        $content .= isset($order_meta['_vc_aino_arrival']) ? '<strong>' . __('Arrival', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_vc_aino_arrival'][0] . '<br /><br />' : '';
        $content .= isset($order_meta['_vc_aino_delivery_type']) ? '<strong>' . __('Delivery type', 'vc-allinone') . ':</strong></td><td>' . $order_meta['_vc_aino_delivery_type'][0] . '<br /><br />' : '';
        $content .= '</p>';

        echo "<script>jQuery('div.address').eq(1).append('" . $content . "')</script>";
    }

    function checkout_update_order_meta($order_id, $posted) {
        global $woocommerce;

        // Handle Pickup Info
        if (isset($woocommerce->checkout->posted['service_point_id'])) {
            update_post_meta($order_id, '_service_point_id', $woocommerce->checkout->posted['service_point_id']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_name'])) {
            update_post_meta($order_id, '_service_point_id_name', $woocommerce->checkout->posted['service_point_id_name']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_address'])) {
            update_post_meta($order_id, '_service_point_id_address', $woocommerce->checkout->posted['service_point_id_address']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_city'])) {
            update_post_meta($order_id, '_service_point_id_city', $woocommerce->checkout->posted['service_point_id_city']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_country'])) {
            update_post_meta($order_id, '_service_point_id_country', $woocommerce->checkout->posted['service_point_id_country']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_postcode'])) {
            update_post_meta($order_id, '_service_point_id_postcode', $woocommerce->checkout->posted['service_point_id_postcode']);
        }

        // Handle Home and Commercial Options
        if (isset($woocommerce->checkout->posted['vc_aino_arrival'])) {
            update_post_meta($order_id, '_vc_aino_arrival', $woocommerce->checkout->posted['vc_aino_arrival']);
        }

        if (isset($woocommerce->checkout->posted['vc_aino_delivery_type'])) {
            update_post_meta($order_id, '_vc_aino_delivery_type', $woocommerce->checkout->posted['vc_aino_delivery_type']);
        }

        // Handle Flex Additional
        if (isset($woocommerce->checkout->posted['vc_flex_additional'])) {

            $text = isset($woocommerce->checkout->posted['vc_free_text']) ? $woocommerce->checkout->posted['vc_flex_additional'] . " - \n" . $woocommerce->checkout->posted['vc_free_text'] : $woocommerce->checkout->posted['vc_flex_additional'];

            $my_post = array(
                'ID' => $order_id,
                'post_excerpt' => $text,
            );
            wp_update_post($my_post);
        }
    }

    function checkout_process() {
        global $woocommerce;

        if (!empty($_POST['vc_aino_service_point_id'])) {
            $woocommerce->checkout->posted['service_point_id'] = !empty($_POST['vc_aino_service_point_id']) ? $this->sanitize_numeric($_POST['vc_aino_service_point_id']) : '';
            $woocommerce->checkout->posted['service_point_id_name'] = !empty($_POST['vc_aino_service_point_id_name']) ? sanitize_text_field($_POST['vc_aino_service_point_id_name']) : '';
            $woocommerce->checkout->posted['service_point_id_address'] = !empty($_POST['vc_aino_service_point_id_address']) ? sanitize_text_field($_POST['vc_aino_service_point_id_address']) : '';
            $woocommerce->checkout->posted['service_point_id_city'] = !empty($_POST['vc_aino_service_point_id_city']) ? sanitize_text_field($_POST['vc_aino_service_point_id_city']) : '';
            $woocommerce->checkout->posted['service_point_id_country'] = !empty($_POST['vc_aino_service_point_id_country']) ? sanitize_text_field($_POST['vc_aino_service_point_id_country']) : '';
            $woocommerce->checkout->posted['service_point_id_postcode'] = !empty($_POST['vc_aino_service_point_id_postcode']) ? $this->sanitize_numeric($_POST['vc_aino_service_point_id_postcode'], 5) : '';
        }

        if (!empty($_POST['vc_flex_additional']) && $_POST['vc_flex_additional'] != 'Vælg placering') {
            $woocommerce->checkout->posted['vc_flex_additional'] = "Flex: \n" . sanitize_text_field($_POST['vc_flex_additional']);
        }
        if (!empty($_POST['vc_free_text'])) {
            $woocommerce->checkout->posted['vc_free_text'] = sanitize_text_field($_POST['vc_free_text']);
        }

        // Handle Home and Commercial Options
        if (!empty($_POST['vc_aino_arrival'])) {
            $woocommerce->checkout->posted['vc_aino_arrival'] = sanitize_text_field($_POST['vc_aino_arrival']);
        }

        if (!empty($_POST['vc_aino_write_place']) && $_POST['vc_aino_write_place'] !== '') {
            $woocommerce->checkout->posted['vc_aino_delivery_type'] = sanitize_text_field($_POST['vc_aino_write_place']);
        } else if (!empty($_POST['vc_aino_delivery_type'])) {
            $woocommerce->checkout->posted['vc_aino_delivery_type'] = sanitize_text_field($_POST['vc_aino_delivery_type']);
        }
    }

    function validate_information() {
        $chosen_methods = WC()->session->get('chosen_shipping_methods');
        $chosen_method = explode(':', $chosen_methods[0]);
        if ($chosen_method[0] == 'vconnect_postnord_dk_pickup') {
            if (!$_POST['vc_aino_service_point_id'] || $_POST['vc_aino_service_point_id'] == '') {
                wc_add_notice(__('Please choose pickup point to receive your package.'), 'error');
            }
        }
    }

    private function sanitize_numeric($val, $length = 10) {
        $safe_val = intval($val);
        if (!$safe_val) {
            $safe_val = '';
        }

        if (strlen($safe_val) > $length) {
            $safe_val = substr($safe_val, 0, $length);
        }

        return $safe_val;
    }

    function add_email_information($fields, $sent_to_admin, $order) {
        $order_meta = get_post_meta($order->id);

        if (!empty($order_meta['_service_point_id_name'])) {
            $fields['service_point_id_name'] = array(
                'label' => 'Service Point Name',
                'value' => $order_meta['_service_point_id_name'][0]
            );
        }
        if (!empty($order_meta['_service_point_id_address'])) {
            $fields['service_point_id_address'] = array(
                'label' => 'Address',
                'value' => $order_meta['_service_point_id_address'][0]
            );
        }
        if (!empty($order_meta['_service_point_id_city'])) {
            $fields['service_point_id_city'] = array(
                'label' => 'City',
                'value' => $order_meta['_service_point_id_city'][0]
            );
        }
        if (!empty($order_meta['_service_point_id_postcode'])) {
            $fields['service_point_id_postcode'] = array(
                'label' => 'Postcode',
                'value' => $order_meta['_service_point_id_postcode'][0]
            );
        }
        if (!empty($order_meta['_service_point_id_country'])) {
            $fields['service_point_id_country'] = array(
                'label' => 'CountryTag',
                'value' => $order_meta['_service_point_id_country'][0]
            );
        }

        return $fields;
    }

    function add_pdf_information($address) {
        global $wpo_wcpdf;

        $order_meta = get_post_meta($wpo_wcpdf->export->order->id);

        if (!empty($order_meta['_service_point_id_name'])) {
            $address .= '<br /><br /><strong>Afhentningssted:</strong><br />Service Point Name: ' . $order_meta['_service_point_id_name'][0] . '<br />';
        }
        if (!empty($order_meta['_service_point_id_address'])) {
            $address .= 'Address: ' . $order_meta['_service_point_id_address'][0] . '<br />';
        }
        if (!empty($order_meta['_service_point_id_city'])) {
            $address .= 'City: ' . $order_meta['_service_point_id_city'][0] . '<br />';
        }
        if (!empty($order_meta['_service_point_id_postcode'])) {
            $address .= 'Postcode: ' . $order_meta['_service_point_id_postcode'][0] . '<br />';
        }
        if (!empty($order_meta['_service_point_id_country'])) {
            $address .= 'CountryTag: ' . $order_meta['_service_point_id_country'][0] . '<br />';
        }

        return $address;
    }

}
