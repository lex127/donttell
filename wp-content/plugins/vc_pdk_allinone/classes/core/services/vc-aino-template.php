<?php

class Vc_Aino_Template {

    function get_layout(){
        $contents = $this->get_popup_contents();

        ob_start();
        include (AINO_PLUGIN_PATH . 'classes/core/layouts/universe.php');
        $res = ob_get_contents(); // get the contents of the output buffer
        ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering

        return $res;
    }

    function get_popup_content($method, $type, $method_info) {
        ob_start();
        include(AINO_PLUGIN_PATH . 'classes/specific/popups/' . $method . '/' . $type . '.php');
        $res = ob_get_contents(); // get the contents of the output buffer
        ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering

        return $res;
    }

    function get_popup_contents(){
        $contents = array(
            'tabs' => array(),
            'desktop' => array(),
            'mobile' => array(),
        );

        $shippings = new Vc_Aino_Shipping_Methods();

        $available_methods = $shippings->get_available_shipping_methods();
        $aino_methods_instance = new Vc_Aino_Shipping_Methods();
        $aino_methods = $aino_methods_instance->add_your_shipping_method(array());

        foreach($available_methods[0] as $method_info){
            $map = $shippings->map_methods();
            $method_id = $method_info->method_id;



            if (array_key_exists($method_id,$map)){
                $instance_raw = explode(':',$method_info->id);

                $method_instance = new $aino_methods[$method_id]($instance_raw[1]);

                $method_info->instance_id = $instance_raw[1];
                $method_info->transit_time = $method_instance->transit_time;
                $contents['tabs'][] = $this->get_popup_content($map[$method_id], 'tab', $method_info);
                $contents['desktop'][] = $this->get_popup_content($map[$method_id], 'desktop', $method_info);
                $contents['mobile'][] = $this->get_popup_content($map[$method_id], 'mobile', $method_info);
            }
        }

        return $contents;
    }

}