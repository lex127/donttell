var myData;

var UserChoice = function () {
    var choice = {};
};
UserChoice.prototype = {
    getChoice: function () {
        return this.choice;
    },
    setChoice: function (value) {
        this.choice = value;
    }
};

var userChoice = new UserChoice();
var InitAllInOneModule = function (points) {

    var init = function () {
        myData = typeof points !== 'undefined' ? points.origin : {};

        initMobileCollapse();
        openPopup();
        weatherInit();
        calendarInit();
        setActiveDelivery();
        setListDeliveryEvents();
        setTabs();
        setJQueryTabs();
        hideCalendar();
        if (typeof (points) !== "undefined") {
            var map = new AinoMap(myData, points);
        }
    };

    var initMobileCollapse = function () {
        var collapseBtn = jQuery('.aino-collapse');

        collapseBtn.each(function () {
            jQuery(this).unbind('click');
            jQuery(this).on('click', function () {
                var self = this;
                jQuery(self).next().animate({
                    height: "toggle"
                }, 'fast', function () {
                    jQuery(self).toggleClass('active');
                    var sibl = jQuery(self).parent().siblings();
                    sibl.each(function () {
                        jQuery(this).find('.aino-collapse-content').hide();
                        jQuery(this).find('.aino-collapse').removeClass('active');
                    });
                });
            });
        });
    };
    var openPopup = function () {
        var links = jQuery('a.vc-popup-trigger');
        links.each(function () {
            jQuery(this).on('click', function () {
                if (!jQuery(this).hasClass('vc-btn-disabled')) {
                    var target = jQuery(this).attr('href');
                    target = target.substr(1);

                    var popup = jQuery('[data-target="' + target + '"]');

                    if (jQuery(window).width() < 768) {
                        if (jQuery('.aino-cover').length === 0) {
                            jQuery('<div class="aino-cover"></div>').appendTo('body');
                        }

                        jQuery('.aino-container').removeClass('opened');
                        jQuery('.aino-container.mobile').addClass('opened').show();
                    } else {
                        if (jQuery('.aino-cover').length === 0) {
                            jQuery('<div class="aino-cover"></div>').appendTo('body');
                        }

                        jQuery('.aino-container').removeClass('opened');
                        jQuery(popup).find('.aino-container.desktop').addClass('opened').show();
                    }

                    selectTab();
                }
            });

            jQuery(window).resize(function () {
                var opened = jQuery('.aino-container.desktop:visible');
                if (jQuery(opened).length !== 0) {
                    var popup = jQuery(opened).parent('div');
                    if (jQuery(window).width() < 768) {
                        jQuery(popup).find(jQuery('.aino-container.desktop')).removeClass('opened').hide();
                        jQuery(popup).find(jQuery('.aino-container.mobile')).addClass('opened').show();
                    } else {
                        jQuery(popup).find(jQuery('.aino-container.mobile')).removeClass('opened').hide();
                        jQuery(popup).find(jQuery('.aino-container.desktop')).addClass('opened').show();
                    }
                }
            });
        });


    };
    var weatherInit = function () {

        var days = ["Søn", "Man", "Tirs", "Ons", "Tors", "fre", "Lør"];

        jQuery.getJSON("//api.openweathermap.org/data/2.5/forecast/daily?lat=" + myData.lat + "&lon=" + myData.lng + "&APPID=3c2e9f3ba04844da52d31ce5fc16928b",
                function (data) {
                    jQuery('.aino-weather').each(function () {
                        var self = this;
                        if (jQuery(this).find('ul li').length === 0) {
                            var container = jQuery(self).find('ul');
                            var icon;
                            var temp;
                            var listItem;
                            for (var x = 0; x < 5; x++) {
                                var date = (new Date().getDay() + x) <= 6 ? new Date().getDay() + x : (new Date().getDay() + x) - 7;
                                icon = jQuery('<div class="weather-icon"></div>').append('<img src="' + aino_params.plugins_url + '/vc_pdk_allinone/assets/frontend/universe/images/weather-icons/' + data.list[x].weather[0].icon.replace('n', 'd') + '.png">');
                                temp = jQuery('<div class="weather-temp-day"></div>')
                                        .append('<span class="aion-weather-day">' + days[date] + '</span>')
                                        .append('<span class="aion-weather-temp">' + (parseInt(data.list[x].temp.day) - 273) + '<sup>o</sup>C</span>');

                                listItem = jQuery('<li></li>');

                                icon.appendTo(listItem);
                                temp.appendTo(listItem);

                                listItem.appendTo(container);
                            }
                        }
                    });
                });


    };
    var calendarInit = function () {

        var calendars = jQuery('.aino-container.desktop .aino-calendar');

        calendars.each(function () {
            var parentD = jQuery(this).closest('.aino-inner-tabs');

            var tabD = parentD.find('a[href="#arrival"] label');

            if (tabD.length >= 1) {
                tabD.on('click', function () {
                    calendarAJAX(true, parentD);
                });
            } else {
                tabD = parentD.closest('.aino-tabs').find('.aino-smartdelivery-tab');
                tabD.on('click', function () {
                    calendarAJAX(true, parentD);
                });
            }
        });

        calendars = jQuery('.aino-container.mobile .aino-calendar');

        calendars.each(function () {
            var parentM = jQuery(this).closest('.aino-collapse-content');
            var tabM = parentM.siblings('a.aino-collapse');

            tabM.on('click', function () {
                calendarAJAX(false, parentM);
            });
        });
    };

    function calendarAJAX(onDesktop, self) {
        jQuery.ajax({
            url: "json/date.json",
            dataType: "json"
        }).done(function (data) {
            var availableDates = [];
            var timeslots;

            if (data) {
                timeslots = data.items;
                jQuery.each(timeslots, function (i, v) {
                    var bd = new Date(v.beginTs);
                    var ed = new Date(v.endTs);

                    availableDates.push(bd.getFullYear() + '' + bd.getMonth() + '' + bd.getDate());
                });

                var calendar = jQuery(self).find('.aino-calendar');
                var days = ["Sø", "Ma", "Ti", "On", "To", "Fr", "Lø"];
                var mounts = ["Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"];

                jQuery(calendar).datepicker({
                    dayNamesMin: days,
                    monthNames: mounts,
                    firstDay: 1,
                    dateFormat: 'yy-mm-dd',
                    beforeShowDay: function (date) {
                        var strDate = date.getFullYear() + '' + date.getMonth() + '' + date.getDate();
                        return [jQuery.inArray(strDate, availableDates) >= 0 ? true : false, ""];
                    },
                    onSelect: function (date, obj) {
                        if (onDesktop) {
                            calendarDesktop(calendar, timeslots, date, mounts);
                        } else {
                            calendarMobile(calendar, timeslots, date, mounts);
                        }
                    }
                });
            }
        });
    }
    function calendarMobile(calendar, timeslots, date, mounts) {
        var calndar_date = jQuery(calendar).find('.ui-state-active');

        var sday = calndar_date.html() + '.- ' +
                mounts[calndar_date.parent('td').attr('data-month')] + ' ' +
                calndar_date.parent('td').attr('data-year');

        jQuery(calendar).siblings('.aino-delivery-time-picker').find('li').each(function () {
            jQuery(this).remove();
        });

        jQuery.each(timeslots, function (i, v) {
            var bd = new Date(v.beginTs);
            var ed = new Date(v.endTs);
            var dateR =
                    bd.getFullYear() + (bd.getMonth() + 1 < 10 ? '-0' : '-') +
                    (bd.getMonth() + 1) + '-' +
                    bd.getDate();

            if (dateR === date) {
                var list = jQuery('<li></li>')
                        .append(jQuery('<label class="aino-input aino-checkbox"></label>')
                                .append(jQuery('<input type="radio" name="delivery" value="">'))
                                .append(jQuery('<span class="aino-indicator"></span>'))
                                .append(jQuery('<span class="label-text">' +
                                        bd.getHours() + ':' + (bd.getMinutes() < 10 ? '0' + bd.getMinutes() : bd.getMinutes()) + ' - ' +
                                        ed.getHours() + ':' + (ed.getMinutes() < 10 ? '0' + ed.getMinutes() : ed.getMinutes()) +
                                        '</span>')
                                        .prepend(jQuery('<div class="mounth-day">' + bd.getDate() + ' ' + mounts[bd.getMonth()] + '</div>'))
                                        .append(jQuery('<div class="pull-right">49,-kr</div>'))))
                        .on('click', function () {
                            var parentTarget = jQuery(this).parents('.column-2').attr('data-target');
                            jQuery(this).addClass('active');
                            jQuery(this).siblings('li').removeClass('active');
                            jQuery(this).closest('.aino-collapse-content').siblings(".aino-collapse").html(jQuery(this).find('label .label-text').html());

                            var prevChoise = userChoice.getChoice();
                            var newChoise = {};
                            for (var prop in prevChoise) {
                                newChoise[prop] = prevChoise[prop];
                            }

                            newChoise.date = dateR;
                            newChoise.time = bd.getHours() + ':' + (bd.getMinutes() < 10 ? '0' + bd.getMinutes() : bd.getMinutes());

                            userChoice.setChoice(newChoise);

                        });

                list.appendTo(jQuery(calendar).siblings('.aino-delivery-time-picker'));
            }
        });
    }
    function calendarDesktop(calendar, timeslots, date, mounts) {
        var deliveryTime;

        var ifIs = jQuery(calendar).closest('.aino-smart-delivery');

        if (ifIs.length >= 1) {
            deliveryTime = jQuery(calendar).closest('.aino-inner-tabs').find('.aino-delivery-time-picker');

            jQuery(calendar).closest('.aino-inner-tabs').find('.column-2').show();
        } else {
            deliveryTime = jQuery(calendar).closest('.aino-calendar-holder').siblings('.delivery-time');
            jQuery(calendar).addClass('hidden');

            jQuery(calendar).siblings('.toggle-calendar').show();
        }


        jQuery(deliveryTime).find('li').each(function () {
            this.remove();
        });

        deliveryTime.show();

        jQuery.each(timeslots, function (i, v) {
            var bd = new Date(v.beginTs);
            var ed = new Date(v.endTs);
            var dateR =
                    bd.getFullYear() + (bd.getMonth() + 1 < 10 ? '-0' : '-') +
                    (bd.getMonth() + 1) + '-' +
                    bd.getDate();
            var sday = bd.getDate() + ' ' + mounts[bd.getMonth()] + ' ' + bd.getFullYear();
            if (dateR === date) {
                var list = jQuery('<li></li>')
                        .append(jQuery('<label class="aino-input aino-checkbox"></label>')
                                .append(jQuery('<input type="radio" name="delivery-time" value="">'))
                                .append(jQuery('<span class="aino-indicator"></span>'))
                                .append(jQuery('<span class="label-text">' +
                                        bd.getHours() + ':' + (bd.getMinutes() < 10 ? '0' + bd.getMinutes() : bd.getMinutes()) + ' - ' +
                                        ed.getHours() + ':' + (ed.getMinutes() < 10 ? '0' + ed.getMinutes() : ed.getMinutes()) +
                                        '</span>')
                                        .prepend(jQuery('<div class="mounth-day">' + bd.getDate() + ' ' + mounts[bd.getMonth()] + '</div>'))
                                        .append(jQuery('<div class="pull-right">49,-kr</div>'))))
                        .on('click', function () {
                            var parentTarget = jQuery(this).parents('.column-2').attr('data-target');

                            jQuery(this).siblings('li').removeClass('active');
                            if (jQuery(this).parents('.aino-inner-tabs').find("[href=#" + parentTarget + "] span").length >= 1) {
                                jQuery(calendar).siblings('.toggle-calendar')
                                        .html(sday);
                                jQuery(this).parents('.aino-inner-tabs').find("[href=#" + parentTarget + "] span")
                                        .html(jQuery(this).find('label .label-text').html());
                            } else {
                                jQuery(calendar).siblings('.toggle-calendar')
                                        .html('TIME: kl.' + jQuery(this).find('label .label-text').html());

                                jQuery(calendar).siblings('.toggle-calendar').show();
                            }
                            jQuery(this).closest('.aino-tabs-body').removeClass('active');
                            jQuery(this).closest('.aino-tabs-body').find('.column-2').hide();
                            jQuery(this).addClass('active');

                            var prevChoise = userChoice.getChoice();
                            var newChoise = {};
                            for (var prop in prevChoise) {
                                newChoise[prop] = prevChoise[prop];

                            }

                            newChoise.date = dateR;
                            newChoise.time = bd.getHours() + ':' + (bd.getMinutes() < 10 ? '0' + bd.getMinutes() : bd.getMinutes());
                            userChoice.setChoice(newChoise);
                        });



                if (deliveryTime.find('.aino-delivery-time-picker').length >= 1) {
                    deliveryTime.find('.aino-delivery-time-picker')
                            .alternateScroll({'vertical-bar-class': 'nicescroll-rails', 'hide-bars': false});

                    list.appendTo(deliveryTime.find('.aino-delivery-time-picker .alt-scroll-content'));
                    jQuery(calendar).siblings('.toggle-calendar')
                            .html(sday);
                } else {
                    deliveryTime
                            .alternateScroll({'vertical-bar-class': 'nicescroll-rails', 'hide-bars': false});

                    list.appendTo(deliveryTime.find('.alt-scroll-content'));
                }
            }
        });
    }

    var hideCalendar = function () {
        jQuery('.aino-calendar-holder .toggle-calendar').on('click', function () {
            if (jQuery(this).closest('.aino-smart-delivery').length === 0) {
                jQuery('.aino-calendar-holder .aino-calendar').removeClass('hidden');
                jQuery(this).hide();
                jQuery(this).closest('.aino-calendar-holder').siblings('.delivery-time').hide();
            } else {
                jQuery(this).closest('.aino-inner-tabs').find('.column-2').show();
            }
        });
    };
    var submitPopup = function () {

        jQuery('.aino-cover').remove();
        jQuery('.aino-container').hide();

        return userChoice.getChoice();
    };
    var closePopup = function () {
        jQuery('.aino-button[role=close]').on('click', function (e) {
            e.preventDefault();
            var ele = jQuery('.aino-container .aino-input').closest('li');
            ele.each(function () {
                jQuery(this).removeClass('active');
                jQuery(this).find('input').prop('checked', false);
            });

            userChoice.setChoice(null);
            return userChoice.getChoice();
        });

        return null;
    };

    function setJQueryTabs() {
        jQuery('.aino-deliver-to').each(function () {
            jQuery(this).find('a').on('click', function () {
                jQuery(this).closest('li').addClass('ui-state-active');
                jQuery(this).closest('li').siblings().removeClass('ui-state-active');
                var target = jQuery(this).attr('href');
                target = target.substr(1);
                var activeTab = jQuery(this).closest('ul').siblings('.aino-tabs-body').find('[data-target="' + target + '"]');
                activeTab.show();
                activeTab.siblings().hide();
            });
        });
    }
    function setTabs() {
        var tabs = jQuery('.aino-inner-tabs');

        tabs.each(function () {
            var self = this;
            var btn = jQuery(this).find('[role=tab]');
            var target;
            btn.each(function () {
                jQuery(this).unbind('click').on('click', function () {
                    target = jQuery(this).attr('href') ? jQuery(this).attr('href') : jQuery(this).attr('data-target');
                    target = target.substr(1);
                    if (jQuery(self).find('[data-target=' + target + ']').css('display') === 'none') {
                        setActive(self, this, target);
                    } else {
                        setDeactive(self, target);
                        jQuery(this).parent('li').removeClass('active');
                    }

                    jQuery(self).find('[data-target=' + target + ']').siblings().hide();
                });
            });

            var close = jQuery(this).find('.close');
            close.each(function () {
                jQuery(this).on('click', function () {
                    setDeactive(self, target);
                    jQuery(btn).parent('li').removeClass('active');
                });
            });

            var closeInner = jQuery(this).find('.close-inner');
            closeInner.each(function () {
                jQuery(this).on('click', function () {
                    jQuery(this).closest('.delivery-time').hide();
                    jQuery(this).closest('.delivery-time')
                            .siblings('.aino-calendar-holder')
                            .find('.toggle-calendar').hide();
                    jQuery(this).closest('.delivery-time')
                            .siblings('.aino-calendar-holder')
                            .find('.aino-calendar').removeClass('hidden');
                });
            });
        });

        function setActive(tabs, ele, target) {
            jQuery(tabs).find('[data-target=' + target + ']').parent().siblings('ul').removeClass('active');
            jQuery(tabs).find('[data-target=' + target + ']').show();
            jQuery(tabs).find('[data-target=' + target + ']').parent().addClass('active');
            jQuery(ele).parent('li').addClass('active');
            jQuery(ele).parent('li').siblings().removeClass('active');
        }

        function setDeactive(tabs, target) {
            jQuery(tabs).find('[data-target=' + target + ']').parent().siblings('ul').addClass('active');
            jQuery(tabs).find('[data-target=' + target + ']').hide();
            jQuery(tabs).find('[data-target=' + target + ']').parent().removeClass('active');
        }
    }
    function setActiveDelivery() {
        var ele = jQuery('.aino-container .column-2 .aino-input').closest('li');
        ele.each(function () {
            jQuery(this).unbind('click').on('click', function () {

                if (jQuery(this).find('input').val() === 'Other place')
                {
                    closeColumn2AtPick(this, ele);

                    var textarea = jQuery(this).parents('.aino-inner-tabs').find('.aino-textarea-holder textarea');
                    textarea.prop('disabled', false).closest('.aino-textarea-holder').show();

                    textarea.on('focus', function () {
                        jQuery(this).attr('placeholder', '');
                    });

                    textarea.on('focusout', function () {
                        jQuery(this).attr('placeholder', 'Skriv placering');
                    });

                    textarea.on('keyup focus', function () {
                        userChoice.setChoice(jQuery(textarea).val() ? jQuery(textarea).val() : userChoice.getChoice());
                    });
                } else if (jQuery(this).find('input').val() === 'Smart Delivery') {
                    jQuery(ele).removeClass('active');
                    jQuery(this).addClass('active');

                    jQuery(ele).find('input').attr('checked', false);
                    jQuery(this).find('input').attr('checked', true);

                    jQuery(this).closest('.column-2').find('.aino-calendar').removeClass('hidden');
                } else {
                    var textarea = jQuery(this).parents('.aino-inner-tabs').find('.aino-textarea-holder textarea');
                    textarea.prop('disabled', true)
                    closeColumn2AtPick(this, ele);
                    jQuery(this).closest('.column-2').find('.aino-calendar').addClass('hidden');
                    userChoice.setChoice(jQuery(this).find('input').val() ? jQuery(this).find('input').val() : userChoice.getChoice());
                }
            });
        });

        var bagOnDoor = jQuery('a[role="tab"] > label');

        bagOnDoor.on('click', function () {
            var labelFor = jQuery(this).attr('for');

            userChoice.setChoice(jQuery(this).parents('.aino-inner-tabs').find(labelFor).val() ? jQuery(this).parents('.aino-inner-tabs').find(labelFor).val() : userChoice.getChoice());
        });
    }
    function setListDeliveryEvents() {

        jQuery('.aino-tabs-body .column-2 .aino-nav > li').each(function () {
            jQuery(this).on('click', function () {
                var parentTarget = jQuery(this).parents('.column-2').attr('data-target');

                if (jQuery(this).find('.aino-nav').length !== 0) {
                    jQuery(this).unbind('click');
                }

                jQuery(this).closest('.aino-inner-tabs').find('[href="#' + parentTarget + '"] span').html(jQuery(this).find('label .label-text').html());
            });

            var otherPlace = jQuery(this).find(jQuery('input[value="Other place"]').closest('ul').find('li'));
            otherPlace.each(function () {
                var textarea = jQuery(this).parents('.aino-inner-tabs').find('.aino-textarea-holder');
                var item = jQuery(this).find('input');
                if (item.val() === 'Other place') {
                    jQuery(this).on('click', function () {
                        textarea.prop('disabled', false).show();
                    });
                } else {
                    jQuery(this).on('click', function () {
                        textarea.hide().prop('disabled', true);
                    });
                }
            });
        });
    }
    function closeColumn2AtPick(self, ele) {
        jQuery(self).closest('.aino-tabs-body').removeClass('active');
        jQuery(self).closest('.aino-tabs-body').find('.column-2').hide();

        jQuery(self).closest('.column-2').find('li').removeClass('active');
        jQuery(self).addClass('active');

        jQuery(self).closest('.column-2').find('li input').attr('checked', false);
        jQuery(self).find('input').attr('checked', true);
    }

    return{
        user: userChoice.getChoice(),
        init: init(),
        submit: submitPopup(),
        close: closePopup()
    };
};

var AinoMap = function (myData, points) {
    var directionsDisplay;
    var savedPin;
    var map;
    
    var sortedData = sortArrayData(points);
    jQuery('.vc-btn-disabled').removeClass('vc-btn-disabled');
    if (jQuery(window).width() < 768) {
        setListLocations(sortedData);
        setMobileActiveDelivery(sortedData);
        mobileMoreLocations(sortedData);
        mobileLocationList(sortedData);
    } else {
        var links = jQuery('a.vc-popup-trigger');
        links.each(function () {
            var target = jQuery(this).attr('href');
            target = target.substr(1);
            var popup = jQuery('[data-target="' + target + '"]');

            if (jQuery(this).find('.aino-deliver-to > li').length <= 1) {
                jQuery(this).on('click', function () {
                    createMapOnClick(popup, myData, sortedData, map);
                });
            }

            jQuery(popup).find('.aino-office-tab').on('click', function () {
                createMapOnClick(popup, myData, sortedData, map);
            });

        });
    }

    jQuery(window).resize(function () {
        if (jQuery(window).width() < 768) {
            if (jQuery('.aino-container.desktop').css('display') === 'block') {
                jQuery('.aino-cover').css('display', 'none');
                jQuery('.aino-container.desktop').css('display', 'none');
                jQuery('.aino-container.mobile').css('display', 'block');
            }

            setListLocations(sortedData);
            mobileLocationList(sortedData);
        } else {
            if (jQuery('.aino-container.mobile').css('display') === 'block') {
                jQuery('.aino-cover').css('display', 'block');
                jQuery('.aino-container.mobile').css('display', 'none');
                jQuery('.aino-container.desktop').css('display', 'block');
            }
        }
            var links = jQuery('a.vc-popup-trigger');
            links.each(function () {
                var target = jQuery(this).attr('href');
                target = target.substr(1);
                var popup = jQuery(this).parents('table').siblings('[data-target=' + target + ']');

                jQuery(popup).find('.aino-office-tab').on('click', function () {
                    jQuery(popup).find(".address-near-you")
                            .alternateScroll({'vertical-bar-class': 'nicescroll-rails', 'hide-bars': false});

                    jQuery(popup).find('.aino-map').show('fast', function () {

                        if (!map) {
                            map = initMap(myData, popup);
                            setMapContent(sortedData, map, myData, jQuery(this).attr('id'));

                            jQuery('.post-code-input input').val(myData.postcode);
                        }
                    });
                });
            });
        
    });




    createMapOnClick = function (popup, myData, sortedData, map) {
        jQuery(popup).find('.aino-map').show('fast', function () {
            map = initMap(myData, jQuery(popup).attr('data-target'));
            setMapContent(sortedData, map, myData, jQuery(popup).attr('data-target'));
            jQuery('.post-code-input input').val(myData.postcode);
        });

    };



    function sortArrayData(data) {
        
        var addresses = [];
        var city = [];
        var countryCode = [];
        var postcode = [];
        var close = [];
        var close_sat = [];
        var lat = [];
        var lng = [];
        var name = [];
        var distance = [];
        var number = [];
        var opening = [];
        var opening_sat = [];
        var rating = [];
        var servicePointId = [];

        var sortedData = {
            addresses: addresses,
            city: city,
            countryCode: countryCode,
            postcode: postcode,
            close: close,
            close_sat: close_sat,
            lat: lat,
            lng: lng,
            name: name,
            distance: distance,
            number: number,
            opening: opening,
            opening_sat: opening_sat,
            rating: rating,
            servicePointId: servicePointId
        };

        var index = 0;
        for (var i = 0; i < data.servicePointId.length; i++) {
            var minValue = Number.MAX_VALUE;
            for (var x = 0; x < data.servicePointId.length; x++) {

                var dist = data.distance[index];
                if (minValue > dist && sortedData.servicePointId ? sortedData.servicePointId.indexOf(data.servicePointId[x]) === -1 : false) {
                    minValue = dist;
                    index = x;
                    
                    
                }
                
            }

            addresses.push(data.addresses[index]);
            city.push(data.city[index]);
            countryCode.push(data.countryCode[index]);
            postcode.push(data.postcode[index]);
            close.push(data.close[index]);
            close_sat.push(data.close_sat[index]);
            lat.push(data.lat[index]);
            lng.push(data.lng[index]);
            name.push(data.name[index]);
            distance.push(data.distance[index]);
            number.push(data.number[index]);
            opening.push(data.opening[index]);
            opening_sat.push(data.opening_sat[index]);
            rating.push(3);
            servicePointId.push(data.servicePointId[index]);

            sortedData = {
                addresses: addresses,
                city: city,
                countryCode: countryCode,
                postcode: postcode,
                close: close,
                close_sat: close_sat,
                lat: lat,
                lng: lng,
                name: name,
                distance: distance,
                number: number,
                opening: opening,
                opening_sat: opening_sat,
                rating: rating,
                servicePointId: servicePointId
            };
        }

        return sortedData;
    }

    function setListLocations(data) {
        var container = jQuery('.aino-result-pickup .aino-nav');
        var len = data.name.length < 6 ? data.name.length : 6;
        jQuery('.address-near-you li').remove();
        var empty = jQuery('.address-near-you').find('li').length === 0;
        
        console.log(data);
        if (empty) {
            for (var x = 0; x < len; x++) {
                var listItem = jQuery('<li class="aino-result-list"></li>');
                var shell = jQuery('<div class="cf"></div>')
                        .append(jQuery('<label class="aino-input aino-checkbox"></label>')
                                .append(jQuery('<input type="radio" name="delivery" value="">'))
                                .append(jQuery('<span class="aino-indicator"></span>'))
                                .append(jQuery('<div></div>')
                                        .append(jQuery('<div class="aino-distance"><span>' + data.distance[x].toFixed(1) + ' km</span></div>'))
                                        .append(jQuery('<div class="aino-title">' + data.name[x] + '</div>'))
                                        .append(jQuery('<div class="aino-address">' + data.addresses[x] + ' <br />' + data.number[x] + ' </div>'))
                                        .append(jQuery('<div class="aino-open-hours">Open hours: ' + data.opening[x] + ' - ' + data.close[x] + '</div>'))
                                        ));

                shell.appendTo(listItem.appendTo(container));
            }
        }


        if (jQuery('.aino-container .see-more-locations').length <= 0) {
            jQuery('<div class="see-more-locations">See more ...</div>').insertAfter(container);
        }

//        setMobileActiveDelivery();
        mobileMoreLocations(data);
    }

    function mobileMoreLocations(data) {
        var container = jQuery('.aino-result-pickup');
        var len;
        jQuery('.aino-container .see-more-locations').unbind('click').on('click', function () {
            var len_selector = jQuery('.aino-result-list').length;
            len = len_selector + 6 < data.name.length ? len_selector + 6 : data.name.length;
            for (var x = len_selector; x < len; x++) {

                var listItem = jQuery('<li class="aino-result-list"></li>');
                var shell = jQuery('<div class="cf"></div>')
                        .append(jQuery('<label class="aino-input aino-checkbox"></label>')
                                .append(jQuery('<input type="radio" name="delivery" value="">'))
                                .append(jQuery('<span class="aino-indicator"></span>'))
                                .append(jQuery('<div></div>')
                                        .append(jQuery('<div class="aino-distance"><span>' + data.distance[x].toFixed(1) + ' km</span></div>'))
                                        .append(jQuery('<div class="aino-title">' + data.name[x] + '</div>'))
                                        .append(jQuery('<div class="aino-address">' + data.addresses[x] + ' <br />' + data.number[x] + ' </div>'))
                                        .append(jQuery('<div class="aino-open-hours">Open hours: ' + data.opening[x] + ' - ' + data.close[x] + '</div>'))
                                        ));

                shell.appendTo(listItem.appendTo(container));
            }

            if (len >= data.name.length) {
                jQuery('.aino-container .see-more-locations').remove();
            }

            setMobileActiveDelivery();
            mobileLocationList(data);
        });
    }

    function setMobileActiveDelivery() {
        var ele = jQuery('.aino-container .aino-collapse-content .aino-input').closest('li');
        var choice = {};
        ele.each(function () {
            jQuery(this).unbind('click').on('click', function () {
                jQuery(this).siblings('li').find('input').prop("checked", false).attr("checked", false);

                jQuery(ele).removeClass('active');
                jQuery(this).addClass('active');

                var val = jQuery(this).find('input').val();
                var textarea = jQuery(this).closest('.aino-collapse-content').siblings('textarea');
                if (val) {
                    jQuery(this).closest('.aino-collapse-content')
                            .siblings('.aino-collapse')
                            .html(jQuery(this).find('.label-text').html() ? jQuery(this).find('.label-text').html() : jQuery(this).find('input').val());

                    if (val === 'Other place') {
                        jQuery(textarea).prop('disabled', false).show();
                    } else {
                        jQuery(textarea).hide().prop('disabled', true);
                    }
                }

                var deliveryOption = '';
                if (jQuery(this)
                        .closest('.aino-collapse-content')
                        .siblings('.aino-tab-sub-title').length > 0) {
                    deliveryOption = jQuery(this)
                            .closest('.aino-collapse-content')
                            .siblings('.aino-tab-sub-title')
                            .html()
                            .toString()
                            .trim();
                } else {
                    var data_method = jQuery(this)
                            .closest('.aino-collapse-content')
                            .siblings('.aino-collapse')
                            .attr('data-method');

                    deliveryOption = data_method.substring(1);
                    deliveryOption = deliveryOption.replace("-", " ");
                    deliveryOption = deliveryOption.substr(0, 1).toUpperCase() + deliveryOption.substr(1);
                }

                choice[deliveryOption] = jQuery(this).find('input').val();

                userChoice.setChoice(choice);

                jQuery(textarea).on('keyup', function () {
                    userChoice.setChoice(jQuery(textarea).val());
                });
            });
        });
    }

    function mobileLocationList(data) {
        jQuery('.aino-result-pickup li').each(function (index) {
            jQuery(this).on('click', function () {
                var choise = {
                    id: data.servicePointId ? data.servicePointId[index] : '',
                    address: data.addresses ? data.addresses[index] : '',
                    name: data.name ? data.name[index] : '',
                    country: data.country ? data.country[index] : '',
                    city: data.city ? data.city[index] : '',
                    countryCode: data.countryCode ? data.countryCode[index] : '',
                    zip: data.postcode ? data.postcode[index] : ''
                };

                userChoice.setChoice(choise);
            });
        });


    }

    function initMap(myData, target) {
        // Create a map object and specify the DOM element for display.

        var aino_map = '[data-target=' + target + ']' + ' .aino-map';

        var map = new google.maps.Map(jQuery(aino_map)[0], {
            center: {lat: myData.lat, lng: myData.lng},
            scrollwheel: true,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        return map;
    }

    function setMapContent(data, map, myData, target) {
        var myLatLng;
        var markers = [];
        var bounds = new google.maps.LatLngBounds();
        var labels = makeMarkerLabels(data);
        var image = {
            url: aino_params.plugins_url + '/vc_pdk_allinone/assets/frontend/universe/images/temp.png',
            size: new google.maps.Size(20, 40)
        };

        userChoice.setChoice({id: "2638", address: "Taastrup Torv 18", name: "Posthus Kvickly", country: "", city: "", zip: ""});
        setListLocations(data);
        initAddresses(data, myData);

        closePopup(markers, labels);

        for (var i = 0; i < data.name.length; i++) {
            myLatLng = new google.maps.LatLng(data.lat[i], data.lng[i]);
            bounds.extend(myLatLng);

            var marker = new MarkerWithLabel({
                map: map,
                icon: image,
                zIndex: i,
                labelContent: (i + 1).toString(),
                labelClass: "aino-label",
                labelAnchor: new google.maps.Point(14, 46),
                position: {lat: data.lat[i], lng: data.lng[i]},
                labelStyle: {'display': 'block', 'height': '50px'}
            }).addListener('click', function () {
                setEventsToMarkers(data, map, myData, markers, this, labels, target);
            });

            if (markers.indexOf(marker) === -1) {
                var x = 0;
                for (var key in marker) {
                    var value = marker[key];
                    if (x === 0) {
                        markers.push(value);
                    }
                    x++;
                }
            }
        }

        setDirectionsFromList(map, myData, data, markers, labels, target);

        if (typeof userChoice.getChoice() === 'object' && userChoice.getChoice() !== null) {
            var index = 0;
            for (var x = 0; x < data.name.length; x++) {

                var selectedPoint = typeof Cookies.get('service_point_id') !== 'undefined' ? Cookies.get('service_point_id') : '';

                if (selectedPoint === data.servicePointId[x]) {
                    index = x;
                    break;
                }
            }

            directionsDisplay = setActiveMapComponent(data, map, myData, markers[index], labels[index], index, target);
        }

        jQuery('.aino-map-container .directions-display-type > button').each(function () {
            jQuery(this).unbind('click').on('click', function () {
                var type = jQuery(this).attr('role');
                var index = 0;
                for (var x = 0; x < data.name.length; x++) {
                    if (userChoice.getChoice().id === data.servicePointId[x]) {
                        index = x;
                        break;
                    }
                }
                directionsDisplay = showDirections(map, myData, data.lat[index], data.lng[index], type);
            });
        });

        map.fitBounds(bounds);

    }

    function initAddresses(data, myData) {
        var container = jQuery('.address-near-you .aino-nav');
        var len = container.find('li').length === 0 ? data.addresses.length : 0;

        for (var x = 0; x < len; x++) {
            var rating = jQuery('<div class="aino-rating"></div>');
            for (var i = 0; i < 5; i++) {
                if (data.rating[x] > i) {
                    jQuery('<span class="aino-star active"></span>').appendTo(rating);
                } else {
                    jQuery('<span class="aino-star"></span>').appendTo(rating);
                }
            }

            var listItem = jQuery('<li></li>');
            var shell = jQuery('<div class="cf"></div>')
                    .append(jQuery('<label class="aino-input aino-checkbox"></label>')
                            .append(jQuery('<input type="radio" name="delivery" value="">'))
                            .append(jQuery('<span class="aino-indicator"></span>'))
                            .append(jQuery('<div class="cf"></div>')
                                    .append(jQuery('<div class="aino-distance"><span>' + data.distance[x].toFixed(1) + ' km</span></div>'))
                                    .append(jQuery('<div class="aino-index">' + (x + 1) + '</div>'))
                                    .append(jQuery('<div class="aino-title">' + data.name[x] + '</div>'))
//                                    .append(rating)
                                    .append(jQuery('<div class="aino-address">' + data.addresses[x] + ' <br />' + data.number[x] + ' </div>'))
                                    ));

            shell.appendTo(listItem.appendTo(container));
        }
    }

    function setDirectionsFromList(map, myData, data, markers, labels, target) {
        var addresses = jQuery('[data-target=' + target + ']').find('.address-near-you li');
        var cutOffset = jQuery(addresses[0]).offset().top;
        var lastOffset = (jQuery(addresses[addresses.length - 1]).offset().top);

        addresses.each(function (index) {
            var offset = jQuery(this).offset().top - cutOffset;
            jQuery(this).unbind('click').on('click', function () {

                jQuery('.aino-container .aino-input').closest('li').removeClass('active');
                resetMapComponents(map, markers, labels);
                directionsDisplay =
                        setActiveMapComponent(data, map, myData, markers[index], labels[index], index, target);
                closeBtnLabel(map, labels[index]);

                setScrollToElement(target, offset, lastOffset, index);
                return false;
            });
        });

        return directionsDisplay;
    }

    function setEventsToMarkers(data, map, myData, markers, marker, labels, target) {
        var iwIndex = parseInt(marker.get('labelContent')) - 1;
        resetMapComponents(map, markers, labels);
        directionsDisplay = setActiveMapComponent(data, map, myData, marker, labels[iwIndex], iwIndex, target);

        closeBtnLabel(map, labels[iwIndex]);
        var addresses = jQuery('[data-target=' + target + ']').find('.address-near-you li');
        var cutOffset = jQuery(addresses[0]).offset().top;
        var lastOffset = (jQuery(addresses[addresses.length - 1]).offset().top);

        var offset = [];
        for (var x = 0; x < addresses.length; x++) {
            offset.push(jQuery(addresses[x]).offset().top - cutOffset);
        }

        setScrollToElement(target, offset[iwIndex], lastOffset, iwIndex);

        return directionsDisplay;
    }

    function closeBtnLabel(map, label) {
        var btn = jQuery('.aino-pin-label-close');
        jQuery(btn[0]).on('click', function () {
            label.close(map, this);
        });
    }

    function resetMapComponents(map, markers, label, target) {

        if (directionsDisplay !== null && myData.type === 'ROOFTOP' && myData.partial_match !== 1) {
            directionsDisplay.setMap(null);
            directionsDisplay = null;
        }

        var addresses = jQuery('[data-target' + target + ']').find('.address-near-you li');
        for (var x = 0; x < markers.length; x++) {

            markers[x].set('labelClass', "aino-label");
            markers[x].set('zIndex', parseInt(markers[x].get('labelContent')) - 1);
            label[x].close(map, markers[x]);
            jQuery(addresses[x]).removeClass('active');
        }
    }

    function setActiveMapComponent(data, map, myData, marker, label, index, target) {
        var addresses = jQuery('[data-target=' + target + ']').find('.address-near-you li');
        marker.set('zIndex', parseInt(marker.get('labelContent')) + data.lat.length);
        marker.set('labelClass', "aino-label active");
        label.open(map, marker);
        jQuery(addresses[index]).siblings('li').removeClass('active');
        jQuery(addresses[index]).addClass('active');

        var choice = {
            id: data.servicePointId ? data.servicePointId[index] : '',
            address: data.addresses ? data.addresses[index] : '',
            name: data.name ? data.name[index] : '',
            country: data.country ? data.country[index] : '',
            city: data.city ? data.city[index] : '',
            countryCode: data.countryCode ? data.countryCode[index] : '',
            zip: data.postcode ? data.postcode[index] : ''
        };
        var prevChoise = userChoice.getChoice();

        choice.date = prevChoise.date;
        choice.time = prevChoise.time;

        userChoice.setChoice(choice);

        savedPin = {
            index: index
        };

        if (myData.type === 'ROOFTOP' && myData.partial_match !== 1) {
            return showDirections(map, myData, data.lat[index], data.lng[index], 'DRIVING');
        } else {
            return false;
        }

    }

    function showDirections(map, myData, lat, lng, type) {

        if (directionsDisplay !== null && typeof directionsDisplay !== 'undefined') {
            directionsDisplay.setMap(null);
            directionsDisplay = null;
        }

        directionsDisplay = new google.maps.DirectionsRenderer({
            map: map,
            suppressMarkers: true
        });

        // Set destination, origin and travel mode.
        var request = {
            destination: {lat: lat, lng: lng},
            origin: {lat: myData.lat, lng: myData.lng},
            travelMode: google.maps.TravelMode[type]
        };


        // Pass the directions request to the directions service.
        var directionsService = new google.maps.DirectionsService();
        directionsService.route(request, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                // Display the route on the map.
                directionsDisplay.setDirections(response);
            }

            google.maps.event.addListenerOnce(map, 'zoom_changed', function () {
                var oldZoom = map.getZoom();

                if (oldZoom > 14) {
                    map.setZoom(oldZoom - 1);
                } else {
                    map.setZoom(oldZoom);
                }
            });
        });

        return directionsDisplay;
    }

    function makeMarkerLabels(data) {
        var iw;
        var labels = [];
        for (var i = 0; i < data.name.length; i++) {
            iw = new google.maps.InfoWindow({
                content:
                        '<div class="aino-info-window">' +
                        '<div>' +
                        '<span>Åbningstider</span>' +
                        '<img src="' + aino_params.plugins_url + '/vc_pdk_allinone/assets/frontend/universe/images/aino-postnord-logo.png" alt="">' +
                        '</div>' +
                        '<div class="aino-work-hours">' +
                        '<b>MAN-FRE : </b>' + data.opening[i] + ' - ' + data.close[i] + '<br/>' +
                        '<b>LØR : </b>' + data.opening_sat[i] + ' - ' + data.close_sat[i] +
                        '</div>' +
                        '</div>'
            });
            labels.push(iw);
        }

        return labels;
    }

    function closePopup(markers, labels) {
        jQuery('.aino-footer button[role=close]').on('click', function () {
            var popUp = jQuery('.aino-container');
            jQuery('.aino-cover').remove();
            popUp.hide();
            resetMapComponents(map, markers, labels);
        });
    }

    function calcDistance(coord, lat, lng) {

        if (coord.lat !== null && coord.lng !== null) {
            if (typeof (Number.prototype.toRadians) === "undefined") {
                Number.prototype.toRadians = function () {
                    return this * (Math.PI / 180);
                };
            }
            var R = 6371; // metres
            var f1 = coord.lat.toRadians();
            var f2 = coord.lat.toRadians();
            var df = (lat - coord.lat).toRadians();
            var dg = (lng - coord.lng).toRadians();

            var a = Math.sin(df / 2) * Math.sin(df / 2) +
                    Math.cos(f1) * Math.cos(f2) *
                    Math.sin(dg / 2) * Math.sin(dg / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

            var d = R * c;

            return d.toFixed(1);
        } else {
            return null;
        }
    }

    function setScrollToElement(target, offset, lastOffset, index) {

        var scrollContent = jQuery('[data-target=' + target + ']').find('.alt-scroll-content'),
                scrollContentH = scrollContent.height(),
                scrollHolder = jQuery('[data-target=' + target + ']').find('.alt-scroll-holder'),
                scrollHolderH = scrollHolder.height();

        if (-offset < (scrollHolderH - scrollContentH)) {
            jQuery(scrollContent).css('top', scrollHolderH - scrollContentH);
        } else {
            jQuery(scrollContent).css('top', -offset);
        }

        var content = jQuery('[data-target=' + target + ']').find('.alt-scroll-holder');
        var scroll = jQuery('[data-target=' + target + ']').find('.alt-scroll-vertical-bar');
        var len = jQuery('[data-target=' + target + ']').find('.address-near-you li').length;
        var def = Math.abs(content.height() - scroll.height());
        var offsetT = (def / (len - 2)) + len;

        if (index === 0) {
            scroll.css('top', 0);
        } else if (index === len) {
            scroll.css('top', def);
        } else {
            var midOffset = offsetT * index > def ? def : offsetT * index;
            scroll.css('top', midOffset);
        }
    }
};

/**
 * collectUserData
 */
function collectUserData() {
    var method_raw = jQuery('.aino-container.opened .aino-nav li.ui-state-active a').attr('href');

    if (typeof method_raw === 'undefined') {
        method_raw = jQuery('.aino-container.opened .aino-nav li a.active').attr('data-method');
    }

    var method = mapMethods(method_raw);

    jQuery('#shipping_method_0_' + method).click();
    jQuery(('[value*="' + method + '"]')).click();
    var options = getAinoMethodOptions(method);
    jQuery('.vc-aino-inputs').html(options);

}


/**
 * setAinoCookies
 */
function setAinoCookies(choice) {
    Cookies.set('service_point_id', choice.id);
    Cookies.set('service_point_id_name', choice.name);
    Cookies.set('service_point_id_address', choice.address);
    Cookies.set('service_point_id_city', choice.city);
    Cookies.set('service_point_id_country', choice.countryCode);
    Cookies.set('service_point_id_postcode', choice.zip);
}

/**
 * removeAinoCookies
 */
function removeAinoCookies() {
    Cookies.remove('service_point_id');
    Cookies.remove('service_point_id_name');
    Cookies.remove('service_point_id_address');
    Cookies.remove('service_point_id_city');
    Cookies.remove('service_point_id_country');
    Cookies.remove('service_point_id_postcode');
}

/**
 * createInput
 */
function createInput(name, value) {
    return '<input type="text" name="vc_aino_' + name + '" value="' + value + '" />';
}

/**
 * reverseMapMethods
 */
function mapMethods(method_val) {

    var mapMethodsOriginal = aino_params.methods_map;
    for (var i in mapMethodsOriginal) {
        if (method_val === '#' + mapMethodsOriginal[i]) {
            return i;
        }
    }
}

function selectTab() {
    var method_val = jQuery('[value*="vconnect_postnord"]:checked').val();

    if (typeof (method_val) === 'undefined') {
        method_val = jQuery('[value*="vconnect_postnord"]').val();
    }
    var method_raw = method_val.split(':');
    var methods = aino_params.methods_map;
    var href = '#' + methods[method_raw[0]];

    jQuery('.aino-tabs>.aino-nav li').removeClass('ui-state-active');
    jQuery('.aino-tabs-body>div.tab-list').hide();
    jQuery('a[href="' + href + '"]').trigger('click');
    jQuery('a[href="' + href + '"]').parent().addClass('ui-state-active');
    jQuery('div[data-target="' + href.substr(1) + '"]').css('display', 'block');
}

jQuery(document).on('click', '.aino-button.blue', function (e) {
    collectUserData();
});