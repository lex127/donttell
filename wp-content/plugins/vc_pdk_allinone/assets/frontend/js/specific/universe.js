/**
 * getAinoMethodOptions
 */
function getAinoMethodOptions(method) {
    var options = '';
    if (method === 'vconnect_postnord_dk_privatehome') {
        options += createInput('arrival', jQuery('input[name="arrival"][checked="checked"]', '.aino-your-address ').val());
        options += createInput('delivery_type', jQuery('input[name="personal-delivery"][checked="checked"]', '.aino-your-address').val());
        options += createInput('write_place', jQuery('textarea[name="write_place"]:not(:disabled)', '.aino-your-address').size()>0 ? jQuery('textarea[name="write_place"]:not(:disabled)', '.aino-your-address').val() : '');
        removeAinoCookies();
    } else if (method === 'vconnect_postnord_dk_commercial') {
        options += createInput('arrival', jQuery('.aino-your-business input[name="arrival-2"][checked="checked"]').val());
        options += createInput('delivery_type', jQuery('input[name="personal-delivery"][checked="checked"]', '.aino-your-business').val());
        options += createInput('write_place', jQuery('textarea[name="write_place"]:not(:disabled)', '.aino-your-business').size()>0 ? jQuery('textarea[name="write_place"]:not(:disabled)', '.aino-your-business').val() : '');
        removeAinoCookies();
    } else if (method === 'vconnect_postnord_dk_pickup' || method === 'vconnect_postnord_se_pickup' || method === 'vconnect_postnord_no_pickup' || method === 'vconnect_postnord_fi_pickup') {

        var choice = userChoice.getChoice();

        setAinoCookies(choice);

        options += createInput('service_point_id', choice.id);
        options += createInput('service_point_id_name', choice.name);
        options += createInput('service_point_id_address', choice.address);
        options += createInput('service_point_id_city', choice.city);
        options += createInput('service_point_id_country', choice.countryCode);
        options += createInput('service_point_id_postcode', choice.zip);
        var billing_country = jQuery('#billing_country').val();
        if (billing_country === 'DK' || billing_country === 'SE' || billing_country === 'NO' || billing_country === 'FI') {
            jQuery('#sp_info #sp_name').html(choice.name + ', ');
            jQuery('#sp_info #sp_address').html(choice.address + ' ' + choice.city + ' ' + choice.zip);
        }
    }

    return options;
}

function getAinoStandartMethodOptions(method_val) {
    var options = '';
    var method = method_val.split(':')[0];

    if (method === 'vconnect_postnord_dk_privatehome') {
        options += '<br />';
        options += '<select name="vc_aino_arrival">';
        options += '    <option value="Standard Delivery">1-3 dage 08:00 - 17:00</option>';
        options += '</select>';
        options += '<select name="vc_aino_delivery_type">';
        options += '    <option value="Personal Delivery">Din adresse</option>';
        options += '    <optgroup label="Flexlevering">';
        options += '        <option value="In front of the Door">Foran hoveddøren</option>';
        options += '        <option value="Carport">Carport/garage</option>';
        options += '        <option value="Infront of backdoor">Ved bagdøren</option>';
        options += '        <option value="I have modttagarflex">Jeg har Modtagerflex</option>';
        options += '        <option value="Other place">Andet sted</option>';
        options += '    </optgroup>';
        options += '</select>';
        options += '<textarea rows="6" cols="10" name="vc_aino_write_place" style="height: 100px; display: none;" placeholder="Skriv placering"></textarea>';

        removeAinoCookies();
    } else if (method==='vconnect_postnord_dk_commercial') {
        options += '<br />';
        options += '<select name="vc_aino_arrival">';
        options += '    <option value="Standard Delivery">1-3 dage 08:00 - 16:00</option>';
        options += '</select>';
        options += '<select name="vc_aino_delivery_type">';
        options += '    <option value="Personal Delivery">Din adresse</option>';
        options += '    <optgroup label="Flexlevering">';
        options += '        <option value="In front of the Door">Foran hoveddøren</option>';
        options += '        <option value="Carport">Carport/garage</option>';
        options += '        <option value="Infront of backdoor">Ved bagdøren</option>';
        options += '        <option value="I have modttagarflex">Jeg har Modtagerflex</option>';
        options += '        <option value="Other place">Andet sted</option>';
        options += '    </optgroup>';
        options += '</select>';
        options += '<textarea rows="6" cols="10" name="vc_aino_write_place" style="height: 100px; display: none;" placeholder="Skriv placering"></textarea>';

        removeAinoCookies();
    }

    return options;
}