(function ($) {
    $.vcAinoWp = function (el, target, options) {
        var base = this;

        base.$el = $(el);
        base.el = el;

        base.$el.data("vcAinoWp", base);

        base.init = function () {
            if (typeof (target) === "undefined" || target === null)
                target = ".shop_table";

            base.target = target;

            base.options = $.extend({}, $.vcAinoWp.defaultOptions, options);

            $(document).on('DOMNodeInserted', function (event) {
                base.domInsertCheck(event);
            });

            $(document).on('click', '.aino-button', function (e) {
                e.preventDefault();
            });

            $(document).on('click', '.post-code-input button', function (e) {
                var zip = $('.post-code-input input').val();
                if (zip.length === 4) {
                    base.changeZip(zip);
                }
            });

        };

        // Check if the inserted DOM element is the shipping methods block and containes any selected postnord methods
        base.domInsertCheck = function (event) {
            $('.vc-pnrd-inputs').html('');
            if ($(event.target).is(base.target) && $('#vc_trigger').size() === 0) { //changed class
                if ($('[value*="vconnect_postnord"]:checked').size() > 0) {
                    base.insertVcPDKbutton($('[value*="vconnect_postnord"]:checked').next());
                } else if ($('[value*="vconnect_postnord"]').attr('type') === 'hidden') {
                    base.insertVcPDKbutton($('[value*="vconnect_postnord"]'));
                }
            }
        };

        base.insertVcPDKbutton = function (element) {

            var ship_to_address = $('[name="ship_to_different_address"]').is(':checked') ;
            
            var params = {
                action: 'get_points',
                postcode: ship_to_address ? $('#shipping_postcode').val() : $('#billing_postcode').val(),
                address: ship_to_address ? $('#shipping_address_1').val() : $('#billing_address_1').val(),
                country_code: ship_to_address ? $('#shipping_country').val() : $('#billing_country').val()
            };

            if ($('#sog_loader').size() < 1) {
                element.after('<image id="sog_loader" src="' + aino_params.plugins_url + '/vc_pdk_allinone/assets/images/loading.gif" /><ul id="vc_points_container"></ul>');

                $.post(aino_params.admin_ajax, params, function (response) {
                    if (response.length > 0) {
                        var service_points = $.parseJSON(response);

                        if (typeof service_points === 'object') {

                            var method_val = $('[value*="vconnect_postnord"]:checked').val();

                            if (typeof (method_val) === 'undefined') {
                                method_val = $('[value*="vconnect_postnord"]').val();
                            }

                            var method_raw = method_val.split(':');

                            var method_val = method_raw[0];
                            if (aino_params.use_universe === 'yes') {
                                $('#sog_loader').replaceWith('<div style="width: 95%; text-align: left;"><a href="#aino-popup" class="btn vc-popup-trigger vc-btn-disabled" style=" position: relative; left: 25px; background-color: #cc0000 !important; color: #fff; display: block; width: 190px; text-align: center;">' + aino_params.translations.main_button_text + '</a><div id="sp_info"><span id="sp_name"></span><span id="sp_address"></span></div><span id="pn_error"></span></div>');
                                base.addInputs();
                            } else {
                                if (method_val === 'vconnect_postnord_dk_se_pickup' || method_val === 'vconnect_postnord_dk_no_pickup' || method_val === 'vconnect_postnord_dk_fi_pickup' || method_val === 'vconnect_postnord_dk_pickup') {
                                    $('#sog_loader').replaceWith('<div style="width: 95%; text-align: left;"><a href="#aino-popup" class="btn vc-popup-trigger vc-btn-disabled" style=" position: relative; left: 25px; background-color: #1799b9 !important; color: #fff; display: block; width: 190px; text-align: center;">' + aino_params.translations.main_button_text + '</a><div id="sp_info"><span id="sp_name"></span><span id="sp_address"></span></div><span id="pn_error"></span></div>');
                                    base.addInputs();
                                } else {
                                    $('.vc-aino-inputs').html('');
                                    if ($('[value*="vconnect_postnord"]:checked').size() > 0) {
                                        $('#sog_loader').replaceWith(getAinoStandartMethodOptions($('[value*="vconnect_postnord"]:checked').val()));
                                    } else if ($('[value*="vconnect_postnord"]').attr('type') === 'hidden') {
                                        $('#sog_loader').replaceWith(getAinoStandartMethodOptions($('[value*="vconnect_postnord"]').val()));
                                    }
                                }
                            }

                            base.cleanPopups();

                            if (window.google && google.maps) {
                                // Map script is already loaded
                                base.lazyLoadGoogleMarker(service_points);
                            } else {
                                base.lazyLoadGoogleMap(service_points);
                            }

                            if (aino_params.methods_map[method_val] === 'post-office') {
                                base.getPointInfo();
                            }
                        }
                    } else {
                        $('#sog_loader').replaceWith('<div style="color: red; font-weight: normal; ">Indtast venligst et gyldigt postnummer.</div>');
                        $('#sog_loader').remove();
                    }
                }
                );
            }
        };

        base.addInputs = function () {
            if ($('.vc-aino-inputs').size() < 1) {
                $('form.woocommerce-checkout').append('<div class="vc-aino-inputs"></div>');
            }
        }

        base.cleanPopups = function () {
            if ($('body>.aino-popup').size() > 0) {
                $('body>.aino-popup').remove();
            }

            if ($('.aino-container.mobile').size() > 0) {
                $('.aino-container.mobile').not('.aino-container.mobile:eq(0)').remove();
            }

            $('.aino-popup').detach().appendTo('body');
            $('.aino-container.mobile').detach().appendTo('body');
        }

        base.changeZip = function (zip) {
            var params = {
                action: 'get_points',
                postcode: zip,
                address: $('#billing_address_1').val(),
                country_code: $('#billing_country').val()
            };


            $.post(aino_params.admin_ajax, params, function (response) {
                if (response.length > 0) {
                    var service_points = $.parseJSON(response);

                    if (typeof service_points === 'object') {
                        $('#sog_loader').replaceWith('<div style="width: 95%; text-align: left;"><a href="#aino-popup" class="btn vc-popup-trigger vc-btn-disabled" style=" position: relative; left: 25px; background-color: #cc0000 !important; color: #fff; display: block; width: 190px; text-align: center;">' + aino_params.translations.main_button_text + '</a><div id="sp_info"><span id="sp_name"></span><span id="sp_address"></span></div><span id="pn_error"></span></div>');

                        InitAllInOneModule(service_points);
                        $('.vc-popup-trigger').click();
                        $('a[href="#post-office"]').click();
                    }
                } else {
                    $('#sog_loader').replaceWith('<div style="color: red; font-weight: normal; ">Indtast venligst et gyldigt postnummer.</div>');
                    $('#sog_loader').remove();
                }
            });
        };

        base.getPointInfo = function () {
            var spId = typeof Cookies.get('service_point_id') !== 'undefined' ? Cookies.get('service_point_id') : '';
            var spName = typeof Cookies.get('service_point_id_name') !== 'undefined' ? Cookies.get('service_point_id_name') : '';
            var spAdress = typeof Cookies.get('service_point_id_address') !== 'undefined' ? Cookies.get('service_point_id_address') : '';
            var spCity = typeof Cookies.get('service_point_id_city') !== 'undefined' ? Cookies.get('service_point_id_city') : '';
            var spCountry = typeof Cookies.get('service_point_id_country') !== 'undefined' ? Cookies.get('service_point_id_country') : '';
            var spZip = typeof Cookies.get('service_point_id_postcode') !== 'undefined' ? Cookies.get('service_point_id_postcode') : '';
            var billing_country = $('#billing_country').val();

            options = '';
            options += createInput('service_point_id', spId);
            options += createInput('service_point_id_name', spName);
            options += createInput('service_point_id_address', spAdress);
            options += createInput('service_point_id_city', spCity);
            options += createInput('service_point_id_country', spCountry);
            options += createInput('service_point_id_postcode', spZip);

            if (spName !== '' && (billing_country === 'DK' || billing_country === 'SE' || billing_country === 'NO' || billing_country === 'FI')) {
                jQuery('#sp_info #sp_name').html(spName + ', ');
                jQuery('#sp_info #sp_address').html(spAdress + ' ' + spCity + ' ' + spZip);
            }

            jQuery('.vc-aino-inputs').html(options);
        };

        base.lazyLoadGoogleMap = function (service_points) {
            var map_url = "//maps.googleapis.com/maps/api/js";
            if(aino_params.gmaps_api_key!==''){
                map_url += "?key=" + aino_params.gmaps_api_key;
            }
            $.getScript(map_url)
                    .done(function (script, textStatus) {
                        $.getScript(aino_params.plugins_url + "/vc_pdk_allinone/assets/frontend/js/lib/markerwithlabel.js")
                                .done(function (script, textStatus) {
                                    InitAllInOneModule(service_points);
                                })
                                .fail(function (jqxhr, settings, ex) {});

                    })
                    .fail(function (jqxhr, settings, ex) {});


        }

        base.lazyLoadGoogleMarker = function (service_points) {
            $.getScript(aino_params.plugins_url + "/vc_pdk_allinone/assets/frontend/js/lib/markerwithlabel.js")
                    .done(function (script, textStatus) {
                        InitAllInOneModule(service_points);
                    })
                    .fail(function (jqxhr, settings, ex) {});

        }

        // Run initializer
        base.init();
    };

    $.vcAinoWp.defaultOptions = {
        target: ".shop_table"
    };

    $.fn.vcAinoWp = function (target, options) {
        return this.each(function () {
            (new $.vcAinoWp(this, target, options));
        });
    };

    $(document).ready(function () {
        $('body').vcAinoWp("#order_review");

        $(document).on('change', 'select[name="vc_aino_delivery_type"]', function () {
            if ($(this).val() === 'Other place') {
                $('textarea[name="vc_aino_write_place"]').prop('disabled', false).show();
            } else {
                $('textarea[name="vc_aino_write_place"]').val('').hide().prop('disabled', true);
            }
        });

        $(document).on('click', '[name="personal-delivery"]', function () {
            if ($(this).val() === 'Personal Delivery') {
                var textarea = $(this).parents('.aino-inner-tabs').find('.aino-textarea-holder');
                textarea.hide().prop('disabled', true);
            }
        });

        $(document).on('click', '.aino-input', function () {
            var textarea_holder = $(this).closest('.mobile-container').find('.aino-textarea-holder');
            var textarea = textarea_holder.find('textarea');

            if ($(this).find('input[name="personal-delivery"]').val() === 'Other place') {
                textarea_holder.show();
                textarea.show().prop('disabled', false);
            } else {
                textarea_holder.hide();
                textarea.hide().prop('disabled', true);
            }
        });

    });
})(jQuery);

