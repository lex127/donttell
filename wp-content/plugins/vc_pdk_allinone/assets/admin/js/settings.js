(function($) {

    $(document).ready(function(){
        ProcessVcSubmenu();
    });

    $(document).on('click', '#mainform input[name="save"]', function(e){
        e.preventDefault();
        var check = validateAllInOneSettings();
        console.log(check);
        if(check.valid){
            $('#mainform').submit();
        } else {
            var error_string = '';
            for(i in check.errors){
                error_string += check.errors[i];
            }

            return false;
        }

    });

    function ProcessVcSubmenu(){
        $('.subsubsub').append('<li><a href="#" class="vc-pdk-menu-title">vConnect All In One</a> <ul class="vc-pdk-menu"></ul></li>');

        $('.subsubsub li').each(function(i, item){
            if($(item).hasClass('current')){
                $('.vc-pdk-menu-title').addClass('current');
            }

            var href = $('a', item).attr('href').split('&');

            for (var i in href) {
                var part = href[i].split('=');

                if(part[0]==='section'){
                    var section = part[1].split('_');

                    if(section[1]==='postnord'){
                        var anch = $(item).find('a');
                        $(item).html(anch);
                        $(item).detach().appendTo('.vc-pdk-menu');
                    }
                }
            }
        });
    }


    /**
     * validateSettings
     */
    function validateAllInOneSettings() {

        var result = {
            valid: true,
            errors: {}
        };

        if($('.availability[name*="vconnect_postnord"]').size()>0){
            if($('.availability[name*="vconnect_postnord"]').val()==='specific'){
                if($('select.vc-countries-multiselect').val()===null){
                    result.errors['no_country'] = 'Please select a country';
                    result.valid = false;
                }
            }
        }

        $('.vc_rate').each(function(i, row){
            var wmin = $('.vc-rate-weight-min', row).val()!=='' ? parseInt($('.vc-rate-weight-min', row).val()) : 0;
            var wmax = $('.vc-rate-weight-max', row).val()!=='' ? parseInt($('.vc-rate-weight-max', row).val()) : 0;
            var tf = $('.vc-rate-total-from', row).val()!=='' ? parseInt($('.vc-rate-total-from', row).val()) : 0;
            var tt = $('.vc-rate-total-to', row).val()!=='' ? parseInt($('.vc-rate-total-to', row).val()) : 0;

            if(wmin>=wmax){
                result.errors['weight_error'] = 'Minimum weight cannot be more than maximum weight.';
                result.valid = false;
            }

            if(tf>=tt){
                result.errors['weight_error'] = 'Total from cannot be more than total to.';
                result.valid = false;
            }
        });

        return result;
    }

})( jQuery );