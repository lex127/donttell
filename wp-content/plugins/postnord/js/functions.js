/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function(){
    jQuery('.map-button-save').on('click', function() {
        if (!jQuery('input[name="postnord_pickupLocation"]').is(':checked')) {
            if (jQuery('#error_checked_radio').length === 0) {
                jQuery('#map-button-save').before('<span id="error_checked_radio" style="color: red; position: absolute; right: 140px;">Please select one of the options.</span>');
            }
        } else {
            if (jQuery('#error_checked_radio').length > 0) {
                jQuery('#error_checked_radio').remove();
            }
    
            id_element = jQuery('input[name="postnord_pickupLocation"]:checked').val();
            jQuery('input[name=\'service_point_id\']').val(jQuery('input[id="' + id_element + '"]').val());
            jQuery('input[name=\'service_point_id_name\']').val(jQuery('input[id="' + id_element + '"]').parent().find('strong').html());
            jQuery('input[name=\'service_point_id_address\']').val(jQuery('input[id="' + id_element + '"]').parent().find('.postnord_address > .street').html());
            jQuery('input[name=\'service_point_id_city\']').val(jQuery('input[id="' + id_element + '"]').parent().find('.postnord_address > .city').html());
            jQuery('input[name=\'service_point_id_postcode\']').val(jQuery('input[id="' + id_element + '"]').parent().find('.postnord_address > .service_postcode').val());
            jQuery('#sp_info #sp_name').html(jQuery('input[id="' + id_element + '"]').parent().find('strong').html() + ', ');
            jQuery('#sp_info #sp_address').html(jQuery('input[id="' + id_element + '"]').parent().find('.postnord_address > .street').html() + ' ' + jQuery('input[id="' + id_element + '"]').parent().find('.postnord_address > .city').html() + ' ' + jQuery('input[id="' + id_element + '"]').parent().find('.postnord_address > .service_postcode').val());
            jQuery('.tb-close-icon').click();
        }
    });
    
    jQuery(document).on('click', '#place_order', function(e){
        if(jQuery('#shipping_method_0_postnord').is(':checked') && (jQuery('input[name=\'service_point_id\']').size()>0 && jQuery('input[name=\'service_point_id\']').val()=='')){
            e.preventDefault();
            if(jQuery('input[name=\'service_point_id\']').size()>0){
                if(jQuery('.woocommerce-error').size()>0){
                    jQuery('.woocommerce-error').html('<li>Brug venligst Søg for at vælge posthus.</li>');
                } else {
                    jQuery('form.checkout').prepend('<ul class="woocommerce-error"><li>Brug venligst Søg for at vælge posthus.</li></ul>');
                }

            } else {
                jQuery('form.checkout').prepend('<ul class="woocommerce-error"><li>Brug venligst Søg for at vælge posthus.</li></ul>');                
            }


            jQuery('html,body').animate({
               scrollTop: $("#content").offset().top
            });            
        }
    });
});

function showForm(id) {
    if (id === 'showMap' && !jQuery('#showMap_input').hasClass('thickbox')) {
        jQuery('#showMap_input').addClass('thickbox');
        jQuery('#showMap_input').click();
    }
}