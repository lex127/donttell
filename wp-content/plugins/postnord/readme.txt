=== PostNord Pickup Locator ===
Version 1.0.1
Developed by: Hoj and Just Ltd.
Tested up to: Wordpress 3.6 with WooCommerce 2.0.13

== Description ==

Let your customers choose a post office for the delivery of the package (With google map) in the woocommerce checkout process.

= Documentation =

After installation activate this shipping method. Go to WooCommerce->Settings->Shipping select AWD Weight/Country Shipping and thick enable box.

== Installation ==

1. Upload 'postnord' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Set your delivery rates in WooCommerce->Settings->Shipping->Post Danmark Afhentingssted
    - Input your Post Danmark Afhentingssted API general consumer id key
    - Select 'Yes' for Google map option in order to allow users to choose points from the map