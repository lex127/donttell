<?php
/**
 * Plugin Name: PostNord Pickup Locator
 * Description: PostNord Pickup Locator.
 * Version: 1.0.0
 * Author: Hoj and Just Ltd.
  /*  Copyright 2013
 */
add_action('plugins_loaded', 'init_postnord', 0);
add_action('wp_ajax_get_points', 'get_points');
add_action('wp_ajax_nopriv_get_points', 'get_points');
add_action('wp_enqueue_scripts', 'put_scripts');

function put_scripts() {
    echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>';
    echo '<script type="text/javascript" src="' . plugins_url() . '/postnord/js/map_functions.js"></script>';
    echo '<script type="text/javascript" src="' . plugins_url() . '/postnord/js/functions.js"></script>';
}

function get_points() {
    global $woocommerce;
    $postnord_rates = array_filter((array) get_option('woocommerce_postnord_rates'));
    $woocommerce->session->postnord_settings = array_filter((array) get_option('woocommerce_postnord_settings'));
    unset($woocommerce->session->service_points);

    if ($woocommerce->customer->shipping_postcode) {
        $postnumber = $woocommerce->customer->shipping_postcode;
    } else {
        $postnumber = 1000;
    }

    $service_points = getPostnordServicePoints($postnumber);
    if ($service_points) {
        $woocommerce->session->service_points = array();
        if ($woocommerce->session->postnord_settings['google_map'] == 'yes') {
            $woocommerce->session->service_points = json_encode($service_points);
        } else {
            $woocommerce->session->service_points = $service_points;
        }
    } else {
//        $woocommerce->session->service_points = array();
    }

    echo $woocommerce->session->service_points;
    die();
}

function init_postnord() {
    if (!class_exists('WC_Shipping_Method'))
        return;

    add_action('woocommerce_admin_order_data_after_shipping_address', 'admin_order_data_after_shipping_address', '');
    add_action('woocommerce_checkout_update_order_meta', 'checkout_update_order_meta', 10, 2);
    add_action('woocommerce_checkout_process', 'checkout_process');
    add_action('woocommerce_shipping_method_chosen', 'shipping_method_chosen');
    add_action('woocommerce_after_checkout_form', 'after_checkout_form', $content);

    function after_checkout_form($content) {
        global $woocommerce;
        $content = '<meta name="viewport" content="initial-scale=1.0, user-scalable=no">';
        $content .= '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>';

        $content .= "<link rel='stylesheet' id='thickbox-css' href='" . plugins_url() . "/postnord/js/thickbox/thickbox.css' type='text/css' media='all' />";
        $content .= '<script type="text/javascript">var thickbox_editL10n = {"next":"N\u00e6ste >","prev":"< Tidligere","image":"Billede","of":"af","close":"Luk","noiframes":"Denne mulighed kr\u00e6ver inline frames. Enten har du deaktiveret iframes eller de underst\u00f8ttes ikke af din browser. ","loadingAnimation":"' . plugins_url() . '/postnord/js/thickbox/loadingAnimation.gif","closeImage":"' . plugins_url() . '/postnord/js/thickbox/tb-close.png"};</script>';
        $content .= '<script type="text/javascript" src="' . plugins_url() . '/postnord/js/thickbox/thickbox.js"></script>';

        $inj = '';
        if (!is_null($woocommerce->checkout)) {
            $service_points = json_decode($woocommerce->session->service_points);
            if (!isset($service_points->error)) {
                if ($woocommerce->session->postnord_settings['google_map'] == 'yes') {
//                if (!empty($woocommerce->session->service_points)) {

                    /* <br />
                      Indtast navn på den der afhenter pakken: <abbr class="required" title="required">*</abbr>
                      <br /><input type="text" name="postnord_pickupPerson" value="" /> */
                    $inj .= "<input type=\"button\" onclick=\"showForm(\'showMap\')\" value=\"Søg\"  alt=\"#TB_inline?width=790&amp;inlineId=showMap\" id=\"showMap_input\" /><div id=\"sp_info\"><span id=\"sp_name\"></span><span id=\"sp_address\"></span></div>";
                    $inj .= '<input type="hidden" name="service_point_id" value="" /><input type="hidden" name="service_point_id_name" value="" /><input type="hidden" name="service_point_id_address" value="" /><input type="hidden" name="service_point_id_city" value="" /><input type="hidden" name="service_point_id_postcode" value="" />';
                } else {

                    /* <br /><input type="hidden" name="postnord_pickupPerson" value="" /> */

                    $inj .= '<input type="hidden" name="service_point_id" value="" /><input type="hidden" name="service_point_id_name" value="" /><input type="hidden" name="service_point_id_address" value="" /><input type="hidden" name="service_point_id_city" value="" /><input type="hidden" name="service_point_id_postcode" value="" /><span style="color: red;">' . $service_points->error . '</span>';
                }
//                }
            } else {
                if (!isset($woocommerce->session->service_points['error'])) {
                    $inj .= '<br />Vælg en adresse HER (Ikke på kortet): <abbr class="required" title="required">*</abbr><br /><select name="service_point_id"><option value="">Vælg en adresse</option>';
                    foreach ($woocommerce->session->service_points['servicePointId'] as $key => $value) {
                        $inj .= '<option value="' . $value . '">' . $woocommerce->session->service_points['addresses'][$key] . '</option>';
                    }
                    $inj .= '</select>';
                    $inj .= '<br /><input type="hidden" name="service_point_id_name" value="" /><input type="hidden" name="service_point_id_address" value="" /><input type="hidden" name="service_point_id_city" value="" /><input type="hidden" name="service_point_id_postcode" value="" />';

                    /* <br />
                      Indtast navn på den der afhenter pakken: <abbr class="required" title="required">*</abbr>
                      <br /><input type="text" name="postnord_pickupPerson" value="" /> */

                    $inj .= '<script type="text/javascript" src="' . plugins_url() . '/postnord/js/sp_select.js"></script>';
                } else {
                    $inj .= '<span style="color: red;">' . $woocommerce->session->service_points['error'] . '</span>';
                }
                $inj .= '</li>';
            }

            echo '<script type="text/javascript">';
            echo '        var markers = [];';
            echo "  jQuery('#order_review').on('DOMNodeInserted', function(event) {";
            echo "      if (jQuery(event.target).hasClass('shop_table')) {";
            echo "          if (jQuery('#shipping_method_0_postnord').attr('checked') == 'checked') {";
            echo "          jQuery('#shipping_method_0_postnord').next().after('<image id=\"sog_loader\" src=\"" . plugins_url() . "/postnord/images/loading.gif\" />');";
            echo '        function reinit_map(service_points){ ';
            echo '            if (service_points.name.length>0) { ';
            echo '                initialize_map(service_points.addresses, service_points.name, service_points.number, service_points.opening, service_points.close, service_points.opening_sat, service_points.close_sat, service_points.lat, service_points.lng, service_points.servicePointId);';
            echo "                jQuery('#postnord_list').html(service_points.radio_html);";
            echo '            }';
            echo '        }';
            echo "          jQuery.post(
                                '" . admin_url('admin-ajax.php') . "',
                                {
                                    'action': 'get_points'
                                }, 
                                function(response){
                                    if(response.length>0){                      
                                        var service_points=jQuery.parseJSON(response);
                                        if(typeof service_points =='object')
                                        {
                                            jQuery('#sog_loader').replaceWith('" . $inj . "');  
                                            reinit_map(service_points);
                                        }
                                        else
                                        {
                                            jQuery('#sog_loader').replaceWith('<div style=\"color: red; font-weight: normal; \">Indtast venligst et gyldigt postnummer.</div>');  
                                        }                                    
                                    } else {
                                        jQuery('#sog_loader').replaceWith('<div style=\"color: red; font-weight: normal; \">Indtast venligst et gyldigt postnummer.</div>');  
                                    }
                                }
                            );";

            echo "          }";
            echo "      }";
            echo "  });";
            echo '</script>';
        }

        $available_methods = $woocommerce->shipping->get_shipping_methods();
        if ($woocommerce->session->postnord_settings['enabled'] == 'yes' && $woocommerce->session->postnord_settings['google_map'] == 'yes') {

            if (!empty($woocommerce->session->service_points)) {
                $service_points = json_decode($woocommerce->session->service_points);
                if (!empty($service_points->servicePointId)) {

                    $content .= '<div id="showMap" style="position: absolute; top: 0; left: -10000px;">';
                    $content .= '    <style>';
                    $content .= '        #sog_loader, #showMap_input {';
                    $content .= '            margin-left: 15px;';
                    $content .= '        }';
                    $content .= '        #TB_ajaxContent {';
                    $content .= '            overflow: visible;';
                    $content .= '        }';
                    $content .= '        #pickupLocations {';
                    $content .= '            margin-top: 15px;';
                    $content .= '            margin-bottom: 15px;';
                    $content .= '        }';
                    $content .= '        ul#mapAddress {';
                    $content .= '            list-style: none;';
                    $content .= '            padding-left: 0px;';
                    $content .= '        }';
                    $content .= '        ul#mapAddress li {';
                    $content .= '            float: left;';
                    $content .= '            margin-left: 30px;';
                    $content .= '            width: 160px;';
                    $content .= '            margin-bottom: 15px;';
                    $content .= '            cursor: pointer;';
                    $content .= '        }';
                    $content .= '        ul#mapAddress li input[name="postnord_pickupLocation"]{';
                    $content .= '            margin-right: 5px;';
                    $content .= '        }';
                    $content .= '        #sp_address {';
                    $content .= '            font-weight: normal;';
                    $content .= '        }';
                    $content .= '        .buttons-container {';
                    $content .= '            height: 20px';
                    $content .= '        }';
                    $content .= '    </style>';
                    $content .= '    <div id="map_canvas" style="height: 350px; width: 780px; position: relative; margin-top: 20px;"></div><div id="pickupLocations" class="pickupLocation-container"><div class="map-button-save" style="cursor: pointer; float: right; padding: 2px 5px; font-weight:bold; border: 1px solid #000; background:#ccc;"><span><span>Afslut og Gem</span></span></div><div id="postnord_list">' . $service_points->radio_html . '</div><div class="clear"></div><div class="buttons-container"><div class="map-button-save" style="cursor: pointer; float: right; padding: 2px 5px; font-weight:bold; border: 1px solid #000; background:#ccc;"><span><span>Afslut og Gem</span></span></div></div>';

                    $content .= '</div></div>';
                }
            }
        }
        echo $content;
    }

    function admin_order_data_after_shipping_address($order) {
        $post_custom = get_post_custom($order_id);
        if (isset($post_custom['_service_point_id'])) {
            $content = '';
            $content .= '<p class="none_set"><table>';
            if (isset($post_custom['_service_point_id'])) {
                $content .= '<tr><td><strong>Service Point Id:</strong></td><td>' . $post_custom['_service_point_id'][0] . '</td></tr>';
            }
            if (isset($post_custom['_service_point_id_name'])) {
                $content .= '<tr><td><strong>Service point name:</strong></td><td>' . $post_custom['_service_point_id_name'][0] . '</td></tr>';
            }
            if (isset($post_custom['_service_point_id_address'])) {
                $content .= '<tr><td><strong>Address:</strong></td><td>' . $post_custom['_service_point_id_address'][0] . '</td></tr>';
            }
            if (isset($post_custom['_service_point_id_city'])) {
                $content .= '<tr><td><strong>City:</strong></td><td>' . $post_custom['_service_point_id_city'][0] . '</td></tr>';
            }
            if (isset($post_custom['_service_point_id_postcode'])) {
                $content .= '<tr><td><strong>Postcode:</strong></td><td>' . $post_custom['_service_point_id_postcode'][0] . '</td></tr>';
            }
            if (isset($post_custom['_service_point_id_country'])) {
                $content .= '<tr><td><strong>CountryTag:</strong></td><td>' . $post_custom['_service_point_id_country'][0] . '</td></tr>';
            }
            /*
              if (isset($order->order_custom_fields['_postnord_pickup_person'])) {
              echo '<tr><td><strong>Pickup person:</strong></td><td>' . $order->order_custom_fields['_postnord_pickup_person'][0] . '</td></tr>';
              }
             */
            $content .= '</table></p>';

            echo "<script>jQuery('div.address').eq(1).append('" . $content . "')</script>";
        }
    }

    function checkout_update_order_meta($order_id, $posted) {
        global $woocommerce;

        if (isset($woocommerce->checkout->posted['service_point_id'])) {
            update_post_meta($order_id, '_service_point_id', $woocommerce->checkout->posted['service_point_id']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_name'])) {
            update_post_meta($order_id, '_service_point_id_name', $woocommerce->checkout->posted['service_point_id_name']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_address'])) {
            update_post_meta($order_id, '_service_point_id_address', $woocommerce->checkout->posted['service_point_id_address']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_city'])) {
            update_post_meta($order_id, '_service_point_id_city', $woocommerce->checkout->posted['service_point_id_city']);
        }

        if (isset($woocommerce->checkout->posted['service_point_id_postcode'])) {
            update_post_meta($order_id, '_service_point_id_postcode', $woocommerce->checkout->posted['service_point_id_postcode']);
        }

        if (!empty($woocommerce->session->postnord_settings['postnord_country'])) {
            update_post_meta($order_id, '_service_point_id_country', $woocommerce->session->postnord_settings['postnord_country']);
        }

        /*
          if (isset($woocommerce->checkout->posted['postnord_pickupPerson'])) {
          update_post_meta( $order_id, '_postnord_pickup_person', 		$woocommerce->checkout->posted['postnord_pickupPerson'] );
          }
         */
    }

    function checkout_process() {
        global $woocommerce;

        if (!empty($_POST['service_point_id'])) {
            $woocommerce->checkout->posted['service_point_id'] = $_POST['service_point_id'];
            $woocommerce->checkout->posted['service_point_id_name'] = $_POST['service_point_id_name'];
            $woocommerce->checkout->posted['service_point_id_address'] = $_POST['service_point_id_address'];
            $woocommerce->checkout->posted['service_point_id_city'] = $_POST['service_point_id_city'];
            $woocommerce->checkout->posted['service_point_id_postcode'] = $_POST['service_point_id_postcode'];
        }

        /*
          if (!empty($_POST['postnord_pickupPerson'])) {
          $woocommerce->checkout->posted['postnord_pickupPerson'] = $_POST['postnord_pickupPerson'];
          }
         */


        if ($woocommerce->session->chosen_shipping_method == 'postnord') {
            /*
              if (strlen(trim($this->posted['postnord_pickupPerson'])) == 0) {
              $woocommerce->add_error( '<strong>Please, fill in pickup person field</strong>' );
              }
             */

            if (empty($woocommerce->checkout->posted['service_point_id']) && $woocommerce->session->postnord_settings['google_map'] == 'yes') {
                $woocommerce->add_error('<strong>Tryk venligst på “Søg” knappen for at vælge et afhentningssted. </strong>');
            } elseif (empty($woocommerce->checkout->posted['service_point_id']) && $woocommerce->session->postnord_settings['google_map'] == 'no') {
                $woocommerce->add_error('<strong>Vælg venligst et afhentningssted.</strong>');
            }
        }
    }

    function shipping_method_chosen() {
        global $woocommerce;
        $postnord_rates = array_filter((array) get_option('woocommerce_postnord_rates'));
        $woocommerce->session->postnord_settings = array_filter((array) get_option('woocommerce_postnord_settings'));

        $woocommerce->session->service_points = array();

        if ($woocommerce->customer->shipping_postcode) {
            $postnumber = $woocommerce->customer->shipping_postcode;
        } else {
            $postnumber = 1000;
        }

        $service_points = getPostnordServicePoints($postnumber);
        if ($service_points) {
            if ($woocommerce->session->postnord_settings['google_map'] == 'yes') {
                $woocommerce->session->service_points = json_encode($service_points);
            } else {
                $woocommerce->session->service_points = $service_points;
            }
        } else {
            $woocommerce->session->service_points = array();
        }
    }

    //Get postnord
    function getPostnordServicePoints($postcode) {
        global $woocommerce;

        if (strlen((int) $postcode) == 4 && strlen($woocommerce->session->postnord_settings['postnord_consumer_id']) > 1 && !empty($woocommerce->session->postnord_settings['postnord_country'])) {
            $url = "http://api.postnord.com/wsp/rest/BusinessLocationLocator" .
                    "/Logistics/ServicePointService_1.0/findNearestByAddress.json?" .
                    "consumerId=" . trim($woocommerce->session->postnord_settings['postnord_consumer_id']) .
                    "&countryCode=" . trim($woocommerce->session->postnord_settings['postnord_country']) .
                    "&postalCode=" . trim($postcode) . "&numberOfServicePoints=20";

            //$jsonFile = file_get_contents($url);

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $jsonFile = curl_exec($curl);
            curl_close($curl);

            $data = json_decode($jsonFile);

            if ($data) {
                if ($woocommerce->session->postnord_settings['google_map'] == 'yes') {
                    $post_addresses['radio_html'] = GetPickupLocationsResult($postcode, $data);

                    $points = $data->servicePointInformationResponse->servicePoints;

                    $addresses = array();
                    $name = array();
                    $number = array();
                    $generate = array();
                    $opening = array();
                    $close = array();
                    $opening_sat = array();
                    $close_sat = array();
                    $lat = array();
                    $lng = array();
                    $key = 1;

                    if (!empty($points)) {
                        foreach ($points as $key => $point) {
                            if ($point->visitingAddress->streetName) {
                                $addresses[] = $point->visitingAddress->streetName . ' ' . $point->visitingAddress->streetNumber;
                                $name[] = $point->name;
                                $opening[] = $point->openingHours[0]->from1;
                                $close[] = $point->openingHours[0]->to1;
                                $opening_sat[] = $point->openingHours[5]->from1;
                                $close_sat[] = $point->openingHours[5]->to1;
                                $lat[] = $point->coordinate->northing;
                                $lng[] = $point->coordinate->easting;
                                $number[] = $point->deliveryAddress->postalCode . ' ' . $point->deliveryAddress->city;
                                $servicePointId[] = $point->servicePointId;
                                $generate[$point->servicePointId] = $point->deliveryAddress->streetName . ' ' . $point->deliveryAddress->streetNumber . ',' . $point->name . ',' . $point->deliveryAddress->postalCode . ' ' . $point->deliveryAddress->city . ',' . ($key) . ',' . $point->openingHours[0]->from1 . ',' . $point->openingHours[0]->to1 . ',' . $point->openingHours[5]->from1 . ',' . $point->openingHours[5]->to1 . ',' . $point->coordinate->northing . ',' . $point->coordinate->easting;

                                $key++;
                            }
                        }
                    }

                    $post_addresses['addresses'] = $addresses;
                    $post_addresses['name'] = $name;
                    $post_addresses['number'] = $number;
                    $post_addresses['generate'] = $generate;
                    $post_addresses['opening'] = $opening;
                    $post_addresses['close'] = $close;
                    $post_addresses['opening_sat'] = $opening_sat;
                    $post_addresses['close_sat'] = $close_sat;
                    $post_addresses['lat'] = $lat;
                    $post_addresses['lng'] = $lng;
                    $post_addresses['servicePointId'] = $servicePointId;
                } else {
                    $points = $data->servicePointInformationResponse->servicePoints;

                    $addresses = array();
                    $servicePointId = array();
                    $key = 1;

                    if (!empty($points)) {
                        foreach ($points as $key => $point) {
                            if ($point->visitingAddress->streetName) {
                                $addresses[] = $point->visitingAddress->streetName . ' ' . $point->visitingAddress->streetNumber;
                                $servicePointId[] = $point->servicePointId;

                                $key++;
                            }
                        }
                    }

                    $post_addresses['addresses'] = $addresses;
                    $post_addresses['servicePointId'] = $servicePointId;
                }

                return $post_addresses;
            } else {
                $post_addresses['error'] = 'Der er desværre ingen Post Danmark udleveringssted i det ønskede postnummer.';
                return $post_addresses;
            }
        }
    }

    function GetPickupLocationsResult($zip, $data) {
        global $woocommerce;

        $response = '';
        $shops = $data->servicePointInformationResponse->servicePoints;

        if (!isset($shops) || sizeof($shops) == 0) {
            $response .= '<span class="postnord-error" id="postnord-error">Postnummeret er ikke korrekt.</span><br/>';
            $response .= '<input type="hidden" name="postnord_pickupLocation" id="location" class="postnord_location">';
        } else {
            //Mage::getSingleton('core/session')->setPostnordPostnr($zip);
            if ($woocommerce->session->postnord_settings['google_map'] == 'yes') {
                $response .= '<div class="postnord-choose"><strong>Vælg et afhentningssted:</strong></div>';
                //$response .= '<select name="postnord_pickupLocation" onchange="adress_zoom(this);" id="" class="postnord_location">';
            } else {
                $response .= '<div class="postnord-choose"><strong>Vælg et afhentningssted:</strong></div>';
                //$response .= '<select name="postnord_pickupLocation" id="location" class="postnord_location">';
            }
            $response .= '<ul id="mapAddress">';
            //$response .= '<option value="">' . Mage::getStoreConfig('postnord/general/postnord_address') . '</option>';
            $checked = ' CHECKED';
            if (sizeof($shops) == 1) {
                $response .= createShop($shops[0], $checked);
            } else {
                $checked = ' CHECKED';
                $cnt = 0;
                $count = count($shops);
                foreach ($shops as $key => $shop) {
                    $response .= createShop($shop, $checked, $cnt, $count);
                    $cnt++;
                    $checked = '';
                }
            }

            $response .= '<ul>';
        }

        return $response;
    }

    function createShop($shop, $checked, $key, $count) {
        ++$key;
        $response = '<li onclick="selectMarker(' . trim($shop->servicePointId) . ')">';
        if ($count == $key) {
            $response .= '<input type="radio" id="' . trim($shop->servicePointId) . '" value="' . trim($shop->servicePointId) . '" name="postnord_pickupLocation" class="radio validate-one-required-by-name" />';
        } else {
            $response .= '<input type="radio" id="' . trim($shop->servicePointId) . '" value="' . trim($shop->servicePointId) . '" name="postnord_pickupLocation" class="radio" />';
        }

        $response .= $key . '. ';
        $response .= '<strong>' . trim($shop->name) . '</strong><div class="postnord_address"><span class="street">' . trim($shop->deliveryAddress->streetName) . ' ' . trim($shop->deliveryAddress->streetNumber) . '</span><br /><span class="city">' . trim($shop->deliveryAddress->city) . '</span><input type="hidden" class="service_postcode" value="' . trim($shop->deliveryAddress->postalCode) . '" /></div>';

        $response .= '</li>';

        if ($key % 4 == 0) {
            $response .= '<div class="clear"></div>';
        }
        return $response;
    }

    class WC_Shipping_Postnord extends WC_Shipping_Method {

        /**
         * __construct function.
         *
         * @access public
         * @return void
         */
        function __construct() {
            $this->id = 'postnord';
            $this->method_title = __('Post Danmark Afhentingssted', 'woocommerce');
            $this->postnord_rate_option = 'woocommerce_postnord_rates';

            // Actions
            add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
            add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_postnord_rates'));

            $this->init();
        }

        /**
         * init function.
         *
         * @access public
         * @return void
         */
        function init() {

            // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->enabled = $this->get_option('enabled');
            $this->title = $this->get_option('title');
            $this->availability = $this->get_option('availability');
            $this->countries = $this->get_option('countries');
            $this->requires = $this->get_option('requires');
            $this->postnord_consumer_id = $this->get_option('postnord_consumer_id');
            $this->google_map = $this->get_option('google_map');
            $this->postnord_country = $this->get_option('postnord_country');

            // Load Flat rates
            $this->get_postnord_rates();
        }

        /**
         * Initialise Gateway Settings Form Fields
         *
         * @access public
         * @return void
         */
        function init_form_fields() {
            global $woocommerce;

            $shipping_classes = array();
            $shipping_classes['NULL'] = 'N/A';
            foreach (get_terms('product_shipping_class') as $shipping_class) {
                $shipping_classes[$shipping_class->term_id] = $shipping_class->name;
            }

            // Backwards compat
            if ($this->get_option('requires_coupon') && $this->min_amount)
                $default_requires = 'either';
            elseif ($this->get_option('requires_coupon'))
                $default_requires = 'coupon';
            elseif ($this->min_amount)
                $default_requires = 'min_amount';
            else
                $default_requires = '';

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Enable/Disable', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Enable Post Danmark Afhentingssted', 'woocommerce'),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __('Method Title', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('This controls the title which the user sees during checkout.', 'woocommerce'),
                    'default' => __('Post Danmark Afhentingssted', 'woocommerce'),
                    'desc_tip' => true,
                ),
                'availability' => array(
                    'title' => __('Method availability', 'woocommerce'),
                    'type' => 'select',
                    'default' => 'all',
                    'class' => 'availability',
                    'options' => array(
                        'all' => __('All allowed countries', 'woocommerce'),
                        'specific' => __('Specific Countries', 'woocommerce')
                    )
                ),
                'countries' => array(
                    'title' => __('Specific Countries', 'woocommerce'),
                    'type' => 'multiselect',
                    'class' => 'chosen_select',
                    'css' => 'width: 450px;',
                    'default' => '',
                    'options' => $woocommerce->countries->countries,
                ),
                'requires' => array(
                    'title' => __('Post Danmark Afhentingssted Requires...', 'woocommerce'),
                    'type' => 'select',
                    'default' => $default_requires,
                    'options' => array(
                        '' => __('N/A', 'woocommerce'),
                        'billing_postcode' => __('Postnumber', 'woocommerce'),
                    )
                ),
                'postnord_consumer_id' => array(
                    'title' => __('API consumerId', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Post Danmark Afhentingssted general consumer id'),
                    'default' => '',
                    'desc_tip' => true
                ),
                'google_map' => array(
                    'title' => __('Google map', 'woocommerce'),
                    'type' => 'select',
                    'default' => 'no',
                    'options' => array(
                        'yes' => __('Yes', 'woocommerce'),
                        'no' => __('No', 'woocommerce')
                    )
                ),
                'postnord_country' => array(
                    'title' => __('CountryTag', 'woocommerce'),
                    'type' => 'select',
                    'default' => 'no',
                    'options' => array(
                        'DK' => __('Denmark', 'woocommerce'),
                        'SE' => __('Sweden', 'woocommerce'),
                        'NO' => __('Norway', 'woocommerce')
                    )
                ),
                'one_without_discount_enable' => array(
                    'title' => __('Enable price if at least one product is without discount', 'woocommerce'),
                    'type' => 'checkbox',
//                    'label' => __('Enable price if at least one product is without discount', 'woocommerce'),
                    'default' => 'yes'
                ),
                'one_without_discount_price' => array(
                    'title' => __('Price if least one product is without discount', 'woocommerce'),
                    'type' => 'text',
                    'label' => __('Enable Post Danmark Afhentingssted', 'woocommerce'),
                    'default' => __('', 'woocommerce'),
                ),
                'free_price_for_no_discount_products_enable' => array(
                    'title' => __('Enable free shipping if no discount products are above price value.', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Enable free shipping if no discount products are above price value.', 'woocommerce'),
                    'default' => 'yes'
                ),
                'free_price_for_no_discount_products' => array(
                    'title' => __('Free Shipping for no discount products above price', 'woocommerce'),
                    'type' => 'text',
                    'label' => __('Free Shipping for no discount products above price'),
                    'default' => __('', 'woocommerce'),
                ),                
                'cheap_shipping_class_enable' => array(
                    'title' => __('Enable price if at least one product has the cheap shipment class', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Enable price if at least one product has the cheap shipment class', 'woocommerce'),
                    'default' => 'yes'
                ),
                'cheap_shipping_class' => array(
                    'title' => __('Cheap Shipping Class', 'woocommerce'),
                    'type' => 'select',
                    'options' => $shipping_classes
                ),
                'cheap_shipping_class_price' => array(
                    'title' => __('Price if at least one product has the cheap shipment class', 'woocommerce'),
                    'type' => 'text',
                    'label' => __('Price if at least one product has the cheap shipment class', 'woocommerce'),
                    'default' => __('', 'woocommerce'),
                ),
                'all_with_discount_enable' => array(
                    'title' => __('Enable price if all products are with discount', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Enable price if all products are with discount', 'woocommerce'),
                    'default' => 'yes'
                ),
                'all_with_discount_price' => array(
                    'title' => __('Price if all products are with discount', 'woocommerce'),
                    'type' => 'text',
                    'label' => __('Enable Post Danmark Afhentingssted', 'woocommerce'),
                    'default' => __('', 'woocommerce'),
                ),
                'postnord_rates' => array(
                    'title' => __('Post Danmark Afhentingssted rates', 'woocommerce'),
                    'type' => 'title'
                ),
                'postnord_rates_table' => array(
                    'type' => 'postnord_rates_table'
                )
            );
        }

        /**
         * Admin Panel Options
         * - Options for bits like 'title' and availability on a country-by-country basis
         *
         * @since 1.0.0
         * @access public
         * @return void
         */
        public function admin_options() {
            ?>
            <h3><?php _e('Post Danmark Afhentingssted', 'woocommerce'); ?></h3>
            <table class="form-table">
                <?php
                // Generate the HTML For the settings form.
                $this->generate_settings_html();
                ?>
            </table><!--/.form-table-->
            <?php
        }

        /**
         * is_available function.
         *
         * @access public
         * @param mixed $package
         * @return bool
         */
        function is_available($package) {
            global $woocommerce;

            if ($this->enabled == "no")
                return false;

            $ship_to_countries = '';

            if ($this->availability == 'specific') {
                $ship_to_countries = $this->countries;
            } else {
                if (get_option('woocommerce_allowed_countries') == 'specific')
                    $ship_to_countries = get_option('woocommerce_specific_allowed_countries');
            }

            if (is_array($ship_to_countries))
                if (!in_array($package['destination']['country'], $ship_to_countries))
                    return false;

            // Enabled logic
            $is_available = false;
            $has_coupon = false;
            $has_met_min_amount = false;

            if (in_array($this->requires, array('coupon', 'either', 'both'))) {

                if ($woocommerce->cart->applied_coupons) {
                    foreach ($woocommerce->cart->applied_coupons as $code) {
                        $coupon = new WC_Coupon($code);

                        if ($coupon->is_valid() && $coupon->enable_postnord())
                            $has_coupon = true;
                    }
                }
            }

            if (in_array($this->requires, array('min_amount', 'either', 'both'))) {

                if (isset($woocommerce->cart->cart_contents_total)) {

                    if ($woocommerce->cart->prices_include_tax)
                        $total = $woocommerce->cart->tax_total + $woocommerce->cart->cart_contents_total;
                    else
                        $total = $woocommerce->cart->cart_contents_total;

                    if ($total >= $this->min_amount)
                        $has_met_min_amount = true;
                }
            }

            switch ($this->requires) {
                case 'min_amount' :
                    if ($has_met_min_amount)
                        $is_available = true;
                    break;
                case 'coupon' :
                    if ($has_coupon)
                        $is_available = true;
                    break;
                case 'both' :
                    if ($has_met_min_amount && $has_coupon)
                        $is_available = true;
                    break;
                case 'either' :
                    if ($has_met_min_amount || $has_coupon)
                        $is_available = true;
                    break;
                default :
                    $is_available = true;
                    break;
            }

            return apply_filters('woocommerce_shipping_' . $this->id . '_is_available', $is_available);
        }

        /**
         * calculate_shipping function.
         *
         * @access public
         * @return array
         */
        function calculate_shipping($package = array()) {
            global $woocommerce;
            
            $cost = 0;
            if (isset($woocommerce->cart->cart_contents_total)) {

                if ($woocommerce->cart->prices_include_tax)
                    $total = $woocommerce->cart->tax_total + $woocommerce->cart->cart_contents_total;
                else
                    $total = $woocommerce->cart->cart_contents_total;

                if (!empty($this->postnord_rates)) {
                    foreach ($this->postnord_rates as $postnord_rate) {
                        if ($total >= $postnord_rate['total_from'] && $total <= $postnord_rate['total_to']) {
                            $cost = $postnord_rate['cost'];
                        }
                    }
                }
            }

            $args = array(
                'id' => $this->id,
                'label' => $this->title,
                'cost' => $cost,
                'taxes' => false
            );
            
            
            $special = $this->check_pn_special();

            if ($special != 'no') {
                $args['cost'] = $special;
            }
            
            if($this->get_option('free_price_for_no_discount_products_enable') == 'yes' ){
                $shipping_price = $this->check_no_discounts_value($this->get_option('free_price_for_no_discount_products'));
                
                if($shipping_price){
                    $args['cost'] = 0;
                } else {
                    $args['cost'] = 50;
                }
            }
                        
            
            $this->add_rate($args);
        }

        function check_no_discounts_value($above) {
            global $woocommerce;
            
            $normal_products_cost = 0;
            $cart = $woocommerce->cart;

            foreach ($cart->cart_contents as $product_id) {
                $product = get_product($product_id['product_id']);
                if (!$product->is_on_sale()) {
                    $normal_products_cost += $product->get_price() * $product_id['quantity'];
                }
            }
            if($normal_products_cost>=$above){
                return true;
            }
            
            return false;
            
        }        
        
        function check_pn_special() {
            global $woocommerce;
            $one_without_discount_enabled = $this->get_option('one_without_discount_enable');
            $one_without_discount_price = $this->get_option('one_without_discount_price');
            $cheap_shipping_class_enable = $this->get_option('cheap_shipping_class_enable');
            $cheap_shipping_class_id = $this->get_option('cheap_shipping_class');
            $cheap_shipping_class_price = $this->get_option('cheap_shipping_class_price');
            $all_with_discount_enable = $this->get_option('all_with_discount_enable');
            $all_with_discount_price = $this->get_option('all_with_discount_price');

            $cart = $woocommerce->cart;
            $has_discounts = 0;
            $has_shipping_class = 0;

            foreach ($cart->cart_contents as $product_id) {
                $product = get_product($product_id['product_id']);
                if ($cheap_shipping_class_enable) {
                    if ($product->get_shipping_class_id() == $cheap_shipping_class_id) {
                        $has_shipping_class++;
                    }
                }

                if ($product->is_on_sale()) {
                    $has_discounts++;
                }
            }
//            echo 'st:' . $one_without_discount_enabled . ':';
            if ($one_without_discount_enabled == 'yes' && count($has_discounts) > 0) {
                return $one_without_discount_price;
            }

            if ($cheap_shipping_class_enable == 'yes' && $has_shipping_class) {
                return $cheap_shipping_class_price;
            }

            if ($all_with_discount_enable == 'yes' && count($cart->cart_contents) == $has_discounts) {
                return $all_with_discount_price;
            }
            return 'no';
        }



        /**
         * validate_additional_costs_field function.
         *
         * @access public
         * @param mixed $key
         * @return void
         */
        function validate_postnord_rates_table_field($key) {
            return false;
        }

        /**
         * generate_additional_costs_html function.
         *
         * @access public
         * @return void
         */
        function generate_postnord_rates_table_html() {
            global $woocommerce;
            ob_start();
            ?>
            <tr valign="top">
                <th scope="row" class="titledesc"><?php _e('Costs', 'woocommerce'); ?>:</th>
                <td class="forminp" id="<?php echo $this->id; ?>_postnord_rates">
                    <table class="shippingrows widefat" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="check-column"><input type="checkbox"></th>
                                <th><?php _e('Total from', 'woocommerce'); ?></th>
                                <th><?php _e('Total to', 'woocommerce'); ?></th>
                                <th><?php _e('Shipping Price', 'woocommerce'); ?></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="4"><a href="#" class="add button"><?php _e('+ Add row', 'woocommerce'); ?></a> <a href="#" class="remove button"><?php _e('Delete selected rows', 'woocommerce'); ?></a></th>
                            </tr>
                        </tfoot>
                        <tbody class="postnord_rates">
                            <?php
                            $i = -1;
                            if ($this->postnord_rates) {
                                foreach ($this->postnord_rates as $postnord_rate) {
                                    $i++;

                                    echo '<tr class="postnord_rate">
		                			<th class="check-column"><input type="checkbox" name="select" /></th>
				                    <td><input type="number" step="any" min="0" value="' . esc_attr($postnord_rate['total_from']) . '" name="' . esc_attr($this->id . '_rates[' . $i . '][total_from]') . '" placeholder="' . __('0.00', 'woocommerce') . '" size="4" /></td>
				                    <td><input type="number" step="any" min="0" value="' . esc_attr($postnord_rate['total_to']) . '" name="' . esc_attr($this->id . '_rates[' . $i . '][total_to]') . '" placeholder="' . __('0.00', 'woocommerce') . '" size="4" /></td>
				                    <td><input type="text" value="' . esc_attr($postnord_rate['cost']) . '" name="' . esc_attr($this->id . '_rates[' . $i . '][cost]') . '" placeholder="' . __('0.00', 'woocommerce') . '" size="4" /></td>
			                    </tr>';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        jQuery(function() {

                            jQuery('#<?php echo $this->id; ?>_postnord_rates').on('click', 'a.add', function() {

                                var size = jQuery('#<?php echo $this->id; ?>_postnord_rates tbody .postnord_rate').size();

                                jQuery('<tr class="postnord_rate">\
                                                            <th class="check-column"><input type="checkbox" name="select" /></th>\
                                        <td><input type="number" step="any" min="0" name="<?php echo $this->id; ?>_rates[' + size + '][total_from]" placeholder="0.00" size="4" /></td>\
                                        <td><input type="number" step="any" min="0" name="<?php echo $this->id; ?>_rates[' + size + '][total_to]" placeholder="0.00" size="4" /></td>\
                                        <td><input type="text" name="<?php echo $this->id; ?>_rates[' + size + '][cost]" placeholder="0.00" size="4" /></td>\
                                </tr>').appendTo('#<?php echo $this->id; ?>_postnord_rates table tbody');

                                return false;
                            });

                            // Remove row
                            jQuery('#<?php echo $this->id; ?>_postnord_rates').on('click', 'a.remove', function() {
                                var answer = confirm("<?php _e('Delete the selected rates?', 'woocommerce'); ?>")
                                if (answer) {
                                    jQuery('#<?php echo $this->id; ?>_postnord_rates table tbody tr th.check-column input:checked').each(function(i, el) {
                                        jQuery(el).closest('tr').remove();
                                    });
                                }
                                return false;
                            });

                        });
                    </script>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

        /**
         * process_postnord_rates function.
         *
         * @access public
         * @return void
         */
        function process_postnord_rates() {
            // Save the rates

            $postnord_rates_records = array();
            $postnord_rates = array();

            if (!empty($_POST[$this->id . '_rates'])) {
                foreach ($_POST[$this->id . '_rates'] as $postnord_rate) {
                    $postnord_rates[] = array(
                        'total_from' => number_format($postnord_rate['total_from'], 2, '.', ''),
                        'total_to' => number_format($postnord_rate['total_to'], 2, '.', ''),
                        'cost' => number_format($postnord_rate['cost'], 2, '.', '')
                    );
                }
            }

            update_option($this->postnord_rate_option, $postnord_rates);

            $this->get_postnord_rates();
        }

        /**
         * get_postnord_rates function.
         *
         * @access public
         * @return void
         */
        function get_postnord_rates() {
            $this->postnord_rates = array_filter((array) get_option($this->postnord_rate_option));
        }

        function get_points() {
            global $woocommerce;
            return $woocommerce;
        }

    }

    // end Postnord
}

/**
 * Add shipping method to WooCommerce
 * */
function add_postnord($methods) {
    $methods[] = 'WC_Shipping_Postnord';
    return $methods;
}

add_filter('woocommerce_shipping_methods', 'add_postnord');
?>
