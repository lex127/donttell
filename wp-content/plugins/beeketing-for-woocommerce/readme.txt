=== Beeketing for WooCommerce: Upsell & Cross-sell, Email Marketing, Promotions ===
Contributors: beeketing
Tags: woocommerce, ecommerce, marketing, email, popup
Stable tag: trunk
Requires at least: 4.0.0
Tested up to: 4.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Popup, email marketing, social media boost, product recommendation, upsell & cross-sell to boost sales for WooCommerce stores.

== Description ==

Built to seamlessly integrate with WooCommerce, Beeketing plugin is a comprehensive automated sales & marketing solution for WooCommerce online stores. We provide essential tools any eCommerce store needs to improve conversion rates, nurture customers, and increase sales.

= BOOST SALES =

Benefit: Increase average order value by upselling and cross-selling

- Upselling: when customers add products to cart, upsell relevant items customers might want to replace or buy together
- Cross-selling: when customers view products, cross-sell a bundle of complementary products that go well together
- Offer discount for upsell & cross-sell products to motivate customers to get value deals
- Sales Gamification: set up an order value threshold and motivate customers to spend more to reach the goal and receive a discount for total cart
- Last-step Upsell: automatically upsell more products based on on-cart items when customers review their order at cart page
- Smart auto-recommendation or User-defined recommendation: you can create custom offers for target products, or let the app automatically generate upsell & cross-sell for all products based on customer’s behavior and interests on the store.

= CHECKOUT BOOST =

Benefit: increase checkout rate and prevent cart abandonment by running different types of incentive offers ; Bring social referral traffic via social sharing by customers

- Ask customers to share their cart on Facebook / Twitter to receive a discount code / free shipping / free gift. When customers share their cart to social network, they bring more social referral traffic to your store.
- Add a countdown timer to incentive offer to urge customers to take the offer before time runs out
- Run exit-intent incentive offer to target cart abandoning visitors
- Post-purchase Upsell offer: after customers finish their purchase, offer customer a discount / free-shipping code and upsell more items to motivate repeat purchase

= PERSONALIZED RECOMMENDATION =

Benefit: Increase sales by suggesting the right products customers may be interested in, based on their behavior and purchase history.

- Add product recommendation sliders in all website pages
- Types of recommendation: Bought This Also Bought, Recently Viewed, Featured Recommendation, Best Sellers of Store, Cart Recommendation
- All recommendations are generated automatically and smartly by built-in advanced algorithms that become more and more exact by learning from store’s historical data and customer shopping patterns
- Customizable design to make product sliders match into store’s theme

= MAILBOT | EMAIL MARKETING =

Benefit: Send personalized email campaigns to customers to engage, sell more, and nurture loyal customers. A perfect alternative to Mailchimp focusing on increasing sales for eCommerce.

- Pre-designed email campaigns with carefully written content, customized with your name, title, store domain, logo, social media
- Newsletter Bot plans, drafts and schedules weekly newsletters that are personalized to each customer based on their interests and behavior on store
- Tools for you to set up product recommendation rules, date & time to schedule, subject line and template libraries to help Mailbot build perfect campaigns
- Segment customers into targeted groups so you can send tailored content that is relevant to them.
- Browse Abandonment & Cart Abandonment campaigns to recover abandoned sales and lost customers
- Nurture loyal customers by sending welcome email from store’s founder and rewarding coupons for repeat customers
- Promote New Arrivals, Featured Recommendations, and Products that can be bought again

= SALES POP | RECENT-SALES NOTIFICATIONS =

Benefit: Leverage social proof to encourage customers to add items to cart, build trust and authenticity for your store

- Show “Rachel from San Francisco has just bought Jersey T-shirt 5 mins ago” notification popup when there is a sales
- Auto sync with your store’s sales data to turn sales into notification in real time
- Got no sales yet but want to create the sense of a busy store? Create custom notifications for products you want to promote as hot sellers
- Design notification themes, position so they display perfectly on your store

= QUICK FACEBOOK CHAT =

Benefit: Implement an instant Facebook chat tool on the site so customers can send messages directly to your page Messenger on the store

- Chat and support customers anywhere on Facebook Messenger.
- Make the response time quick and keep the customers happy.
- Monitor the messages from any computer or phone with Facebook.
- Reduction in phone calls and emails.

= More About Beeketing =

- Visit our official website: https://beeketing.com
- Contact us for support: hi@beeketing.com
- Read our eCommerce blog: https://beeketing.com/blog

[youtube https://www.youtube.com/watch?v=BE-SZ4vL2CE]

== Installation ==

= Quick setup: =

1. Download & Activate this plugin on WordPress Admin Dashboard
2. Sign up for a free Beeketing account. If you already had a Beeketing account, simple log in.
3. Allow us to connect with your store and sync data.
4. Start by using any apps you like right on your Wordpress Admin Dashboard!

= Detailed Instruction: =

https://beeketing.freshdesk.com/solution/articles/6000105402-how-to-install-a-beeketing-s-app-for-woocommerce-stores

== Frequently Asked Questions ==

= 1. What is Beeketing platform? Why does it have many apps within when I install the plugin? =

Beeketing platform consists many separate apps, each perform a unique feature to help improve an element in your storefront and marketing & sales conversion flow.
First, you need to install this plugin to connect your store with Beeketing system. Then you can choose any apps to run on your store.

= 2. Where can I find Beeketing apps after I install this plugin? =

After installing Beeketing plugin into your store, go to WooCommerce >> Beeketing >> You will see all available apps there. You can directly install and activate these apps right on admin dashboard.

= 3. Where can I find more details about Apps in Beeketing platform ? =

- Beeketing official landing page for WooCommerce stores is: https://beeketing.com/beeketing-for-woocommerce/
- All Beeketing apps can be found here: https://beeketing.com/apps

= 4. Where can I find pricing for Beeketing apps? =

- Some Beeketing apps are forever-FREE (Better Coupon Box, Happy Email, Quick Facebook Chat)
- Some are monthly-charged (Boost Sales, Checkout Boost, Email Automation, We Ship To). You have 15-day free trial to use these apps before paying for them. If you cancel the apps within the free trial, you will not be charged!
- Some are commission-based priced (Personalized Recommendation). You will be charged a small 4% of extra sales the app generates for you each month. No set-up, fixed, or unexpected costs. FREE if the app makes you no sales! So no brainer on your business budget!

= 5. Can I request more free trial for Beeketing apps? =

We are flexible to extend more free-trial case-by-case. We want to make sure our customers have enough time to experience our apps before making long-term investment. If customers encounter technical issues during the free trial and it takes us time to fix for them, they can ask for more free trial.

= 6. Can I request new features for Beeketing apps to better fit my business goal? =

Sure. Give us any feedback, recommendations, feature requests regarding Beeketing apps, support services at hi@beeketing.com. Our support agents will respond any emails from you within 24 hours.

= 7. Where can I find documentation and other guides? =

All general & detailed guides to help you install Beeketing platform & its apps into your WooCommerce store can be found here: https://beeketing.freshdesk.com/solution/categories/6000129224

= 8. Where can I get direct support from the developer? =

The best way to reach us for support is by sending an email to hi@beeketing.com, our support agents will get back to you within 24 hours. We are dedicated to super-fast & efficient customer support.

= 9. Do I have to pay for technical support from Beeketing? =

No. Our support is forever free for all customers. Our marketing & engineer experts can help you tackle any sales, marketing, UI/UX and technical issues. Even when you stop using Beeketing platform, we always support you with all questions you may have about making your WooCommerce store sell better

= 10. Do I need to edit my WooCommerce theme? =

No, Beeketing works smoothly with any WooCommerce themes. You just need to install the plugin, add your API key and we will take care of the rest.

== Screenshots ==

4. [Boost Sales] When customers add an item to cart, an up-sell offers show up to suggest them a list of better options they may want to buy more or replace added items.
5. [Boost Sales] Cross-sell feature allows you to bundle products that go well together into an irresistible combo your customers love, then suggest to them when they view any items in the bundle.
6. [Boost Sales] In Sales Motivator feature, game design technique is applied in smart coupon popups that motivate your shoppers to add more products to cart. This also increases user engagement on your websites, motivates your customers to spend more, come back & refer you to their friends.
7. [Boost Sales] Last Step Up-sell: When customers click “Check Out” button in cart page, one last up-sell offer will show up to suggest them more items they might want to add before check out. These products are well-chosen based on their shopping behaviors and your sales history, which makes customers highly likely to purchase.
8. [Checkout Boost] When customers reach cart page, a popup gradually appears to catch their attention, ask them to share their cart on facebook / twitter to receive a coupon code / free gift / free shipping (up to you).
9. [Checkout Boost] You can prepare a message for your customers to share on their social profiles to introduce your brand with their networks.
10. [Checkout Boost] Cart Notification features motivates customers to spend a little more in order to receive your checkout-boost offer.
11. [Checkout Boost] By motivating customers to share your products, you will receive lots of referral traffic from social word-of-mouth.
12. [Email Automation] This app helps you send personalized follow-up emails to customers to drive more sales from email marketing. This is Sales Nurturing campaign - send emails to visitors who are interested in some items but didn’t buy.
13. [Email Automation] Sales Return campaign - re-engage customers who haven’t come back within 30 days to increase retention rate and boost revenue from old customers.
14. [Email Automation] Reward Coupon campaign - send coupons to reward loyal customers and encourage them to buy even more
15. [Email Automation] Smart Bot newsletter - automatically generate newsletter to customers based on their triggered actions to boost sales for you
16. [Quick Facebook Chat] Help you connect with customers via Facebook Messenger right on your store and bring customer support to another level for your business
17. [Quick Facebook Chat] We make sure Quick Facebook Chat window shows responsively on both desktop and mobile so you lose no potential customers wherever they come from.
18. [Personalized Recommendation] Collect sales history & purchase behaviors of your customers, analyze to understand what they need, then suggest the right products to the right customer at the right place.
19. [Personalized Recommendation] “Customer bought this also bought” suggestion just like Amazon
20. [Personalized Recommendation] “Frequently viewed and featured recommendation” reminds customers of products they’re interested in and suggests most popular items
21. [Personalized Recommendation] “Cart recommendation” recommends products based on customers’ cart order.

== Changelog ==
* 3.0.4 - 09/09/2017
- Fix array syntax support old PHP version

* 3.0.3 - 08/09/2017
- Fix migrate process

* 3.0.2 - 07/09/2017
- Fix create order bug

* 3.0.1 - 06/09/2017
- Fix wrong order product variant id

* 3.0.0 - 06/09/2017
- Update core plugin
- Optimize plugin

* 2.9.9 - 11/08/2017
- New plugin dashboard

* 2.9.8 - 11/07/2017
- Update app card style

* 2.9.7 - 10/07/2017
- Update images data

* 2.9.6 - 13/06/2017
- Fix get post data

* 2.9.5 - 05/06/2017
- Fix order success

* 2.9.4 - 24/05/2017
- Fix post data header can not decode

* 2.9.3 - 24/05/2017
- Fix get post data

* 2.9.2 - 24/05/2017
- Fix variant data

* 2.9.1 - 25/04/2017
- Fix currency format

* 2.9.0 - 13/04/2017
- Fix order sync

* 2.8.9 - 12/04/2017
- Fix collection handle
- Update cron

* 2.8.8 - 11/04/2017
- Fix major bug about sync data
- Bug with woocommerce 3.x

* 2.8.7 - 10/04/2017
- Fix plugin with woocommerce 3.x

* 2.8.6 - 07/04/2017
- Update plugin to works with woocommerce 3.x

* 2.8.5 - 04/04/2017
- Fix plugin bugs

* 2.8.4 - 03/04/2017
- Add handle collection, fix api product variant

* 2.8.3 - 31/03/2017
- Remove json_last_error function

* 2.8.2 - 30/03/2017
- Fix currency format

* 2.8.1 - 21/03/2017
- Update api for sales pop app

* 2.8.0 - 20/03/2017
- Fix price format

* 2.7.9 - 16/03/2017
- Fix price format

* 2.7.8 - 14/03/2017
- Sync removed products

* 2.7.7 - 02/03/2017
- Fix order contact

* 2.7.6 - 22/02/2017
- Fix cron sync collects loop

* 2.7.5 - 21/02/2017
- Remove Better Coupon Box app from plugin

* 2.7.4 - 17/02/2017
- Fix add cart api

* 2.7.3 - 13/02/2017
- Fix warning type scalar in wp_localize_script

* 2.7.2 - 09/02/2017
- Fix api get configs

* 2.7.1 - 07/02/2017
- Update product visible condition

* 2.7.0 - 07/02/2017
- Reduce cron time

* 2.6.9 - 03/02/2017
- Remove bk attribute after add cart

* 2.6.8 - 02/02/2017
- Fix api get configs

* 2.6.7 - 20/01/2017
- Fix api product tag

* 2.6.6 - 18/01/2017
- Add api update product tag

* 2.6.5 - 20/12/2016
- Update loader script

* 2.6.4 - 15/12/2016
- Fix share facebook checkout boost app

* 2.6.3 - 24/11/2016
- Update plugin to use beeketing sdk2

* 2.6.2 - 16/11/2016
- Fix cron job run in order

* 2.6.1 - 10/11/2016
- Fix api get missing header

* 2.6.0 - 10/11/2016
- Fix api get

* 2.5.9 - 01/11/2016
- Validate order data, add version info to api

* 2.5.8 - 24/10/2016
- Fix cboost share facebook

* 2.5.7 - 21/10/2016
- Fix user query

* 2.5.6 - 20/10/2016
- Fix sync bug

* 2.5.5 - 19/10/2016
- Update plugin menu page permission

* 2.5.4 - 14/10/2016
- Add plugin version header to sync request

* 2.5.3 - 14/10/2016
- Fix sync loop

* 2.5.2 - 12/10/2016
- Update cart data

* 2.5.1 - 04/10/2016
- Update sync cron jobs

* 2.5.0 - 03/10/2016
- Fix boost sales app variant title

* 2.4.9 - 30/09/2016
- Improve plugin dashboard

* 2.4.8 - 20/09/2016
- Fix add not valid product, blank select option

* 2.4.7 - 31/08/2016
- Fix variants discount bug

* 2.4.6 - 30/08/2016
- Fix add cart session, bugs

* 2.4.5 - 24/08/2016
- Fix Boost Sales, Checkout Boost, Personalized Recommendation app bugs

* 2.4.4 - 12/08/2016
- Update api
- Fix redirect bug

* 2.4.3 - 04/08/2016
- Send plugin version to beeketing

* 2.4.2 - 25/07/2016
- Fix conflict with plugin Woocommerce Product Gallery Slider

* 2.4.1 - 22/07/2016
- Fix conflict with jquery

* 2.4.0 - 21/07/2016
- Fix bugs

* 2.3.9 - 18/07/2016
- Update plugin events
- Fix bugs

* 2.3.8 - 15/07/2016
- Fix bug deactive and uninstall plugin
- Optimize sign up flow
- Fix missing style

* 2.3.7 - 14/07/2016
- Add message require woocommerce plugin
- Fix bugs

* 2.3.6 - 11/07/2016
- Fix bug reload plugin

* 2.3.5 - 11/07/2016
- Add function to remove app from plugin dashboard

* 2.3.4 - 08/07/2016
- Fix api order

* 2.3.3 - 06/07/2016
- Fix login and sign up flow

* 2.3.2 - 04/07/2016
- Update api functions
- Update plugin structure
- Update plugin menu icon

* 2.3.1 - 30/06/2016
- Fix api to support older php version

* 2.3.0 - 30/06/2016
- Add get collects by collection id api
- Update uninstall hook
- Fix app order statistics

* 2.2.9 - 27/06/2016
- Improve plugin dashboard
- Fix root dir for lower php version

* 2.2.8 - 27/06/2016
- Fix admin script

* 2.2.7 - 24/06/2016
- Optimize javascript for admin

* 2.2.6 - 23/06/2016
- Improve flow register account
- Improve plugin speed

* 2.2.5 - 21/06/2016
- Fix warning when create new customer

* 2.2.4 - 16/06/2016
- Fix sync customer api
- Fix error with check api key

* 2.2.3 - 31/05/2016
- Add deactivate and uninstall hooks

* 2.2.1 - 19/05/2016
- Fix customer sync
- Fix order sync

* 2.2.0 - 18/05/2016
- Add beeketing notifier system

* 2.1.5 - 13/05/2016
- Fix beeketing menu not showing up

* 2.1.4 - 09/05/2016
- Fix product api array

* 2.1.3 - 09/05/2016
- Fix array error on low php version

* 2.1.2 - 03/05/2016
- Fix product time format

* 2.1.1 - 28/04/2016
- Move script content to CDN

* 2.1.0 - 27/04/2016
- Change registration flow

* 2.0.2 - 02/04/2016
- Fix product format time

* 2.0.1 - 30/03/2016
- Fix file not found error when installing plugin

* 2.0.0 - 4/02/2016
- Add more apps: Checkout Boost for WooCommerce, Boost Sales for WooCommerce, Email Automation for WooCommerce, Product Recommendation for WooCommerce, Happy Email for WooCommerce

* 1.0.0 - 20/10/2015
- Initial Release: Better Coupon Box

== Upgrade Notice ==

== Support ==

You can get in touch with us in many ways:

- Send us your queries via our website
- Chat live with us in www.beeketing.com or within all Beeketing apps
- Send us direct email at hi@beeketing.com
- Find all general & detailed guides to help you install Beeketing platform & its apps into your WooCommerce store here: https://beeketing.freshdesk.com/solution/categories/6000129224

Our support team with get back to you within 24 hours. We’re happy to help you with any questions you might have!

== Developers ==

Beeketing for WooCommerce is a marketing automation platform to turn your visitors into revenue.
Try it for free at [www.beeketing.com](https://beeketing.com/beeketing-for-woocommerce/?utm_channel=appstore&utm_medium=woolisting&utm_fromapp=beeketing)

Find us on:

- Website: https://beeketing.com
- Blog: https://blog.beeketing.com/
- Facebook: https://www.facebook.com/beeketing
- Twitter: https://twitter.com/beeketing

= Beeketing - Our mission is to help you sell better =
