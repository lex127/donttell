<div class="beeketing-banner-dashboard">
    <div class="bk-main-content">
        <h3>Looking for ideas to grow sales for your online business?</h3>
        <p>Beeketing is the #1 Marketing Automation platform for e-Commerce.</p>
        <p>Get started today to start selling like Amazon with our free marketing automation tools </p>

        <h4>And BOOST SALES drastically </h4>

        <a href="<?php _e($plugin_url); ?>" class="button button-primary button-hero dashboard-btn tracking-target"
           data-event="wc_homebanner_getstarted">Get started with Beeketing</a>
        <p>
            <em>Learn more about <a href="<?php _e($learn_more_url); ?>" target="_blank">Beeketing for WooCommerce</a></em>
        </p>
    </div>
</div>
