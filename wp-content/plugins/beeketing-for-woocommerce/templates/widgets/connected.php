<div class="bk-dashboard">
    <div class="apps-installed-succesful">
        <div class="apps-installed-content">
            <h1>Beeketing is connected!</h1>

            <a href="<?php _e($plugin_url); ?>" class="button button-hero btn-bk-dashboard">dashboard</a>
        </div>
    </div>
</div>
