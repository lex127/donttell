<div class="notice notice-success beeketing-warning is-dismissible">
    <h2>Beeketing is ready to boost sales for your online store! Are you?</h2>
    <p>Lose no more potential customers and sales by setting up Beeketing - #1 Marketing Automation platform for WooCommerce store - today!</p>
    <p>
        <a class="button button-primary tracking-target" data-event="wc_htmlbar_setupbkt" href="<?php _e($plugin_url); ?>">Set up Beeketing</a>
        <a class="button tracking-target" data-event="wc_htmlbar_learnmore" href="<?php _e($learn_more_url); ?>" target="_blank">Learn more</a>
    </p>
</div>