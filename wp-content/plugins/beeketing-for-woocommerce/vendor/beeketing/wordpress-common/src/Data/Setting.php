<?php
/**
 * Plugin setting keys
 *
 * @since      1.0.0
 * @author     Beeketing
 *
 */

namespace BKWPCommon\Data;


class Setting
{
    const SETTING_API_KEY = 'api_key';
    const SETTING_ACCESS_TOKEN = 'access_token';
    const SETTING_SNIPPET = 'snippet';
    const SETTING_SHOP_ID = 'shop_id';
    const SETTING_CLIENT_SECRET = 'client_secret';
    const SETTING_PLUGIN_VERSION = 'plugin_version';
    const SETTING_DOMAIN = 'domain';
    const SETTING_SITE_URL = 'site_url';
    const SETTING_TOTAL_INSTALLED_APPS = 'total_installed_apps';
}