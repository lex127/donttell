<?php
/**
 * Plugin constants
 *
 * @since      1.0.0
 * @author     Beeketing
 *
 */

namespace BKWPCommon\Data;


class Constant
{
    const PLATFORM = 'wordpress';
    const COOKIE_CART_TOKEN_LIFE_TIME = 2592000;
    const CART_TOKEN_KEY = '_beeketing_cart_token';
}