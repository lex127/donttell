<?php
/**
 * Created by PhpStorm.
 * User: tungquach
 * Date: 30/03/2017
 * Time: 19:00
 */

namespace BKWPCommon\Manager;


use BKWPCommon\Data\Api;
use BKWPCommon\Libraries\Helper;

class CollectionManager
{
    /**
     * Count collections
     *
     * @return mixed
     */
    public function get_collections_count()
    {
        $result = wp_count_terms( 'product_cat', array(
            'hide_empty' => false,
        ) );

        return $result;
    }

    /**
     * Get collection by id
     *
     * @param $id
     * @return array
     */
    public function get_collection_by_id( $id )
    {
        $term = get_term( $id, 'product_cat' );

        // Traverse all terms
        if ( !is_wp_error( $term ) ) {
            return $this->format_collection( $term );
        }

        return array();
    }

    /**
     * Get collections
     *
     * @param $title
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function get_collections( $title, $page = Api::PAGE, $limit = Api::ITEM_PER_PAGE )
    {
        $args = array(
            'hide_empty' => false,
        );

        // Limit
        if ( $limit ) {
            $args['number'] = $limit;
        }

        // Offset
        if ( $page ) {
            $page = $page - 1;
            $args['offset'] = $page * $limit;
        }

        // Title
        if ( $title ) {
            $args['search'] = $title;
        }

        $result = array();
        // Get terms
        $terms = get_terms( 'product_cat', $args );

        // Traverse all terms
        if ( !is_wp_error( $result ) ) {
            foreach ($terms as $term) {
                $result[] = $this->format_collection( $term );
            }
        } else {
            $result = $terms;
        }

        return $result;
    }

    /**
     * Format collection
     *
     * @param $collection
     * @return array
     */
    private function format_collection( $collection )
    {
        // Get category image
        $image = '';
        if ($image_id = get_woocommerce_term_meta( $collection->term_id, 'thumbnail_id') ) {
            $image = wp_get_attachment_url( $image_id );
        }

        return array(
            'id' => $collection->term_id,
            'title' => $collection->name,
            'handle' => ltrim( str_replace( get_site_url(), '', get_term_link( $collection ) ), '/' ),
            'published_at' => Helper::format_date( new \DateTime() ),
            'image_url' => $image,
        );
    }
}