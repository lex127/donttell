<?php
/**
 * Created by PhpStorm.
 * User: tungquach
 * Date: 31/03/2017
 * Time: 13:40
 */

namespace BKWPCommon\Manager;


use BKWPCommon\Data\Api;
use BKWPCommon\Libraries\Helper;
use BKWPCommon\Libraries\QueryHelper;

class ProductManager
{
    private $image_manager;
    private $variant_manager;

    public function __construct()
    {
        $this->image_manager = new ImageManager();
        $this->variant_manager = new VariantManager();
    }

    /**
     * Count products
     *
     * @return mixed
     */
    public function get_products_count()
    {
        global $wpdb;
        $count = $wpdb->get_var(
            $wpdb->prepare(
                "
                SELECT COUNT(ID)
                FROM $wpdb->posts
                WHERE post_type = %s
                  AND post_status = %s
                  AND post_password = %s
                  AND ID NOT IN ( " . QueryHelper::get_exclude_products_id() . " )
                ",
                "product",
                "publish",
                null
            )
        );

        return $count;
    }

    /**
     * Get product by id
     *
     * @param $id
     * @return array
     */
    public function get_product_by_id( $id )
    {
        global $wpdb;
        $result = $wpdb->get_row(
            $wpdb->prepare(
                "
                SELECT *
                FROM $wpdb->posts
                WHERE post_type = %s
                  AND post_status = %s
                  AND post_password = %s
                  AND ID = %d
                ",
                "product",
                "publish",
                null,
                $id
            )
        );

        $product = $result ? $this->format_product( $result ) : array();

        return $product;
    }

    /**
     * Get products
     *
     * @param $title
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function get_products( $title = null, $page = Api::PAGE, $limit = Api::ITEM_PER_PAGE )
    {
        $offset = ( $page - 1 ) * $limit;

        global $wpdb;
        $result = $wpdb->get_results(
            $wpdb->prepare(
                "
                SELECT *
                FROM $wpdb->posts
                WHERE post_type = %s
                  AND post_status = %s
                  AND post_password = %s
                  AND post_title LIKE %s
                  AND ID NOT IN ( " . QueryHelper::get_exclude_products_id() . " )
                LIMIT %d
                OFFSET %d
                ",
                "product",
                "publish",
                null,
                "%" . $title . "%",
                $limit,
                $offset
            )
        );

        $products = array();
        // Traverse all result
        foreach ($result as $item) {
            $products[] = $this->format_product( $item );
        }

        return $products;
    }

    /**
     * Format product
     *
     * @param $product
     * @return array
     */
    private function format_product( $product )
    {
        $product = wc_get_product( $product->ID );

        // Check wc version
        if ( Helper::is_wc3() ) {
            $product_id = $product->get_id();
            $post = get_post( $product_id );
        } else {
            $product_id = $product->id;
            $post = $product->post;
        }

        $tags = wp_get_post_terms( $product_id, 'product_tag', array( 'fields' => 'names' ) );
        $images = $this->image_manager->get_product_images_by_product( $product );

        // Get variants
        $variants = $this->variant_manager->get_variants_by_product( $product );

        return array(
            'id' => $product_id,
            'published_at' => $post->post_modified_gmt,
            'handle' => ltrim( str_replace( get_site_url(), '', $product->get_permalink() ), '/' ),
            'title' => $product->get_title(),
            'vendor' => '',
            'tags' => $tags ? implode( ', ', $tags ) : '',
            'description' => $post->post_excerpt,
            'images' => $images,
            'image' => isset($images[0]['src']) ? $images[0]['src'] : '',
            'variants' => $variants,
        );
    }

    /**
     * Update product
     *
     * @param $id
     * @param $content
     * @return array
     */
    public function update_product( $id, $content )
    {
        $tags = isset( $content['tags'] ) ? explode(',', $content['tags']) : array();

        $product_tags = array();
        foreach ( $tags as $tag ) {
            if ( $tag ) {
                $args = array(
                    'hide_empty' => false,
                    'fields' => 'ids',
                    'name' => $tag
                );

                $tag_ids = get_terms( 'product_tag', $args );

                if ( !$tag_ids ) {
                    $defaults = array(
                        'name' => $tag,
                        'slug' => sanitize_title( $tag ),
                    );

                    $insert = wp_insert_term( $defaults['name'], 'product_tag', $defaults );
                    $id = $insert['term_id'];
                    $product_tags[] = $id;

                } else {
                    $product_tags = array_merge( $product_tags, $tag_ids );

                }
            }
        }

        // Update tag
        if ( $product_tags ) {
            wp_set_object_terms($id, $product_tags, 'product_tag');
        }

        return $this->get_product_by_id( $id );
    }
}