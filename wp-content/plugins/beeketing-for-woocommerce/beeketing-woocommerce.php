<?php
/**
 * Plugin Name: Beeketing For WooCommerce
 * Plugin URI: https://beeketing.com
 * Description: Built to seamlessly integrate with WooCommerce, Beeketing is a platform of marketing & sales apps to grow sales for e-Commerce stores. Features include popup builders, up-sell & cross-sell offers, social lead generation, email automation service, product recommendation, and more. All to help you <strong>sell automatically and boost revenue to the max</strong>.
 * Version: 3.0.4
 * Author: Beeketing
 * Author URI: https://beeketing.com
 */

use BeeketingWoocommerce\Api\App;
use BKWPCommon\Api\BridgeApi;
use BeeketingWoocommerce\Data\Constant;
use BeeketingWoocommerce\PageManager\AdminPage;
use BKWPCommon\Data\Setting;
use BKWPCommon\Libraries\Helper;
use BKWPCommon\Libraries\SettingHelper;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Define plugin constants
define( 'BEEKETINGWOOCOMMERCE_VERSION', '3.0.4' );
define( 'BEEKETINGWOOCOMMERCE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'BEEKETINGWOOCOMMERCE_PLUGIN_DIRNAME', __FILE__ );

// Require plugin autoload
require_once( BEEKETINGWOOCOMMERCE_PLUGIN_DIR . 'vendor/autoload.php' );

// Get environment
$env = Helper::get_local_file_contents( BEEKETINGWOOCOMMERCE_PLUGIN_DIR . 'env' );
$env = trim( $env );

if ( !$env ) {
    throw new Exception( 'Can not get env' );
}

define( 'BEEKETINGWOOCOMMERCE_ENVIRONMENT', $env );

if ( ! class_exists( 'BeeketingWooCommerce' ) ):

    class BeeketingWooCommerce {
        /**
         * @var AdminPage $admin_page;
         *
         * @since 1.0.0
         */
        private $admin_page;

        /**
         * @var App $api_app
         *
         * @since 1.0.0
         */
        private $api_app;

        /**
         * @var BridgeApi
         *
         * @since 1.0.0
         */
        private $bridge_api;

        /**
         * @var SettingHelper
         *
         * @since 1.0.0
         */
        private $setting_helper;

        /**
         * @var string
         */
        private $api_key;

        /**
         * The single instance of the class
         *
         * @since 1.0.0
         */
        private static $_instance = null;

        /**
         * Get instance
         *
         * @return BeeketingWooCommerce
         * @since 1.0.0
         */
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }

            return self::$_instance;
        }

        /**
         * Constructor
         *
         * @since 1.0.0
         */
        public function __construct()
        {
            $this->setting_helper = SettingHelper::get_instance();
            $this->setting_helper->set_app_setting_key( Constant::APP_SETTING_KEY );
            $this->api_key = $this->setting_helper->get_settings( Setting::SETTING_API_KEY );

            // Init api app
            $this->api_app = new App( $this->api_key );

            // Bridge api
            $this->bridge_api = new BridgeApi( Constant::APP_SETTING_KEY );

            // Plugin hooks
            $this->hooks();
        }

        /**
         * Hooks
         *
         * @since 1.0.0
         */
        private function hooks()
        {
            // Initialize plugin parts
            add_action( 'plugins_loaded', array( $this, 'init' ) );

            // Add the plugin page Settings and Docs links
            add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_links' ) );

            // Plugin updates
            add_action( 'admin_init', array( $this, 'check_version' ) );

            // Plugin activation
            add_action( 'activated_plugin', array( $this, 'plugin_activation' ) );

            // Register plugin deactivation hook
            register_deactivation_hook( __FILE__, array( $this, 'plugin_deactivation' ) );
        }

        /**
         * Init
         *
         * @since 1.0.0
         */
        public function init()
        {
            $this->ajax();

            if ( is_admin() ) {
                $this->admin_page = new AdminPage();
                add_action( 'current_screen', array( $this, 'screen_action' ) );
                add_action( 'wp_dashboard_setup', array( $this, 'dashboard_widgets' ) );
            }

            // Enqueue scripts
            add_action( 'admin_enqueue_scripts', array( $this, 'register_script' ) );

            // Enqueue styles
            add_action( 'admin_enqueue_scripts', array( $this, 'register_style' ) );

            // Add scripts
            add_action( 'wp_footer', array( $this, 'add_scripts' ) );
        }

        /**
         * Add a widget to the dashboard.
         *
         * This function is hooked into the 'wp_dashboard_setup' action below.
         */
        function dashboard_widgets() {
            $name = null;
            if ( !$this->api_key ) {
                $name = 'beeketing_dashboard_widget_banner';
                wp_add_dashboard_widget( $name, 'Beeketing for WooCommerce', array( $this, $name ) );
            } else {
                $total_installed_apps = $this->setting_helper->get_settings( Setting::SETTING_TOTAL_INSTALLED_APPS );
                if ( !$total_installed_apps ) {
                    $name = 'beeketing_dashboard_widget_connected';
                    wp_add_dashboard_widget( $name, 'Beeketing', array( $this, $name ) );
                }
            }

            if ( $name ) {
                wp_register_style( 'beeketing_global_style', plugins_url( 'dist/css/global.css', __FILE__ ), array(), null, 'all' );
                wp_enqueue_style( 'beeketing_global_style' );

                Helper::high_priority_widget( $name );
            }
        }

        /**
         * Banner dashboard widget
         */
        public function beeketing_dashboard_widget_banner() {
            $plugin_url = admin_url( 'admin.php?page=' . Constant::PLUGIN_ADMIN_URL );
            $learn_more_url = 'https://beeketing.com/?utm_channel=bktdashboard&utm_medium=banner';
            include_once( BEEKETINGWOOCOMMERCE_PLUGIN_DIR . 'templates/widgets/banner.php' );
        }

        /**
         * Connected dashboard widget
         */
        public function beeketing_dashboard_widget_connected() {
            $plugin_url = admin_url( 'admin.php?page=' . Constant::PLUGIN_ADMIN_URL );
            include_once( BEEKETINGWOOCOMMERCE_PLUGIN_DIR . 'templates/widgets/connected.php' );
        }

        /**
         * Screen action
         */
        public function screen_action() {
            $current_screen = get_current_screen();
            if( $current_screen->id == 'toplevel_page_' . Constant::PLUGIN_ADMIN_URL ) {
                $this->api_app->detect_domain_change();
                $this->api_app->check_migrate_after_update();
            } else {
                // Require woocommerce
                if ( !Helper::is_woocommerce_active() ) {
                    add_action( 'admin_notices', array( $this, 'require_wooCommerce' ) );
                    return false;
                }

                // If not api key
                if ( !$this->api_key ) {
                    add_action( 'admin_notices', array( $this, 'notice_set_up' ) );
                }
            }
        }

        /**
         * Notice set up beeketing
         */
        public function notice_set_up()
        {
            $plugin_url = admin_url( 'admin.php?page=' . Constant::PLUGIN_ADMIN_URL );
            $learn_more_url = 'https://beeketing.com/?utm_channel=bktdashboard&utm_medium=htmlbar';
            include_once( BEEKETINGWOOCOMMERCE_PLUGIN_DIR . 'templates/notices/set_up.php' );
        }

        /**
         * Require woocommerce warning
         */
        public function require_wooCommerce() {
            $woocommerce_plugin_url = Helper::get_woocommerce_plugin_url();
            include_once( BEEKETINGWOOCOMMERCE_PLUGIN_DIR . 'templates/notices/require_wc.php' );
        }

        /**
         * Add snippet to store
         *
         * @since 1.0.0
         */
        public function add_scripts()
        {
            echo $this->api_app->get_snippet();
        }

        /**
         * Enqueue and localize js
         *
         * @since 1.0.0
         * @param $hook
         */
        public function register_script( $hook )
        {
            // Load only on plugin page
            if ($hook != 'toplevel_page_' . Constant::PLUGIN_ADMIN_URL) {
                return;
            }

            // Enqueue script
            $app_name = BEEKETINGWOOCOMMERCE_ENVIRONMENT == 'local' ? 'app' : 'app.min';
            wp_register_script( 'beeketing_app_script', plugins_url( 'dist/js/' . $app_name . '.js', __FILE__ ) , array( 'jquery' ), null, false );
            wp_enqueue_script( 'beeketing_app_script' );

            $api_key = $this->setting_helper->get_settings( Setting::SETTING_API_KEY );
            $current_user = wp_get_current_user();
            $beeketing_email = false;
            $beeketing_apps_data = array();
            if ( $api_key ) {
                $beeketing_apps_data = $this->api_app->get_apps();

                // Update total installed apps
                $total_installed_apps = 0;
                foreach ( $beeketing_apps_data as $beeketing_app_data ) {
                    if ( isset( $beeketing_app_data['is_installed'] ) && $beeketing_app_data['is_installed'] ) {
                        $total_installed_apps++;
                    }
                }
                $this->setting_helper->update_settings( Setting::SETTING_TOTAL_INSTALLED_APPS, $total_installed_apps );
            } else {
                $beeketing_email = $this->api_app->get_user_email();
            }

            wp_localize_script( 'beeketing_app_script', 'beeketing_app_vars', array(
                'plugin_url' => plugins_url( '/', __FILE__ ),
                'api_urls' => $this->api_app->get_api_urls(),
                'api_key' => $api_key,
                'user_display_name' => $current_user->display_name,
                'user_email' => $current_user->user_email,
                'site_url' => site_url(),
                'domain' => Helper::beeketing_get_shop_domain(),
                'beeketing_email' => $beeketing_email,
                'env' => BEEKETINGWOOCOMMERCE_ENVIRONMENT,
                'apps' => $beeketing_apps_data,
                'oauth_sign_in_urls' => $this->api_app->oauth_sign_in_urls(),
                'is_woocommerce_active' => Helper::is_woocommerce_active(),
                'woocommerce_plugin_url' => Helper::get_woocommerce_plugin_url(),
            ));
        }

        /**
         * Enqueue style
         *
         * @since 1.0.0
         * @param $hook
         */
        public function register_style( $hook )
        {
            // Load only on plugin page
            if ( $hook != 'toplevel_page_' . Constant::PLUGIN_ADMIN_URL ) {
                return;
            }

            wp_register_style( 'beeketing_app_style', plugins_url( 'dist/css/app.css', __FILE__ ), array(), null, 'all' );
            wp_enqueue_style( 'beeketing_app_style' );
        }

        /**
         * Ajax
         *
         * @since 1.0.0
         */
        public function ajax()
        {
            add_action( 'wp_ajax_beeketingwoocommerce_verify_account_callback', array( $this, 'verify_account_callback' ) );
            add_action( 'wp_ajax_beeketingwoocommerce_app_action_callback', array( $this, 'app_action_callback' ) );
        }

        /**
         * Verify account callback
         *
         * @since 1.0.0
         */
        public function verify_account_callback() {
            $api_key = $this->api_app->register_shop();

            wp_send_json_success( array(
                'api_key' => $api_key,
            ) );
            wp_die();
        }

        /**
         * App action callback
         *
         * @since 1.0.0
         */
        public function app_action_callback()
        {
            // If not app code
            if ( !isset( $_POST['app_code'], $_POST['type'] ) ) {
                wp_send_json_error();
            }

            $app_code = $_POST['app_code'];
            $type = $_POST['type'];
            $this->setting_helper->switch_settings( $app_code );
            $this->api_app->set_app_code( $app_code );

            // Get app action result
            $result = null;
            switch ( $type ) {
                case 'install':
                    $result = $this->api_app->install_app();
                    break;

                case 'uninstall':
                    $result = $this->api_app->uninstall_app( true );
                    break;
            }

            if ( $result ) {
                wp_send_json_success();
            } else {
                wp_send_json_error();
            }
            wp_die();
        }

        /**
         * Plugin links
         *
         * @param $links
         * @return array
         * @since 1.0.0
         */
        public function plugin_links( $links )
        {
            $more_links = array();
            $more_links['settings'] = '<a href="' . admin_url( 'admin.php?page=' . Constant::PLUGIN_ADMIN_URL ) . '">' . __( 'Settings', 'beeketing' ) . '</a>';

            return array_merge( $more_links, $links );
        }

        /**
         * Check version
         *
         * @since 1.0.0
         */
        public function check_version()
        {
            // Update version number if its not the same
            if ( BEEKETINGWOOCOMMERCE_VERSION != $this->setting_helper->get_settings( Setting::SETTING_PLUGIN_VERSION ) ) {
                $this->setting_helper->update_settings( Setting::SETTING_PLUGIN_VERSION, BEEKETINGWOOCOMMERCE_VERSION );

                // Send tracking
                $this->api_app->send_tracking_event( array(
                    'event' => \BeeketingWoocommerce\Data\Event::PLUGIN_VERSION,
                    'beeketing_wc_plugin_version' => BEEKETINGWOOCOMMERCE_VERSION,
                ) );
            }
        }

        /**
         * Plugin activation
         *
         * @param $plugin
         * @since 1.0.0
         */
        public function plugin_activation( $plugin )
        {
            // Send tracking
            $this->api_app->send_tracking_event( array(
                'event' => \BeeketingWoocommerce\Data\Event::PLUGIN_ACTIVATION,
                'beeketing_wc_app_active' => 1,
            ) );

            if ( $plugin == plugin_basename( __FILE__ ) ) {
                exit( wp_redirect( admin_url( 'admin.php?page=' . Constant::PLUGIN_ADMIN_URL ) ) );
            }
        }

        /**
         * Plugin deactivation
         */
        public function plugin_deactivation()
        {
            // Send tracking
            $this->api_app->send_tracking_event( array(
                'event' => \BeeketingWoocommerce\Data\Event::PLUGIN_DEACTIVATION,
                'beeketing_wc_app_active' => 2,
            ) );
        }

        /**
         * Plugin uninstall
         *
         * @since 1.0.0
         */
        public function plugin_uninstall()
        {
            // Send tracking
            $this->api_app->send_tracking_event( array(
                'event' => \BeeketingWoocommerce\Data\Event::PLUGIN_UNINSTALL,
                'beeketing_wc_app_active' => 0,
            ) );

            // Delete all apps setting
            $beeketing_apps_data = $this->api_app->get_apps();
            foreach ( $beeketing_apps_data as $beeketing_app_data ) {
                if (
                    isset( $beeketing_app_data['is_installed'], $beeketing_app_data['app_code'] ) &&
                    $beeketing_app_data['is_installed']
                ) {
                    $app_code = $beeketing_app_data['app_code'];
                    $this->setting_helper->switch_settings( $app_code );
                    $this->api_app->set_app_code( $app_code );

                    // Send uninstall webhook
                    $this->api_app->uninstall_app();
                }
            }

            // Delete plugin setting
            $this->setting_helper->set_app_setting_key( Constant::APP_SETTING_KEY );
            $this->api_app->set_app_code( Constant::APP_CODE );
            $this->setting_helper->delete_settings();
        }
    }

    // Run plugin
    new BeeketingWooCommerce();

endif;