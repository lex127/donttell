<?php
/**
 * Plugin constants
 *
 * @since      1.0.0
 * @author     Beeketing
 *
 */

namespace BeeketingWoocommerce\Data;


class Constant
{
    const APP_CODE = null;
    const PLUGIN_ADMIN_URL = 'beeketing_menu';
    const APP_SETTING_KEY = 'beeketing_woocommerce_settings';
}