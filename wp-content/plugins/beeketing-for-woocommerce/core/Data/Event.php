<?php
/**
 * Created by PhpStorm.
 * User: tungquach
 * Date: 29/08/2017
 * Time: 14:45
 */

namespace BeeketingWoocommerce\Data;


class Event
{
    const PLUGIN_VERSION = 'Beeketing WC Plugin Version';
    const PLUGIN_ACTIVATION = 'WC Activate Beeketing Plugin';
    const PLUGIN_DEACTIVATION = 'WC Inactivate Beeketing Plugin';
    const PLUGIN_UNINSTALL = 'WC Delete Beeketing Plugin';
}