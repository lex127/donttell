<?php
/**
 * App api
 *
 * @since      1.0.0
 * @author     Beeketing
 *
 */

namespace BeeketingWoocommerce\Api;


use BeeketingWoocommerce\Data\Constant;
use BKWPCommon\Api\CommonApi;
use BKWPCommon\Data\AppSettingKeys;
use BKWPCommon\Data\Setting;
use BKWPCommon\Libraries\SettingHelper;

class App extends CommonApi
{
    /**
     * App constructor.
     *
     * @param $api_key
     */
    public function __construct( $api_key )
    {
        $setting_helper = SettingHelper::get_instance();
        $setting_helper->set_app_setting_key( Constant::APP_SETTING_KEY );

        parent::__construct(
            $setting_helper,
            BEEKETINGWOOCOMMERCE_PATH,
            BEEKETINGWOOCOMMERCE_API,
            $api_key,
            Constant::APP_CODE,
            Constant::PLUGIN_ADMIN_URL
        );
    }

    /**
     * Register shop
     *
     * @return bool
     */
    public function register_shop()
    {
        // If not email
        if ( !isset( $_POST['api_key'] ) ) {
            return false;
        }

        $api_key = $_POST['api_key'];
        $this->set_api_key( $api_key );
        if ( $api_key ) {
            $result = $this->update_shop_info();
            $shopResult = $this->get( 'shops/api/' . $api_key );

            // Install app success
            if ( $result && isset( $shopResult['shop']['id'] ) ) {
                // Migrate data
                if (
                    isset( $shopResult['shop']['platform'] ) &&
                    $shopResult['shop']['platform'] != \BKWPCommon\Data\Constant::PLATFORM
                ) {
                    $this->do_migrate( $api_key, false );
                }

                $this->setting_helper->update_settings( Setting::SETTING_API_KEY, $api_key );
                $this->setting_helper->update_settings( Setting::SETTING_SHOP_ID, $shopResult['shop']['id'] );
                return $api_key;
            } else {
                $this->setting_helper->delete_settings();
            }
        }

        return false;
    }

    /**
     * Get beeketing apps
     *
     * @return array
     */
    public function get_apps()
    {
        $result = $this->get( 'wordpress/get_apps' );

        if ( isset( $result['apps'] ) ) {
            return $result['apps'];
        }

        return array();
    }

    /**
     * Get oauth sign in urls
     *
     * @return array
     */
    public function oauth_sign_in_urls()
    {
        $urls = array();
        $setting_keys = SettingHelper::setting_keys();
        foreach ( $setting_keys as $app_code => $setting_key ) {
            $this->set_app_code( $app_code );
            $this->setting_helper->set_app_setting_key( $setting_key );
            $urls[$app_code] = $this->get_oauth_sign_in_url();
        }

        return $urls;
    }

    /**
     * Convert query string
     *
     * @param $key
     * @return string
     */
    public function convert_string( $key )
    {
        return '"' . $key . '"';
    }

    /**
     * Check migrate after update
     */
    public function check_migrate_after_update()
    {
        $api_key = get_option( 'beeketing_api_key' );

        // If detect old setting
        if ( $api_key ) {
            $this->do_migrate( $api_key );
        }
    }

    /**
     * Do migrate from old platform
     *
     * @param $api_key
     * @param bool $redirect
     * @return bool
     */
    private function do_migrate( $api_key, $redirect = true )
    {
        // Clean old settings.
        global $wpdb;

        // Get ignore app keys
        $app_keys_array = AppSettingKeys::get_constants();
        $app_keys_array[] = Constant::APP_SETTING_KEY;
        $app_keys_string = array_map( array( $this, 'convert_string' ), $app_keys_array );
        $app_keys = implode(', ', $app_keys_string );

        // Delete old setting
        $wpdb->query(
            $wpdb->prepare(
                'DELETE FROM ' . $wpdb->options . ' WHERE option_name LIKE %s AND option_name NOT IN (' . $app_keys . ')',
                '%beeketing_%'
            )
        );

        // Update beeketing products meta
        $wpdb->query(
            $wpdb->prepare(
                'UPDATE ' . $wpdb->postmeta . ' SET meta_key = %s WHERE meta_key = %s',
                '_beeketing_option1',
                '_bk_option1'
            )
        );

        // Update shop
        $this->set_api_key( $api_key );
        $this->setting_helper->update_settings( Setting::SETTING_API_KEY, $api_key );
        $updateResult = $this->update_shop_info( array(
            'platform' => \BKWPCommon\Data\Constant::PLATFORM,
        ) );

        // Update shop id
        $shopResult = $this->get( 'shops/api/' . $api_key );
        $shopIdResult = false;
        if ( isset( $shopResult['shop']['id'] ) ) {
            $this->setting_helper->update_settings( Setting::SETTING_SHOP_ID, $shopResult['shop']['id'] );
            $shopIdResult = true;
        }

        // Re-install app
        $beeketing_apps_data = $this->get_apps();
        $installResult = true;
        foreach ( $beeketing_apps_data as $beeketing_app_data ) {
            if ( isset( $beeketing_app_data['is_installed'], $beeketing_app_data['app_code'] ) && $beeketing_app_data['is_installed'] ) {
                $app_code = $beeketing_app_data['app_code'];
                $this->setting_helper->switch_settings( $app_code );
                $this->set_app_code( $app_code );

                // Get app action result
                $installAppResult = $this->install_app();
                if ( !$installAppResult ) {
                    $installResult = false;
                    break;
                }
            }
        }

        // If migrate failed
        if ( !$updateResult || !$shopIdResult || !$beeketing_apps_data || !$installResult ) {
            $wpdb->query(
                $wpdb->prepare(
                    'INSERT INTO ' . $wpdb->options . ' (option_name, option_value) VALUES (%s, %s)',
                    'beeketing_api_key',
                    $api_key
                )
            );
            delete_option( Constant::APP_SETTING_KEY );

            if ( $redirect ) {
                // Redirect to plugin page
                exit(wp_redirect(admin_url()));
            } else {
                return false;
            }
        }

        // Trigger re-sync shop
        $this->post( 'wordpress/sync_shop' );

        if ( $redirect ) {
            // Redirect to plugin page
            exit(wp_redirect(admin_url('admin.php?page=' . Constant::PLUGIN_ADMIN_URL)));
        } else {
            return true;
        }
    }
}