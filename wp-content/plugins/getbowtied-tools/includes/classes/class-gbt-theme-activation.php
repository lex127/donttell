<?php
/**
 * GBT_Activaton
 *
 * The GetBowtied Activation class handles remote theme activation
 *
 * @class       GBT_Activation
 * @version         2.0
 * @category    Class
 * @author      GetBowtied
 */

if ( ! class_exists( 'GBT_Activation' ) ) {
	class GBT_Activation {


		/**
		 * Do not instance
		 */
		private function __construct() {
		}

		/**
		 * Listen for activation requests; validate and save them
		 *
		 * @return bool | NULL
		 */
		public static function activation_listener() {
			if ( isset( $_GET['l'] ) ) {
				$license = $_GET['l'];
				if ( preg_match( '/^[a-f0-9]{32}$/', $license ) ) {
					return self::validate_license( $license );
				}
			}

			return null;
		}

		public static function deactivation_listener() {
			if ( isset( $_POST['deactivate_getbowtied_license'] ) && ($_POST['deactivate_getbowtied_license'] == 1) ) {
				delete_option( 'getbowtied_' . GBTHELPERS::theme_slug() . '_license' );
			}
		}

		/**
		 * Checks if the license received is valid
		 *
		 * @param  string $license
		 *
		 * @return bool
		 */
		private static function validate_license( $license ) {
			$api_url = 'http://my.getbowtied.com/api/api_listener.php';
			$args = array(
						'method' => 'POST',
						'timeout' => 30,
						'body' => array(
							'l' => $license,
							'd' => get_site_url(),
							't' => GBTHELPERS::theme_name(),
						),
					);

			$response = wp_remote_post( $api_url, $args );

			if ( is_wp_error( $response ) ) {
				return false;
			} else {
				$rsp = json_decode( $response['body'] );

				if ( $rsp->status == 1 ) {
					update_option( 'getbowtied_' . GBTHELPERS::theme_slug() . '_license', $license );
					return true;
				}

				return false;
			}
		}

		/**
		 * Returns the active license
		 *
		 * @return string | false
		 */
		public static function get_license() {
			return get_option( 'getbowtied_' . GBTHELPERS::theme_slug() . '_license' );
		}

		/**
		 * Frontend for this function
		 *
		 * @return bool
		 */
		public static function is_active() {
			return (self::get_license() !== false && trim( self::get_license() != false ));
		}

		/**
		 * Gets the connected user info from remote server
		 *
		 * @return bool|array
		 */
		public static function user_info() {
			if ( self::is_active() === true ) {
				$api_url = 'https://api.getbowtied.com/v2/user-info.json';
				$args = array(
					'method' => 'POST',
					'timeout' => 30,
					'body' => array(
						'k' => self::get_license(),
						'd' => get_site_url(),
						't' => GBTHELPERS::theme_name(),
					),
				);

				$response = wp_remote_post( $api_url, $args );
				if ( is_wp_error( $response ) ) {
					return false;
				} else {
					$rsp = json_decode( $response['body'], true );
					if ( isset( $rsp['username'] ) && isset( $rsp['purchased'] ) && isset( $rsp['supported'] ) ) {
						return $rsp;
					}
				}
			}

			return false;
		}
	}
}// End if().


GBT_Activation::activation_listener();
GBT_Activation::deactivation_listener();
